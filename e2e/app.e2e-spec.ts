import { FrsPage } from './app.po';

describe('frs App', () => {
  let page: FrsPage;

  beforeEach(() => {
    page = new FrsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
