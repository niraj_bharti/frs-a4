import { provideRoutes, RouterModule } from '@angular/router';
import { RegisterSucessComponent } from './user_onboard/register/register-sucess.component';
import { LoginComponent } from './user_onboard/login/login.component';
import { OnboardingComponent } from './user_onboard/onboarding.component';
import { RegisterComponent } from './user_onboard/register/register.component';
import { ResetPasswordComponent } from './user_onboard/reset-password/reset-password.component';
import { ForgotPasswordComponent } from './user_onboard/forgot-password/forgot-password.component';
import { GardToken } from './services/gard.service';
import { ResetSuccess } from './user_onboard/reset-password/reset-success';
import { PostHwaComponent } from './post_hwa/post-hwa.component';
import { PaypalComponent } from './paypal/paypal.component';
import { ActiveAdComponent } from './active_ad/active-ad.component';
import { ApplicantWorkflowComponent } from './applicant-workflow/applicant-workflow.component';

// Apllicant workflow section start here
// All the component realted to Applicant start here
import { LandingComponent } from './applicant-workflow/uploadResume/landing/landing.component';
import { KnowMoreComponent } from './applicant-workflow/uploadResume/know-more/know-more.component';
import { KoQuestionComponent } from './applicant-workflow/uploadResume/knockout-question/knockout-question.component';
import { SkillQuestionComponent } from './applicant-workflow/uploadResume/skill-question/skill-question.component';
import { SucessScreenComponent } from './applicant-workflow/uploadResume/sucess-screen/sucess-screen.component';
import { EmploymentHistoryComponent } from './applicant-workflow/createResume/employment-history/employment-history.component';
import { EducationalDetailsComponent } from './applicant-workflow/createResume/educational-details/educational-details.component';

import { LicenceCertificateComponent } from './applicant-workflow/createResume/licence-certificate/licence-certificate.component';
import { MilitaryHistoryComponent } from './applicant-workflow/createResume/military-history/military-history.component';
import { CandidateRefrenceComponent } from './applicant-workflow/createResume/candidate-refrence/candidate-refrence.component';
import { ResumePreviewComponent } from './applicant-workflow/createResume/resume-preview/resume-preview.component';
import { ApplicantLoginComponent } from './applicant-workflow/existingResume/applicant-login/applicant-login.component';
import { ApplicantRegisterComponent } from './applicant-workflow/existingResume/applicant-register/applicant-register.component';
import { ExistingResumeComponent } from './applicant-workflow/existingResume/existing-resume/existing-resume.component';
import { ObjectiveComponent } from './applicant-workflow/createResume/objective/objective.component';
import { AccomplishmentComponent } from './applicant-workflow/createResume/accomplishment/accomplishment.component';
import { MoreInfoComponent } from './applicant-workflow/more-info/more-info.component';
import { ResetpasswordComponent } from './applicant-workflow/existingResume/restpassword/restpassword.component';
import {PreviewComponent} from './applicant-workflow/existingResume/preview/preview.component';
import {ApplicantRegisterSucessComponent} from './applicant-workflow/existingResume/applicant-register/applicant-register-sucess/applicant-register-sucess.component';
import {PasswordSetComponent} from './applicant-workflow/existingResume/password-set/password-set.component';
import {GuardApplicantService} from './applicant-workflow/services/guard-applicant.service';
import {SuccessMsgComponent} from './misce/global/success/success-msg.component';
import {ProfileComponent} from './employer_workflow/0_dashboard/profile/profile.component';
import {CreateHwaComponent} from './employer_workflow/1_hwa/hwa_quadrant/create_hwa/create-hwa/create-hwa.component';
import {KnockoutQuestionComponent} from './employer_workflow/1_hwa/hwa_quadrant/create_hwa/knockout-question/knockout-question.component';
import {SkillAndExperienceComponent} from './employer_workflow/1_hwa/hwa_quadrant/create_hwa/skill-and-experience/skill-and-experience.component';
import {Step1Component} from './employer_workflow/1_hwa/hwa_quadrant/create_hwa/businessProfile/step1/step1.component';
import {Step2Component} from './employer_workflow/1_hwa/hwa_quadrant/create_hwa/businessProfile/step2/step2.component';
import {Step3Component} from './employer_workflow/1_hwa/hwa_quadrant/create_hwa/businessProfile/step3/step3.component';
import {Step4Component} from './employer_workflow/1_hwa/hwa_quadrant/create_hwa/businessProfile/step4/step4.component';
import {HwaQuadrantScreenComponent} from './employer_workflow/1_hwa/hwa_quadrant/hwa-quadrant-screen.component';
import {ParentQuadrantComponent} from './employer_workflow/1_hwa/parent-quadrant.component';
import {ListCopyHwaComponent} from './employer_workflow/1_hwa/hwa_quadrant/copy_hwa/list-copy-hwa.component';
import {ViewadComponent} from './employer_workflow/1_hwa/hwa_quadrant/view_ad/viewad.component';
import {CopyAdComponent} from './employer_workflow/1_hwa/hwa_quadrant/copy_hwa/copy-ad/copy-ad.component';
import {EdithwaComponent} from './employer_workflow/1_hwa/hwa_quadrant/edit_hwa/edit-ad/edithwa.component';
import {ListEditHwaComponent} from './employer_workflow/1_hwa/hwa_quadrant/edit_hwa/list-edit-hwa.component';
import {ListExtendHwaComponent} from './employer_workflow/1_hwa/hwa_quadrant/extend_hwa/list-extend-hwa.component';
import {ExtendHwaComponent} from './employer_workflow/1_hwa/hwa_quadrant/extend_hwa/extend-hwa/extend-hwa.component';
import {ActiveHwaListComponent} from './employer_workflow/2_allapplicant/1_activehwa/active-hwa-list/active-hwa-list.component';
import {ApplicantsListComponent} from './employer_workflow/2_allapplicant/2_applicantswhoapplied/applicants-list/applicants-list.component';
import {ViewApplicationComponent} from './employer_workflow/2_allapplicant/3_viewapplicant/view-application.component';
import {ViewresumeComponent} from './employer_workflow/2_allapplicant/viewresume/viewresume.component';

// End Applicant Workflow

const APP_ROUTES = [
  //  {path:'success', component: RegisterSucessComponent},
  { path: 'login', component: LoginComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'pass/reset/:token/:uid/:isonetime', component: ResetPasswordComponent },
  { path: 'reset-success', component: ResetSuccess },
  // {path:'', component:LoggedinComponent, canActivate: [GardToken]},
  //  {path:'home/:sucessMessage', component:LoggedinComponent, canActivate: [GardToken]},
  { path: 'signup', component: RegisterComponent },

  // Social Login
  { path: 'social', component: ApplicantWorkflowComponent },

  // Applicant Workflow Route Section

  { path: 'landing',
    component: LandingComponent,
    data: {title: 'Upload Resume'}
  },
  { path: 'basicinformation', component: KnowMoreComponent },
  { path: 'knockoutquestion', component: KoQuestionComponent },
  { path: 'skillquestion', component: SkillQuestionComponent },
  { path: 'sucessmessage', component: SucessScreenComponent },
  { path: 'objective', component: ObjectiveComponent },
  { path: 'accomplishment', component: AccomplishmentComponent },
  { path: 'employement', component: EmploymentHistoryComponent },
  { path: 'education', component: EducationalDetailsComponent },
  { path: 'licence', component: LicenceCertificateComponent },
  { path: 'militry', component: MilitaryHistoryComponent },
  { path: 'refrence', component: CandidateRefrenceComponent },
  { path: 'preview', component: ResumePreviewComponent },
  { path: 'applicantlogin', component: ApplicantLoginComponent },
  { path: 'applicantsignup', component: ApplicantRegisterComponent },
  { path: 'useexisting', component: ExistingResumeComponent },
  { path: 'moreinfo', component: MoreInfoComponent},
  {path: 'reset', component: ResetpasswordComponent},
  {path: 'signupsuccess', component: ApplicantRegisterSucessComponent},
  {path: 'viewresume', component: PreviewComponent},
  {path: 'set', component: PasswordSetComponent},

  // Applicant Routing End here

  { path: '', component: ProfileComponent, canActivate: [GardToken] }, // UserProfile
  { path: 'UserProfile', component: ProfileComponent, canActivate: [GardToken] }, //
  { path: 'createHWA', component: CreateHwaComponent, canActivate: [GardToken] },
  { path: 'addKnockoutQuestion', component: KnockoutQuestionComponent, canActivate: [GardToken] },
  { path: 'success/:msg/:tital', component: SuccessMsgComponent },
  { path: 'AddSkillsAndExpertiseQuestions', component: SkillAndExperienceComponent, canActivate: [GardToken] },
  { path: 'businessprofile/step-one', component: Step1Component, canActivate: [GardToken] },
  { path: 'businessprofile/step-two', component: Step2Component, canActivate: [GardToken] },
  { path: 'businessprofile/step-three', component: Step3Component, canActivate: [GardToken] },
  { path: 'businessprofile/step-four', component: Step4Component, canActivate: [GardToken] },
  { path: 'hwa_workflow', component: HwaQuadrantScreenComponent, canActivate: [GardToken] },
  { path: 'employer_dashboard/:frmpaypal', component: ParentQuadrantComponent, canActivate: [GardToken] },
  { path: 'postmyad', component: PostHwaComponent, canActivate: [GardToken] },
  { path: 'payment', component: PaypalComponent, canActivate: [GardToken] },
  { path: 'active/:hwa_id', component: ActiveAdComponent },
  { path: 'listhwa-copy', component: ListCopyHwaComponent, canActivate: [GardToken] },
  { path: 'viewad/:hwaId/:expDate/:btnlbl', component: ViewadComponent, canActivate: [GardToken] },
  { path: 'makeAcopy/:hwaId', component: CopyAdComponent, canActivate: [GardToken] },
  { path: 'makeAedit/:hwaId/:draftEdit', component: EdithwaComponent, canActivate: [GardToken] },
  { path: 'listhwa-edit', component: ListEditHwaComponent, canActivate: [GardToken] },
  { path: 'listhwa-extend', component: ListExtendHwaComponent, canActivate: [GardToken] },
  { path: 'extendhwa/:expDate', component: ExtendHwaComponent, canActivate: [GardToken] },
  { path: 'allactivehwalist', component: ActiveHwaListComponent, canActivate: [GardToken]},
  { path: 'whohasapplied/:hwaName/:hwaId', component: ApplicantsListComponent, canActivate: [GardToken]},
  { path: 'viewapplicant/:hwaName/:hwaId/:applicantId', component: ViewApplicationComponent, canActivate: [GardToken]},
  { path: 'viewresume/:url', component: ViewresumeComponent, canActivate: [GardToken]},
  { path: '**', redirectTo: '/', pathMatch: 'full' }
];

const APP_ROUTES_PROVIDER = [
  provideRoutes(APP_ROUTES)
];
export const routing = RouterModule.forRoot(APP_ROUTES);
