import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { UserService } from '../services/user.service';
import { AlertService } from '../services/alert.service';

@Component({
  selector: 'app-paypal',
  templateUrl: './paypal.component.html',
  styleUrls: ['./paypal.component.css']
})
export class PaypalComponent implements OnInit {

  constructor(
    private router: Router,
    private userService: UserService,
    private alertService: AlertService,
    private activatedRoute: ActivatedRoute) { }
  private title = 'PayPal Test Payment';
  private paymentAmt = 0;
  private subscription: Subscription;
  private hwaId:any = '';
  private positionTitle:string='';
  private activebutton:boolean = false;
  private start_date:any;
  private end_date:any;
  private extend_date_days:any;
  private uid:any;
  private action:string='';
  private domainUrl:string = this.userService.paypalRedirect;
  private drupalDomainUrl = this.userService.switchUrl;
  private currencyCode:string= 'USD';
  private currentDate;
  private currentDateTimestamp;

  ngOnInit() {
    let user = this.userService.isLogedin();
    this.uid = user.uid;
    this.subscription = this.activatedRoute.params.subscribe(
      (param: any) => {
        // console.log(param['totalAmt']);
        if (param['totalAmt']) {
          this.paymentAmt = param['totalAmt'];
        }
      });

    let paymentData = JSON.parse(localStorage.getItem('pamentData'));
    if (paymentData) {
      let sd = new Date(paymentData.start_date);
      let ed = new Date(paymentData.end_date);

      console.log('start date', this.toTimestampParse(sd));
      console.log('toUTCString', this.toTimestampParse(sd.toUTCString()));
      this.start_date  = this.toTimestampParse(sd.toUTCString()); //
      this.end_date  = this.toTimestampParse(ed.toUTCString()); //
      console.log(this.start_date)
// this.start_date  = this.toTimestamp(sd.getUTCFullYear(), sd.getUTCMonth(), sd.getUTCDay(), sd.getUTCHours(), sd.getUTCMinutes(), sd.getUTCSeconds())
// this.end_date  =  this.toTimestamp(ed.getUTCFullYear(), ed.getUTCMonth(), ed.getUTCDay(), ed.getUTCHours(), ed.getUTCMinutes(), ed.getUTCSeconds())
      this.extend_date_days  = paymentData.extend_date_days;
      this.paymentAmt = paymentData.total_amt;
      this.action = paymentData.action;
      console.log(this.start_date , this.end_date  );
      this.currentDate = new Date(new Date().toUTCString()).getTime();
      this.currentDateTimestamp = (this.currentDate / 1000);
      console.log('Rakesh', this.currentDateTimestamp);
    }



    if (localStorage.getItem('storeHwaNid')) {
      this.activebutton = true;
      this.hwaId = localStorage.getItem('storeHwaNid');
      if (localStorage.getItem('storeHwaFormData')) {
        let formdata = JSON.parse(localStorage.getItem('storeHwaFormData'));
        this.positionTitle = formdata.title;
        // this.selectedNofPostion = formdata.field_how_many_people_do_you_nee;
        // this.posType = formdata.field_will_they_be_full_time_par;
      }
    } else {
      this.activebutton = false;
      this.router.navigate(['/employer_dashboard', '0']);
    }
  }

  toTimestamp(year, month, day, hour, minute, second) {
    let datum = new Date(Date.UTC(year, month, day, hour, minute, second));
    console.log('only time ', datum.getTime());
    console.log('with1000 time ', datum.getTime() / 1000);
    return datum.getTime() / 1000.0;
  }
  /* timeToHuman()
   {
   let theDate = new Date(document.u2h.timeStamp.value * 1000);
   dateString = theDate.toGMTString();
   document.u2h.result.value = dateString;
   }
   humanToTime()
   {
   let humDate = new Date(Date.UTC(document.h2u.inYear.value,
   (stripLeadingZeroes(document.h2u.inMon.value)-1),
   stripLeadingZeroes(document.h2u.inDay.value),
   stripLeadingZeroes(document.h2u.inHr.value),
   stripLeadingZeroes(document.h2u.inMin.value),
   stripLeadingZeroes(document.h2u.inSec.value)));
   document.h2u.result.value = (humDate.getTime()/1000.0);
   }*/

  toTimestampParse(strDate) {
// return datum/1000;
    let nowUtc = new Date(strDate).getTime(); // + (new Date().getTimezoneOffset() * 60000);
    return nowUtc / 1000;
  }

  onSubmit() {
    // this.userService.removeHWAstorage()
  }

}
