import { Component, OnInit } from '@angular/core';
// import {location} from "@angular/platform-browser/src/facade/browser";
import {Subscription } from 'rxjs';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-success',
  templateUrl: './success-msg.component.html',
  styleUrls: ['./success-msg.component.css']
})
export class SuccessMsgComponent implements OnInit {
private msg:string ="";
private subscription: Subscription;
private hideCloseBtn: boolean = false;
private tital:string ='Check Your Email';
private enableLogin;
constructor(private activatedRoute: ActivatedRoute, private router: Router ) { }

  ngOnInit() {
  this.enableLogin = localStorage.getItem('yes');
    this.subscription = this.activatedRoute.params.subscribe(
      (param: any) => {
        if (param['msg']) {
          this.msg = param['msg'];
          this.tital = param['tital']; // "Check Your Email";
//console.log(this.msg);
          if(this.msg === "We’ve sent you a link to reset your password"){
            this.hideCloseBtn = true;
          } else {
            this.hideCloseBtn = false;
          }
        }
      });
  }
  ngOnDestroy() {
     // prevent memory leak by unsubscribing
     this.subscription.unsubscribe();
   }
  closeWindow() {
  }
  gotoLogin() {
   localStorage.removeItem('yes');
    if (localStorage.getItem('applicantForgotPassword')) {
      this.router.navigate(['/applicantlogin']);
    } else {
      this.router.navigate(['/login']);
    }

  }
}
