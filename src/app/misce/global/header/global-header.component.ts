import { Component, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Router } from "@angular/router";
import {UserService} from "../../../services/user.service";
declare var $:any;
@Component({
  selector: 'app-global-header',
  templateUrl: './global-header.component.html',
  styleUrls: ['./global-header.component.css']
})
export class GlobalHeaderComponent implements OnInit {
private user: any;
private isLogedin = false;
private  businessName:string;
private firstName:string;


  showStyle: false;
  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
// this.userService.isLogedin();
    let user = this.userService.isLogedin();
    this.userService.ifUserExist(user.uid).subscribe(
      res => {
        if (res.length === 0) {
          alert('user removed!')
          localStorage.clear();
          this.router.navigate(['/signup']);
        } else {
        }
      });
    this.userService.getLogedIn().subscribe((islogedin: any) => {
        if (islogedin) {
          this.isLogedin = true;
          this.user = JSON.parse(localStorage.getItem('currentUser'));
          if (this.user) {
            if (this.user.details) {
            if (this.user.details.field_name_of_your_business[0]) {
            this.businessName = this.user.details.field_name_of_your_business[0].value;
            }
            if(this.user.details.field_first_name[0]) {
            this.firstName = this.user.details.field_first_name[0].value;
            }
          }
        }else{
          this.userService.setLogedIn(false);
          this.router.navigate(['/login']);
        }
        }else {
          this.isLogedin = false;
        }
    });
  }

logout() {
  this.userService.logout();
}
  getStyle() {
    if(this.showStyle){
      return "block";
    } else {
      return "none";
    }
  }

}
