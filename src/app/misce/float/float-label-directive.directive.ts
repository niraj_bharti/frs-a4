import {Directive, ElementRef, Host, HostBinding, Renderer, OnInit} from '@angular/core';

@Directive({
  selector: '[appFloatLabelDirective]',
  host:{
    '(focus)': 'onFocus($event.target)',
    '(blur)': 'onBlur($event.target)',
    '(focusout)':'onFocusout($event.target)',
    '(keyup)':'onKeyup($event.target)',
    '(ngModelChange)':'onChang($event.target)'
  }
})
export class FloatLabelDirectiveDirective  implements OnInit {
  private defaultClass:string = 'blue';

  constructor(private el: ElementRef, private renderer: Renderer) {
    this.el = el;

    let currValue = this.el.nativeElement.value;
    if(currValue !==''){
      this.renderer.setElementClass(this.el.nativeElement, 'goUp', true);
    }
    else {
      this.renderer.setElementClass(this.el.nativeElement, 'goUp', false);
    }
  }
  ngOnInit() {
    let currValue = this.el.nativeElement.value;
    if(currValue !== ''){
      this.renderer.setElementClass(this.el.nativeElement, 'goUp', true);
    }
    else {
      this.renderer.setElementClass(this.el.nativeElement, 'goUp', false);
    }
  
  }
  onFocusout(){
    let currValue = this.el.nativeElement.value;
    if(currValue !==''){
      this.renderer.setElementClass(this.el.nativeElement, 'goUp', true);
    }
    else {
      this.renderer.setElementClass(this.el.nativeElement, 'goUp', false);
    }
  }
  onFocus() {
  let currValue = this.el.nativeElement.value;

    if(currValue ===''){
      this.renderer.setElementClass(this.el.nativeElement, 'goUp', true);
    }
    else{
      this.renderer.setElementClass(this.el.nativeElement, 'goUp', true);
    }
  }
  onBlur() {
    let currValue = this.el.nativeElement.value;
    if(currValue !==''){
      this.renderer.setElementClass(this.el.nativeElement, 'goUp', true);
    }
    else {
      this.renderer.setElementClass(this.el.nativeElement, 'goUp', false);
    }
  }

  onKeyup(){
    let currValue = this.el.nativeElement.value;
    if(currValue !==''){
      this.renderer.setElementClass(this.el.nativeElement, 'goUp', true);
    }
  }
  onChang(){
    let currValue = this.el.nativeElement.value;
    if(currValue !==''){
      this.renderer.setElementClass(this.el.nativeElement, 'goUp', true);
    }
  }
}
