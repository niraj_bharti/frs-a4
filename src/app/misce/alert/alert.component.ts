import { Component, Input, OnInit } from '@angular/core';
import {AlertService} from '../../services/alert.service';

@Component({
    selector: 'alert-msg',
	  styleUrls: ['./alert.component.css'],
    templateUrl: './alert.component.html'
})

export class AlertComponent {
    @Input() message: any;

    constructor(private alertService: AlertService) { }

    ngOnInit () {
        this.alertService.getMessage().subscribe(message => { this.message = message; });
    }
}

