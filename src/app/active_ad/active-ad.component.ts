import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { HwaCommonService } from '../services/hwa-common.service';
import { Subscription } from 'rxjs';
import { OverlayDataService } from '../services/overlay-data.service';
@Component({
  selector: 'app-activead',
  templateUrl: './activehwa.component.html',
  styleUrls: ['./activehwa.component.css']
})
export class ActiveAdComponent implements OnInit {
  private testActive: string;
  private subscription: Subscription;

  private position: any;
  private posType: any;
  private describePos: any;
  private describeSkill: any;
  private City: any[];
  private address1: any;
  private address2: any;
  private address3: any;

  public companyname = '';
  public cityName= '';
  public stateName = '';
  private shoBasicHwa = false;
  private locationLenght: any;

  private noOfPosition: number;
  private showProfile: boolean;
  private addressLenght: number;
  private proFileObj: any;
  private image1Url: any;
  private image2Url: any;
  private image3Url: any;
  private image4Url: any;

  private titalText1: any;
  private titalText2: any;
  private titalText3: any;
  private titalText4: any;

  private bodyText1: any;
  private bodyText2: any;
  private bodyText3: any;
  private bodyText4: any;

  private HwaIdApplyNow;
  private activeAd: boolean;
  private hwaMessage;
  private pageLoded: boolean;
  private hideEntireBP: boolean;
  private hide4th: boolean;

  constructor(
    // private alertService: AlertService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private hwaServices: HwaCommonService,
    private hwaOverlayService: OverlayDataService) { }

  ngOnInit() {
    this.testActive = 'active';
    //  let user = this.userService.isLogedin();
    //  this.companyname =' ' // user.details.field_name_of_your_business[0].value;

    this.subscription = this.activatedRoute.params.subscribe(
      (param: any) => {
        //  console.log(param['totalAmt']);
        if (param['hwa_id']) {
          this.HwaIdApplyNow = param['hwa_id'];
          // this.paymentAmt = param['frmpaypal'];
          this.showAllDataOfHwa(param['hwa_id']);
        }
      });
  }
  applyNow() {
    this.subscription = this.activatedRoute.params.subscribe(
      (param: any) => {

        localStorage.setItem('applicantHWAToken', this.HwaIdApplyNow = param['hwa_id']);
        this.router.navigate(['/landing']);
      });
  }
  public showAllDataOfHwa(hwaid) {
    let hwaObj = {
      'hwa_nid': hwaid
    };

    this.hwaServices.getAllDataOfHwa(hwaObj).subscribe(
      res => {
        if (res['Hwa']) {
          let a = res.business_profile.field_image1 === undefined;
          let b = res.business_profile.field_image2 === undefined;
          let c = res.business_profile.field_image3 === undefined;
          let d = res.business_profile.field_image4 === undefined;
      if (a && b  && c && d) {
        this.hideEntireBP = true;
          } else {
         this.hideEntireBP = false;
          }

          if (res.business_profile.field_image4 === undefined) {
            this.hide4th = true;
          } else {
            this.hide4th = false;
          }

          // this.activeAd = false;
          this.hwaMessage = 'This HWA doesn\'t exist';
          if (res.stop_resume_status[0].field_stop_resume === 'Stop') {
          }  else {
            this.hwaMessage = '';
            this.shoBasicHwa = true;
            this.City = [];
            this.addressLenght = res['Hwa'].field_which_location_s_are_you_h.length;
            this.locationLenght = res['Hwa'].field_which_location_s_are_you_h.length;
            this.companyname = res['business'];
            this.position = res['Hwa'].title[0].value;
            this.posType = res['Hwa'].field_will_they_be_full_time_par[0].value;
            this.noOfPosition = Number(res['Hwa'].field_how_many_people_do_you_nee[0].value);

            if (this.locationLenght > 1) {
              this.City = [];
              this.City = res['Hwa'].field_which_location_s_are_you_h;
              this.address1 = '';
              this.address2 = '';
              this.address3 = '';
            }

            this.describePos = res['Hwa'].field_how_would_you_describe_thi[0].value;
            this.describeSkill = res['Hwa'].field_describe_the_skills_and_ex[0].value;
            if (res['Hwa'].field_which_location_s_are_you_h.length === 1) {
              this.City = [];
              if (res['Hwa'].field_which_location_s_are_you_h[0].field_city[0].value) {
                this.address1 = res['Hwa'].field_which_location_s_are_you_h[0].field_city[0].value + ',';
              } else {
                this.address1 = res['Hwa'].field_which_location_s_are_you_h[0].field_city[0].value;
              }
              if (res['Hwa'].field_which_location_s_are_you_h[0].field_city[0].value) {
                this.address2 = res['Hwa'].field_which_location_s_are_you_h[0].field_state[0].value + ',';
              } else {
                this.address2 = res['Hwa'].field_which_location_s_are_you_h[0].field_state[0].value;
              }
              if (res['Hwa'].field_which_location_s_are_you_h[0].title[0].value) {
                this.address3 = res['Hwa'].field_which_location_s_are_you_h[0].title[0].value + ',';
              } else {
                this.address3 = res['Hwa'].field_which_location_s_are_you_h[0].title[0].value;

              }
            } else {
              this.address1 = '';
              this.address2 = '';
              this.address3 = '';
            }
            if (res['business_profile']) {
              this.showProfile = true;
              this.proFileObj = res['business_profile'];

              if (this.proFileObj.field_image1) {
                this.image1Url = this.proFileObj.field_image1[0].url;
              } else {
                this.image1Url = '';
              }
              if (this.proFileObj.field_image2) {
                this.image2Url = this.proFileObj.field_image2[0].url;
              } else {
                this.image2Url = '';
              }
              if (this.proFileObj.field_image3) {
                this.image3Url = this.proFileObj.field_image3[0].url;
              } else {
                this.image3Url = '';
              }
              if (this.proFileObj.field_image4) {
                this.image4Url = this.proFileObj.field_image4[0].url;
              } else {
                this.image4Url = '';
              }
              // ----------------------------------------------------------------
              if (this.proFileObj.field_title1) {
                this.titalText1 = this.proFileObj.field_title1[0].value;
              } else {
                this.titalText1 = '';
              }
              if (this.proFileObj.field_title2) {
                this.titalText2 = this.proFileObj.field_title2[0].value;
              } else {
                this.titalText2 = '';
              }
              if (this.proFileObj.field_title3) {
                this.titalText3 = this.proFileObj.field_title3[0].value;
              } else {
                this.titalText3 = '';
              }
              if (this.proFileObj.field_title4) {
                this.titalText4 = this.proFileObj.field_title4[0].value;
              } else {
                this.titalText4 = '';
              }
              // ---------------------------------------------------------------------------
              if (this.proFileObj.body) {
                this.bodyText1 = this.proFileObj.body[0].value;
              } else {
                this.bodyText1 = '';
              }
              if (this.proFileObj.field_body2) {
                this.bodyText2 = this.proFileObj.field_body2[0].value;
              } else {
                this.bodyText2 = '';
              }
              if (this.proFileObj.field_body3) {
                this.bodyText3 = this.proFileObj.field_body3[0].value;
              } else {
                this.bodyText3 = '';
              }
              if (this.proFileObj.field_body4) {
                this.bodyText4 = this.proFileObj.field_body4[0].value;
              } else {
                this.bodyText4 = '';
              }
            }
            this.activeAd = true;
          }
        }
        this.pageLoded = true;
      });
  }
}
