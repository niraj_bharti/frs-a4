import {Component, Inject, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {UserService} from '../../services/user.service';
import {AlertService} from '../../services/alert.service';
import {Router} from '@angular/router';
import {DOCUMENT} from "@angular/platform-browser";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  private forgotPasswordForm: FormGroup;
  private loading: boolean;
  // public enableLogin: boolean;
  public emailMask = /^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private alertService: AlertService,
    private router: Router
  ) { }

  ngOnInit() {

    this.document.body.classList.remove('buildresume1');

    this.forgotPasswordForm = this.formBuilder.group(
      {
        'email': ['', Validators.compose([Validators.required, Validators.pattern(this.emailMask)])]
  });
  }

  onSubmit(fields) {

    this.loading = true;
    let data = JSON.stringify({'mail': fields.email, 'action': 'forget-password'});
    this.userService.forgotPassword(data).subscribe(
      res => {
        if (res) {
          this.loading = false;
          if (res['error_message']) {
            this.alertService.success(res['error_message'], true);

            this.router.navigate(['/success', 'This Email Id does not exist in our system.', 'Error!']);
           // this.enableLogin = true;
            localStorage.setItem('yes', 'true');
          } else {
            this.alertService.error('We’ve sent you a link to reset your password');
            this.router.navigate(['/success', `
            We've sent you a link to reset your password. Please use that link to login to Talentrackr. `, 'Check Your Email' ] );
          }
        } else {
          this.loading = false;
          this.alertService.error(res['message']);
        }
        let user = res['current_user'];

        if (user) {
          //  store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('userToken', JSON.stringify(res['csrf_token']));
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.loading = false;
          this.userService.setLogedIn(true);
          this.router.navigate(['/']); //  loggedIn

        }

      },
      error => {
        this.alertService.error(`The password you've entered is incorrect`);
        this.loading = false;
      });
  }
}
