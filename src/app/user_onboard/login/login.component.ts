import { Component, OnInit, ViewChild, Input, AfterContentInit, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { UserService } from '../../services/user.service';
import { AlertService } from '../../services/alert.service';
import { ShowHideInput } from '../reset-password/show-hide-directive';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private loginform: FormGroup;
  private loading: boolean;
  private show = false;
  private rememberme: boolean;
  private alertmsg: boolean;
  @Input() emailval = '';
  @Input() passwordval = '';
  @ViewChild(ShowHideInput) input: ShowHideInput;
  public emailMask = /^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private alertService: AlertService,
    private router: Router,

  ) { }

  ngOnInit() {
    this.userService.isLogedin();
    this.loginform = this.formBuilder.group({
      'password': ['', Validators.required],
      'email': ['', Validators.compose([Validators.required, Validators.pattern(this.emailMask)])]
  });

    setTimeout(() => {
      if (localStorage.getItem('rememberme')) {
        let udata = JSON.parse(localStorage.getItem('rememberme'));
        this.rememberme = true;
        this.emailval = udata.name.toString();
        this.passwordval = udata.pass.toString();
      } else {
        this.emailval = '';
        this.passwordval = '';
      }
    }, 400);
  }
  toggleShow() {
    this.show = !this.show;
    if (this.show) {
      this.input.changeType('text');
    } else {
      this.input.changeType('password');
    }
  }

  autoHideAlertMsg(time) {
    setTimeout(() => {
      this.alertmsg = false;
    }, time);
  }
  onSubmit(loginFields) {
    this.loading = true;
    this.alertmsg = true;
    let data = { 'name': loginFields.email, 'pass': loginFields.password };
    this.userService.login(data).subscribe(
      res => {
        console.log(res)
        if (res) {
          this.loading = false;
          this.alertService.success(res['message'], true);
          this.autoHideAlertMsg(6000);
          this.saveRememberMedata(data);
          localStorage.removeItem('storeHwaFormData');
          localStorage.removeItem('storeHwaNid');
          localStorage.removeItem('profileNid');
          localStorage.removeItem('prof1_img_nid');
          localStorage.removeItem('prof2_img_nid');
          localStorage.removeItem('prof3_img_nid');
          localStorage.removeItem('prof4_img_nid');
          if (res.status !== 'undefined') {
            this.alertmsg = true;
            this.alertService.error('Email Id or Password is Incorrect!');
          } else {
            this.router.navigate(['/']); // loggedIn
          }

        } else {
          this.loading = false;
          this.alertmsg = true;
          this.alertService.error(res['message']);
        }
        let user = res['current_user'];

        if (user) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('userToken', JSON.stringify(res['jwt_token']));
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.loading = false;
          this.userService.getUserData();
          // this.userService.setLogedIn(true);
          this.router.navigate(['/']); // loggedIn

        }

      },
      error => {
        this.alertmsg = true;
        this.alertService.error('Sorry, unrecognized email or password', true);
        this.autoHideAlertMsg(6000);
        this.loading = false;
      });

  }
  rememberMe(event) {
    this.rememberme = event.target.checked;
  }

  saveRememberMedata(data) {
    if (this.rememberme) {
      localStorage.setItem('rememberme', data);
    } else {
      localStorage.removeItem('rememberme');
    }
  }

}
