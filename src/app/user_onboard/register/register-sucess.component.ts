import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';

@Component({
  selector: 'app-register-sucess',
  templateUrl: './register-sucess.component.html',
  styleUrls: ['./register-sucess.component.css']
})
export class RegisterSucessComponent implements OnInit {

  constructor( private router: Router) { }

  ngOnInit() {
  }

}
