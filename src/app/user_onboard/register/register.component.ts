import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router} from '@angular/router';

import { UserService } from '../../services/user.service';
import { AlertService } from '../../services/alert.service';
// import MessageResult = jasmine.MessageResult;
// import {el} from '@angular/platform-browser/testing/browser_util';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  myForm: FormGroup;
  private loading: boolean;
  private emailStatus: string;
  public myModel = '';
  public mask = [/[1-9]/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public emailMask = /^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  constructor(private formBuilder: FormBuilder,  private router: Router, private userService: UserService,
              private alertService: AlertService) { }
  ngOnInit() {
    this.myForm = this.formBuilder.group({
      'businessname': ['', [Validators.required]],
      'firstname': ['', [Validators.required]],
      'lastname': ['', [Validators.required]],
      'email': ['', Validators.compose([Validators.required, Validators.pattern(this.emailMask)])],
      'confirmemail': ['', Validators.compose([Validators.required, Validators.pattern(this.emailMask)])],
      'phone': ['', [Validators.required, Validators.pattern('^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$')]]
  }, {validator: this.isEqualEmail});
  }

  onSubmit(fvalue: any, valid: boolean, enent: Event) {
    this.loading = true;
    let userObj = {
      'field_name_of_your_business': fvalue.businessname,
      'mail': fvalue.email,
      'field_first_name': fvalue.firstname,
      'field_last_name': fvalue.lastname,
      'field_phone_number': fvalue.phone
    };

    if (valid) {
      this.userService.create(userObj).subscribe(
        res => {
          if (res) {
            this.loading = false;
            if (res.user_status === 'existing') {
              this.loading = false;
              this.emailStatus = `An account with this email already exists.
                    Please use the LOGIN button below to access Talentrackr.`;
              this.router.navigate(['/']);
            } else {
              this.router.navigate(['/success', 'Please check your email, we have sent you a link to set your password. You will need to click on that link to complete your registration process.', 'Thank You For Registering']);
            }
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
    }else {
      this.loading = false;
    }
    // return false;
  }
  goTologin() {
    this.router.navigate(['/login']);
  }
  isEqualEmail(control: FormGroup ): {[s: string]: boolean} {
    if (!control) {
      return {emailNotMatch: true};
    }

    if (control.controls['email'].value === control.controls['confirmemail'].value) {
      return null;
    } else {
      return {'emailNotMatch': true};
    }
  }

}
