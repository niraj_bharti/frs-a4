import { Component } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'reset-success',
  templateUrl: './reset-success.html',
  styleUrls: ['./reset-password.component.css']
})

export class ResetSuccess {
  constructor(private router: Router){}
  gotoLogin() {
    this.router.navigate(['/login']);
  }
}
