import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ShowHideInput } from './show-hide-directive';
import { ShowHideCnf } from './show-hide-cnf';

import { UserService } from '../../services/user.service';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit,  OnDestroy {
  private tokenid: any;
  private uid: any;
  private subscription: Subscription;
  private loading: boolean;
  private show = false;
  private show1 = false;
  private displayForm: boolean;
  private titalname = 'Set Your Password';

  @ViewChild(ShowHideInput) input: ShowHideInput;
  @ViewChild(ShowHideCnf) inputcnf: ShowHideCnf;
  resetPassword: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    //  private authenticationService: AuthService,
    private userService: UserService,
    private alertService: AlertService,
    private activatedRoute: ActivatedRoute
  ) {

  }

  ngOnInit() {
    let strongRegex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#_\$%\^&\*])(?=.{8,})');
    this.resetPassword = this.formBuilder.group({
      //  'password':['',Validators.required],
      'password': ['', Validators.compose([Validators.required, Validators.pattern(strongRegex)])],
      'cpassword': ['', Validators.compose([
        Validators.required])]
    }, {validator: this.isEqualEmail});
    this.displayForm = false;
    this.subscription = this.activatedRoute.params.subscribe(
      (param: any) => {
        console.log(param['isonetime']);
        if (param['token'] && param['uid']) {
          if (param['isonetime'].toString() === '1') {
            this.titalname = 'Reset Your Password';
          } else {
            this.titalname = 'Set Your Password';
          }
          //  this.router.navigate(['/']);
          this.tokenid = param['token'];
          this.uid = param['uid'];
          this.tokenValidateUrl(this.tokenid, this.uid);
        }
      });
  }

  ngOnDestroy() {
    //  prevent memory leak by unsubscribing
    this.subscription.unsubscribe();
  }

  toggleShow() {
    this.show = !this.show;
    if (this.show) {
      this.input.changeType('text');
      this.inputcnf.changeType('text');
    } else {
      this.input.changeType('password');
      this.inputcnf.changeType('password');
    }
  }

  cnfToggleShow(e) {
    this.show1 = !this.show1;
    if (this.show1) {
      this.inputcnf.changeType('text');
    } else {
      this.inputcnf.changeType('password');
    }
  }

  tokenValidateUrl(tid, uid) {
    this.userService.validateUrltoken({token: tid, uid: uid}).subscribe(
      res => {
        if (res['data']) {
          this.displayForm = true;
          console.log(res['uid']);
          this.uid = res['uid'];
          this.tokenid = res['token'];
          //  this.alertService.success(res.message, true);
          //  this.router.navigate(['/login']);
        } else {
          this.uid = '';
          this.tokenid = '';
          if (res['message']) {
            this.alertService.error(res['message']);
          }else {
            this.alertService.error('Invalid token');
            this.router.navigate(['/success', 'Invalid Token', 'Invalid Token']);
          }
        }
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      });
  }
  onSubmit(formGroup) {

    formGroup.uid = this.uid;
    formGroup.token = this.tokenid;
    this.loading = true;
// let obj ={ formGroup}
    this.userService.resetPass(formGroup).subscribe(
      res => {
        if (res['status']) {
          this.alertService.success(res['message'], true);
          if (res.roles[1] === 'employer') {
            this.router.navigate(['/reset-success']);
          } else if (res.roles[1] === 'applicant') {
            this.router.navigate(['/set']);
          }
         // this.router.navigate(['/reset-success']);
          //   this.router.navigate(['/success', 'Thank you for registering with Talentrackr, let’s get started.', 'Password Saved!']);
        } else {
          this.alertService.error(res['message']);
        }

      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      });
  }

  matchPassword(control: FormControl): {[s: string]: boolean} {
    if (!this.resetPassword) {
      return {emailNotMatch: true};
    }
    if (control.value !== this.resetPassword.controls['password'].value) {
      return {emailNotMatch: true};
    }
  }

  isEqualEmail(control: FormGroup ): {[s: string]: boolean} {
    if (!control) {
      return {emailNotMatch: true};
    }

    if (control.controls['password'].value === control.controls['cpassword'].value) {
      return null;
    } else {
      return { passwordNotMatch: true};
    }
  }
}
