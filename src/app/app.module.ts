import {TabModule} from 'angular-tabs-component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { OnboardingComponent } from './user_onboard/onboarding.component';
import { RegisterComponent } from './user_onboard/register/register.component';
import { RegisterSucessComponent } from './user_onboard/register/register-sucess.component';
import { LoginComponent } from './user_onboard/login/login.component';
import { routing } from './app.routes';
import { HeaderComponent } from './user_onboard/header/header.component';
import { ResetPasswordComponent } from './user_onboard/reset-password/reset-password.component';
import { ShowHideInput } from './user_onboard/reset-password/show-hide-directive';
import { ShowHideCnf } from './user_onboard/reset-password/show-hide-cnf';
import { GardToken } from './services/gard.service';
import { UserService } from './services/user.service';
import { AlertService } from './services/alert.service';
import { DataService } from './services/data.service';
import { ForgotPasswordComponent } from './user_onboard/forgot-password/forgot-password.component';
import { MaskedInputDirective } from 'angular2-text-mask';
import { ResetSuccess } from './user_onboard/reset-password/reset-success';
import { BreadcrumbTitleComponent } from './employer_workflow/1_hwa/breadcrumb-title/breadcrumb-title.component';
import { OverlayComponent } from './employer_workflow/1_hwa/overlay/overlay.component';
import { HwaCommonService } from './services/hwa-common.service';
import { QuillEditorModule } from 'ng2-quill-editor';
import { OverlayDataService } from './services/overlay-data.service';
import { KnockoutQuestionComponent } from './employer_workflow/1_hwa/hwa_quadrant/create_hwa/knockout-question/knockout-question.component';
import { SkillAndExperienceComponent } from './employer_workflow/1_hwa/hwa_quadrant/create_hwa/skill-and-experience/skill-and-experience.component';
import { Step1Component } from './employer_workflow/1_hwa/hwa_quadrant/create_hwa/businessProfile/step1/step1.component';
import { Step2Component } from './employer_workflow/1_hwa/hwa_quadrant/create_hwa/businessProfile/step2/step2.component';
import { Step3Component } from './employer_workflow/1_hwa/hwa_quadrant/create_hwa/businessProfile/step3/step3.component';
import { Step4Component } from './employer_workflow/1_hwa/hwa_quadrant/create_hwa/businessProfile/step4/step4.component';
import { PostHwaComponent } from './post_hwa/post-hwa.component';
import { FinalOverlayComponent } from './employer_workflow/1_hwa/hwa_quadrant/create_hwa/final-overlay/final-overlay.component';
import { PaypalComponent } from './paypal/paypal.component';
import { MyDatePickerModule } from 'mydatepicker';
import { ActiveAdComponent } from './active_ad/active-ad.component';
import { Angular2SocialLoginModule } from 'angular2-social-login';
import { ApplicantWorkflowComponent } from './applicant-workflow/applicant-workflow.component';
import { ApplicantHeaderComponent } from './applicant-workflow/applicant-header/applicant-header.component';
import { LandingComponent } from './applicant-workflow/uploadResume/landing/landing.component';
import { KnowMoreComponent } from './applicant-workflow/uploadResume/know-more/know-more.component';
import { FileUploadDirective } from './applicant-workflow/directive/file-upload.directive';
// Applicant Services and Module import
import { ApplicantService } from './applicant-workflow/services/applicants.service';
import { ApplicantFooterComponent } from './applicant-workflow/applicant-footer/applicant-footer.component';
import { KoQuestionComponent } from './applicant-workflow/uploadResume/knockout-question/knockout-question.component';
import { SkillQuestionComponent } from './applicant-workflow/uploadResume/skill-question/skill-question.component';
import { SucessScreenComponent } from './applicant-workflow/uploadResume/sucess-screen/sucess-screen.component';
import { EmploymentHistoryComponent } from './applicant-workflow/createResume/employment-history/employment-history.component';
import { EducationalDetailsComponent } from './applicant-workflow/createResume/educational-details/educational-details.component';
import { MilitaryHistoryComponent } from './applicant-workflow/createResume/military-history/military-history.component';
import { LicenceCertificateComponent } from './applicant-workflow/createResume/licence-certificate/licence-certificate.component';
import { CandidateRefrenceComponent } from './applicant-workflow/createResume/candidate-refrence/candidate-refrence.component';
import { ResumePreviewComponent } from './applicant-workflow/createResume/resume-preview/resume-preview.component';
import { ApplicantDataService } from './applicant-workflow/services/applicant-data.service';
import { ApplicantLoginComponent } from './applicant-workflow/existingResume/applicant-login/applicant-login.component';
import { ApplicantRegisterComponent } from './applicant-workflow/existingResume/applicant-register/applicant-register.component';
import { ExistingResumeComponent } from './applicant-workflow/existingResume/existing-resume/existing-resume.component'
import { CeiboShare } from 'ng2-social-share';
import { FileUploadComponent } from './applicant-workflow/file-upload/file-upload.component';
import { ObjectiveComponent } from './applicant-workflow/createResume/objective/objective.component';
import { AccomplishmentComponent } from './applicant-workflow/createResume/accomplishment/accomplishment.component';
import { MoreInfoComponent } from './applicant-workflow/more-info/more-info.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ResetpasswordComponent } from './applicant-workflow/existingResume/restpassword/restpassword.component';
import { ApplicantRegisterSucessComponent } from './applicant-workflow/existingResume/applicant-register/applicant-register-sucess/applicant-register-sucess.component';
import { PreviewComponent } from './applicant-workflow/existingResume/preview/preview.component';
import { PasswordSetComponent } from './applicant-workflow/existingResume/password-set/password-set.component';
import {GuardApplicantService} from './applicant-workflow/services/guard-applicant.service';
import {CreateHwaComponent} from './employer_workflow/1_hwa/hwa_quadrant/create_hwa/create-hwa/create-hwa.component';
import {EdithwaComponent} from './employer_workflow/1_hwa/hwa_quadrant/edit_hwa/edit-ad/edithwa.component';
import {ListExtendHwaComponent} from './employer_workflow/1_hwa/hwa_quadrant/extend_hwa/list-extend-hwa.component';
import {ListCopyHwaComponent} from './employer_workflow/1_hwa/hwa_quadrant/copy_hwa/list-copy-hwa.component';
import {ParentQuadrantComponent} from './employer_workflow/1_hwa/parent-quadrant.component';
import {HwaQuadrantScreenComponent} from './employer_workflow/1_hwa/hwa_quadrant/hwa-quadrant-screen.component';
import {ViewadComponent} from './employer_workflow/1_hwa/hwa_quadrant/view_ad/viewad.component';
import {CopyAdComponent} from './employer_workflow/1_hwa/hwa_quadrant/copy_hwa/copy-ad/copy-ad.component';
import {FullHwaOverlayComponent} from './employer_workflow/1_hwa/hwa_quadrant/full-hwa-overlay/full-hwa-overlay.component';
import {ListEditHwaComponent} from './employer_workflow/1_hwa/hwa_quadrant/edit_hwa/list-edit-hwa.component';
import {ExtendHwaComponent} from './employer_workflow/1_hwa/hwa_quadrant/extend_hwa/extend-hwa/extend-hwa.component';
import {AlertComponent} from './misce/alert/alert.component';
import {FloatLabelDirectiveDirective} from './misce/float/float-label-directive.directive';
import {InputMaskDirective} from './misce/inputMask/input-mask.directive';
import {GlobalHeaderComponent} from './misce/global/header/global-header.component';
import {DropdownDirective} from './misce/global/directives/dropdown.directive';
import {SuccessMsgComponent} from './misce/global/success/success-msg.component';
import {CollapsibleDirective} from './misce/global/directives/collapsible.directive';
import {EditableQuestions} from './misce/global/directives/editable-question';
import {FocusElementDirective} from './misce/global/directives/focus-element.directive';
import {OrderByPipe} from './misce/ng_pipes/order-by.pipe';
import {FocuserDirective} from './misce/global/directives/focuser.directive';
import {Format} from './misce/ng_pipes/format.pipe';
import {LoggedinComponent} from './employer_workflow/0_dashboard/loggedin.component';
import {ProfileComponent} from './employer_workflow/0_dashboard/profile/profile.component';
import { ActiveHwaListComponent } from './employer_workflow/2_allapplicant/1_activehwa/active-hwa-list/active-hwa-list.component';
import { XlComponent } from './employer_workflow/2_allapplicant/1_activehwa/downloadXL/xl.component';
import { ApplicantsListComponent } from './employer_workflow/2_allapplicant/2_applicantswhoapplied/applicants-list/applicants-list.component';
import { FeedbackComponent } from './employer_workflow/2_allapplicant/2_applicantswhoapplied/operations/feedback/feedback.component';
import {AllApplicantServicesService} from './employer_workflow/2_allapplicant/services/all-applicant-services.service';
import { ViewApplicationComponent } from './employer_workflow/2_allapplicant/3_viewapplicant/view-application.component';
import {SqueezeBoxModule} from 'squeezebox';
import { ViewresumeComponent } from './employer_workflow/2_allapplicant/viewresume/viewresume.component';
import { SimpleNotificationsModule } from 'angular2-notifications';
import {OrderByFilter} from './misce/ng_pipes/orderby.pipe';


let providers = {
  'google': {
    'clientId': '814273256390-78pvfeih4oqi4ll22sgk5kbiov5i46pd.apps.googleusercontent.com'
  },
  'linkedin': {
    'clientId': '81xlnpbdaf4et2'
  },
  'facebook': {
    'clientId': '303555973402527',
    'apiVersion': 'v2.9'
  }
};

@NgModule({
  declarations: [
    AppComponent,
    OnboardingComponent,
    RegisterComponent,
    RegisterSucessComponent,
    LoginComponent,
    HeaderComponent,
    ResetPasswordComponent,
    ShowHideInput,
    ShowHideCnf,
    AlertComponent,
    FloatLabelDirectiveDirective,
    ForgotPasswordComponent,
    InputMaskDirective,
    MaskedInputDirective,
    LoggedinComponent,
    GlobalHeaderComponent,
    ResetSuccess,
    ProfileComponent,
    DropdownDirective,
    SuccessMsgComponent,
    CollapsibleDirective,
    CreateHwaComponent,
    BreadcrumbTitleComponent,
    OverlayComponent,
    KnockoutQuestionComponent,
    EditableQuestions,
    SkillAndExperienceComponent,
    Step1Component,
    Step2Component,
    Step3Component,
    Step4Component,
    FocusElementDirective,
    EdithwaComponent,
    ListExtendHwaComponent,
    ListCopyHwaComponent,
    ParentQuadrantComponent,
    HwaQuadrantScreenComponent,
    PostHwaComponent,
    FinalOverlayComponent,
    PaypalComponent,
    ActiveAdComponent,
    ViewadComponent,
    CopyAdComponent,
    OrderByPipe,
    FullHwaOverlayComponent,
    FocuserDirective,
    ListEditHwaComponent,
    EdithwaComponent,
    ExtendHwaComponent,
    ApplicantWorkflowComponent,
    ApplicantHeaderComponent,
    LandingComponent,
    KnowMoreComponent,
    FileUploadDirective,
    ApplicantFooterComponent,
    KnockoutQuestionComponent,
    SkillQuestionComponent,
    SucessScreenComponent,
    KoQuestionComponent,
    EmploymentHistoryComponent,
    EducationalDetailsComponent,
    MilitaryHistoryComponent,
    LicenceCertificateComponent,
    CandidateRefrenceComponent,
    ResumePreviewComponent,
    ApplicantLoginComponent,
    ApplicantRegisterComponent,
    ExistingResumeComponent,
    CeiboShare,
    FileUploadComponent,
    ObjectiveComponent,
    AccomplishmentComponent,
    MoreInfoComponent,
    Format,
    ResetpasswordComponent,
    ApplicantRegisterSucessComponent,
    PreviewComponent,
    PasswordSetComponent,
    ActiveHwaListComponent,
    XlComponent,
    ApplicantsListComponent,
    FeedbackComponent,
    ViewApplicationComponent,
    ViewresumeComponent,
    OrderByFilter,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    routing,
    TabModule,
    QuillEditorModule,
    MyDatePickerModule,
    Angular2SocialLoginModule,
    BrowserAnimationsModule,
    SqueezeBoxModule,
    SimpleNotificationsModule.forRoot()
  ],

  providers: [UserService, OverlayDataService, HwaCommonService, AlertService, GardToken, GuardApplicantService,  DataService,
    ApplicantService, ApplicantDataService, AllApplicantServicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
Angular2SocialLoginModule.loadProvidersScripts(providers);

