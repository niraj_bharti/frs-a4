import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from "@angular/forms";
import { UserService } from "../services/user.service";
import { HwaCommonService } from "../services/hwa-common.service";

// import { forEach } from "@angular/router/src/utils/collection";
import { isNumeric } from "rxjs/util/isNumeric";
import { Router } from "@angular/router";
import { AlertService } from "../services/alert.service";
import {IMyOptions, IMyDateModel} from 'mydatepicker';

@Component({
  selector: 'app-post-hwa',
  templateUrl: './post-hwa.component.html',
  styleUrls: ['./post-hwa.component.css']
})
export class PostHwaComponent implements OnInit {
  private post_ad1:any="post_ad1";
  private submitHwaForm:any;
  private datevalue:any;
  private endDatevalue:any;
  private additionalDays: number=0;
  private defaultDays:number = 14;
  private position;
  private selectedNofPostion;
  private posType;
  private defaultAmount:number = 0;
  private additionalAmt:number = 0;
  private totalAmt:number = 0;
  private for7DaysAmt:any = 12.50 ;
  private for14DaysAmt:any= 25;
  private isDteValid:boolean = false;
  private MS_PER_MINUTE = 60000;
  private durationInMinutes = 1;

  private myDatePickerOptions: IMyOptions = {
       dateFormat: 'mm/dd/yyyy',
       showClearDateBtn: false,
       disableUntil:{year: new Date().getFullYear(), month: new Date().getMonth()+1, day: new Date().getDate()-1}
 };

  constructor(
    private alertService: AlertService,
    private router: Router,
    private formbuilder: FormBuilder,
    private userService: UserService,
    private HwaServices: HwaCommonService) { 
    }

  ngOnInit() {

    // console.log('timestamp', new Date(1491071361000));
let user = this.userService.isLogedin();
//console.log(this.date)
 if (localStorage.getItem('storeHwaFormData')) {
      var formdata = JSON.parse(localStorage.getItem('storeHwaFormData'));
      this.position = formdata.title;
      this.selectedNofPostion = formdata.field_how_many_people_do_you_nee;
      this.posType = formdata.field_will_they_be_full_time_par;

      this.defaultAmount = 25;
      this.additionalAmt = 0;
 }


  }

submitHWA() {
if(this.datevalue){

this.endDatevalue = new Date(this.datevalue);
 this.canculateEndDate();

let hwaid = localStorage.getItem('storeHwaNid');
let data = {
"hwa_nid": hwaid,
"start_date":  this.formatDate(this.datevalue), //"2000-01-20 12:00:00",
"end_date": this.endDatevalue,
"extend_date_days": this.additionalDays,
"total_amt":this.totalAmt,
"action":"active"
}
// extended=Extend
console.log(data);
if(hwaid) {
 localStorage.setItem('pamentData', JSON.stringify(data));
  this.router.navigate(['/payment']);
}else{
  this.alertService.success("Your have to crate HWA", true);
  this.router.navigate(['/hwa_workflow']);
}
 
/* this.HwaServices.submitHWA(data).subscribe(
        res => {
         // console.log(res);
          this.alertService.success("Your have submited Help Wanted Ad Successfully", true);
          this.router.navigate(['/payment', this.totalAmt]);
        }); */
}else {
  alert("You need to select valid date");
}
}

selectstartDate(event: any){
  console.log(event.jsdate)

   if(event.jsdate){
  var d1 = new Date();
  var d2 = new Date(event.jsdate);
  if((d1.getDate() == d2.getDate()) && (d1.getMonth() == d2.getMonth() ) && (d1.getFullYear() == d2.getFullYear()) ) {
    this.datevalue =  event.jsdate; 
    this.isDteValid = true;
   }else{
     if(d1.getTime() < d2.getTime()) {
       this.datevalue = event.jsdate;
       this.isDteValid = true;
     }else{
       //alert("You need to select valid date");
       this.isDteValid = false;
     }
   
   }     
  }

if(this.datevalue){
 this.endDatevalue = new Date(this.datevalue);
 this.canculateEndDate();
 this.defaultAmount = 25.00;
 if(this.additionalDays == 0) {
  this.additionalAmt = 0;
 }else if(this.additionalDays == 7) {
  this.additionalAmt = this.defaultAmount / 2;
 }else if(this.additionalDays == 14) {
  this.additionalAmt = this.defaultAmount;
 }
 this.totalAmt = Number(this.defaultAmount) + Number(this.additionalAmt);
  }else{
    alert("Please Select Date.");
  }
}

canculateEndDate(){

 if(this.additionalDays != 0){
  var dafutDay = Number(this.defaultDays) + Number(this.additionalDays);
  this.endDatevalue.setDate(this.endDatevalue.getDate() + (dafutDay));
  this.endDatevalue = new Date(this.endDatevalue.getTime() - this.durationInMinutes * this.MS_PER_MINUTE);
  this.endDatevalue = this.formatDate(this.endDatevalue);
 }else{

  this.endDatevalue.setDate(this.endDatevalue.getDate() + (this.defaultDays));
  this.endDatevalue = new Date(this.endDatevalue.getTime() - this.durationInMinutes * this.MS_PER_MINUTE);
  this.endDatevalue = this.formatDate(this.endDatevalue);
 }

}
 formatDate(date) {
  
  
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        h = '' + d.getHours(),
        m = '' + d.getMinutes(),
        s = '' + d.getSeconds();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    if (h.length < 2) h = '0' + h;
    if (m.length < 2) m = '0' + m;
    if (s.length < 2) s = '0' + s;
    if(s == '00'){s = '01'}
    let dat = [year, month, day].join('-');
    var dateFromat = dat +" "+[h, m, s].join(':');

  return dateFromat;
}
}
