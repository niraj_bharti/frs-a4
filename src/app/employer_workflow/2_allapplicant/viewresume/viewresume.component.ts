import { Component, OnInit } from '@angular/core';
import {SafeUrl} from '@angular/platform-browser';
import { DomSanitizer} from '@angular/platform-browser';
import {ActivatedRoute, Params, Router} from '@angular/router';
@Component({
  selector: 'app-viewresume',
  templateUrl: './viewresume.component.html',
  styleUrls: ['./viewresume.component.css']
})
export class ViewresumeComponent implements OnInit {
  private resumeUrl: SafeUrl;
  private getValue;
  private getFull;
  private uri;
  constructor(private sanitizer: DomSanitizer, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    //
    this.activatedRoute.params.subscribe((params: Params) => {
      let applicantId = params['url'];
      this.uri = applicantId;
    });
    this.getValue = this.uri;
    this.getFull = 'https://docs.google.com/viewerng/viewer?url=' + this.getValue + '&embedded=true';
    this.resumeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.getFull);
  }

  goBack() {
    history.back();
  }

}
