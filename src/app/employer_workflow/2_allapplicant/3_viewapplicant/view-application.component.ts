import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AllApplicantServicesService} from '../services/all-applicant-services.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {NotificationsService} from 'angular2-notifications/dist';

@Component({
  selector: 'app-view-application',
  templateUrl: './view-application.component.html',
  styleUrls: ['./view-application.component.css', '../allApp.css']

})
export class ViewApplicationComponent implements OnInit {
  private applicantId;
  private HwaId;
  public openThis: boolean;
  public openThis2: boolean;
  private koList = [];
  private skillList = [];
  private basicInfo = [];
  private resumeInfo = [];
  private firstName;
  private lastName;
  private phoneNumber;
  private city;
  private state;
  private zip;
  private email;
  private positionName;
  private employerData = [];
  private educationalData = [];
  private licenceData = [];
  private refData = [];
  private militryData = [];
  private Rank;
  private yearServed;
  private dateOfDischarge;
  private branch;
  private pageLoded: boolean;
  private checkType;
  private resumeBuilder: boolean;
  private resumeFile: boolean;
  private ifDocument;
  private documentUrl;
  public options = {
    position: ['top', 'center'],
    timeOut: 5000,
    lastOnBottom: true
  };
  constructor(private allApplicantservice: AllApplicantServicesService, private router: Router,
              private activatedRoute: ActivatedRoute, private _eref: ElementRef, private _notificationsService: NotificationsService) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe((params: Params) => {
      let applicantId = params['applicantId'];
      let HwaId = params['hwaId'];
      let HwaName = params['hwaName'];
      this.applicantId = applicantId;
      this.HwaId = HwaId;
      this.positionName = HwaName;
    });
    this.fetchApplication();

  }

  // Fetch Application
  fetchApplication() {
    let Uid = {
      'uid': this.applicantId,
      'hwa_id': this.HwaId
    };
    this.allApplicantservice.getApplication(Uid).subscribe(
      res => {
        console.log(res)
        this.checkType = res.applicant_resume_detail.field_resume_type[0].value;
        this.basicInfo = res.user_detail;
        this.koList = res.skill_ko_result.ko_result;
        this.skillList = res.skill_ko_result.skill_result;
        this.resumeInfo = res.applicant_resume_detail;
        console.log(this.checkType)


        // Basic Information
        this.firstName = res.user_detail.field_first_name[0].value;
        this.lastName = res.user_detail.field_last_name[0].value;
        this.phoneNumber = res.user_detail.field_phone_number[0].value;
        this.city = res.user_detail.field_city[0].value;
        this.state = res.user_detail.field_state[0].value;
        this.zip = '12345'; // res.user_detail.field_zip[0].value;
        this.email = res.user_detail.mail[0].value;

        //
        if (this.checkType !== 'File') {
          this.employerData = res.applicant_resume_detail.field_employment_history;
          this.educationalData = res.applicant_resume_detail.field_education_details;
          this.licenceData = res.applicant_resume_detail.field_licenses_certification;
          this.militryData = res['applicant_resume_detail'].field_military_history;
          this.refData = res.applicant_resume_detail.field_references;
          this.branch = res.applicant_resume_detail['field_military_history'][0].field_rank_at_the_time_of_discha[0].value;
          this.yearServed = res.applicant_resume_detail['field_military_history'][0].field_number_of_years_served[0].value;
          this.Rank = res.applicant_resume_detail['field_military_history'][0].field_branch_of_military_served_[0].value;
          this.dateOfDischarge = this.convertTimestapm(res.applicant_resume_detail['field_military_history'][0].field_date_of_your_discharge[0].value)
          this.resumeBuilder = true;
        } else {
          this.resumeFile = true;
          this.ifDocument = res.applicant_resume_detail.field_resume_upload[0].file_name;
          this.documentUrl = res.applicant_resume_detail.field_resume_upload[0].url;
        }
       //
        this.pageLoded = true;
      }
    );
  }

  // toggle class
  toggleMenu() {

      this.openThis = !this.openThis;
  }
  toggleMenu2() {
    this.openThis2 = !this.openThis2;
  }

  convertTimestapm(timestamp) {
    return new Date(timestamp * 1000);
  }
  viewResume() {
    this.router.navigate(['/viewresume', this.documentUrl]);
  }

  nextAction() {
    this._notificationsService.info(
      'Status Changed!',
      'This Candidate is Shortlisted!',
      {
        timeOut: 3500,
        showProgressBar: true,
        pauseOnHover: false,
        clickToClose: false
      }
    );
  }

  // Print This
  print() {
    window.print();
  }

}
