import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/Rx';
import { Observable } from 'rxjs';
import {Subject} from 'rxjs/Subject';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {UserService} from '../../../services/user.service';


@Injectable()

export class AllApplicantServicesService {
  public switchUrl = this.userService.switchUrl;
  private fetchStatusAllApplicantApi = this.switchUrl + 'recruitment_flow_stats.json';
  private summeryOfActiveHwaApi = this.switchUrl + 'summary_of_active_hwa.json';
  private all_applicantsApi = this.switchUrl + 'all_applicant.json';
  private getApplicationApi = this.switchUrl + 'load_resume.json';

  constructor(private http: Http, private router: Router, private userService: UserService) {

  }



  // CORS SEtting
  authHeaders() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    return headers;
  }

  checkQuardrantInfo(empUid) {
    return this.http.post(this.fetchStatusAllApplicantApi, empUid,  { headers: this.authHeaders() })
      .map((res: Response) => res.json())
      .catch((error: any) => error.json());
  }
  summeryOfActiveHwa(uid) {
    return this.http.post(this.summeryOfActiveHwaApi, uid,  { headers: this.authHeaders() })
      .map((res: Response) => res.json())
      .catch((error: any) => error.json());
  }

  getAllApplicant(hwaNid) {
    return this.http.post(this.all_applicantsApi, hwaNid,  { headers: this.authHeaders() })
      .map((res: Response) => res.json())
      .catch((error: any) => error.json());
  }

  getApplication(applicantId) {
    return this.http.post(this.getApplicationApi, applicantId,  { headers: this.authHeaders() })
      .map((res: Response) => res.json())
      .catch((error: any) => error.json());
  }

}
