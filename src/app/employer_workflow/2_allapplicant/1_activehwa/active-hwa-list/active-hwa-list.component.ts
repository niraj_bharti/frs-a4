import { Component, OnInit } from '@angular/core';
import {AllApplicantServicesService} from '../../services/all-applicant-services.service';
import {UserService} from '../../../../services/user.service';
import {Router} from '@angular/router';
import {NotificationsService} from 'angular2-notifications/dist';

@Component({
  selector: 'app-active-hwa-list',
  templateUrl: './active-hwa-list.component.html',
  styleUrls: ['./active-hwa-list.component.css', '../../allApp.css']
})
export class ActiveHwaListComponent implements OnInit {

  private listData = [];
  private HwaId;
  private pageLoded: boolean;
  private status = 'Sort by Ad';
  private  order = 'extendtodate';
  private ascending: boolean;
  public options = {
    position: ['top', 'center'],
    timeOut: 5000,
    lastOnBottom: true
  };
  constructor(
    private allApplicantApi: AllApplicantServicesService,
    private userservice: UserService,
    private router: Router,
    private _notificationsService: NotificationsService
  ) { }

  ngOnInit() {
    this.HwaId = 10;
    this.fetchList();
  }

  // Get List of All active HWA that applicant applied
  fetchList() {
    let user = this.userservice.isLogedin();
    let empid = {'uid': user.uid };
    console.log(user.uid)
    this.allApplicantApi.summeryOfActiveHwa(empid).subscribe(
      res => {
        this.listData = res;
        console.log(res);
        this.pageLoded = true;
      }
    );
  }

  // Get HwaIdInfo
  whoApplied() {
    this.router.navigate(['whohasapplied', 1004]);
  }

  // Shorting Pipes
  shortBy() {
    if  (this.status === 'Sort by Expiration Date') {
    this.status = 'Sort by Ad';
    this.order = 'extendtodate';
    this.ascending = true;
      this._notificationsService.info(
        'Sort by Expiration Date!',
        'List Sorted by Expiration Date!',
        {
          timeOut: 3500,
          showProgressBar: true,
          pauseOnHover: false,
          clickToClose: false
        }
      );
    } else {
      this.status = 'Sort by Expiration Date';
      this.order = 'title';
      this.ascending = true;
      this._notificationsService.info(
        'Sort by Ad!',
        'List sorted alphabetically! ',
        {
          timeOut: 3500,
          showProgressBar: true,
          pauseOnHover: false,
          clickToClose: false
        }
      );
    }
  }
}
