import { Component, OnInit } from '@angular/core';
import {AllApplicantServicesService} from '../../services/all-applicant-services.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
@Component({
  selector: 'app-applicants-list',
  templateUrl: './applicants-list.component.html',
  styleUrls: ['./applicants-list.component.css', '../../allApp.css']
})
export class ApplicantsListComponent implements OnInit {
  private HwaId;
  private hwaName;
  private listData = [];
  private listDataNew = [];
  private listDataViewed = [];
  private listDataShortlisted = [];
  private listDataRejected = [];
  private scoringNumber;
  private pageLoded: boolean;
  constructor(private allApplicantservice: AllApplicantServicesService, private router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    // subscribe to router event
    this.activatedRoute.params.subscribe((params: Params) => {
      let userId = params['hwaId'];
      let hwaName = params['hwaName'];
      this.HwaId = userId;
      this.hwaName = hwaName;
    });
    //
    this.fetchApplicant();
  }
  // Fetch List of all Applicant who has applied for this HWA
  fetchApplicant() {
    let hwaId = {
      'hwa_id': this.HwaId
    };
    this.allApplicantservice.getAllApplicant(hwaId).subscribe(
      res => {
        console.log(res.all_applicant_list);
        this.listData = res.all_applicant_list.all;
        this.listDataNew = res.all_applicant_list['New'];
        this.listDataViewed = res.all_applicant_list['Viewed'];
        this.listDataShortlisted = res.all_applicant_list['Viewed'];
        this.listDataRejected = res.all_applicant_list['Viewed'];
        this.scoringNumber = this.getRandomInt(10, 99);
        this.pageLoded = true;
      }
    );
  }

  // Random Number for scoring engine
  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

}
