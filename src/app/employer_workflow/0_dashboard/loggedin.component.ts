import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { UserService } from '../../services/user.service';
import { DataService } from '../../services/data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-loggedin',
  templateUrl: './loggedin.component.html',
  styleUrls: ['./loggedin.component.css']
})
export class LoggedinComponent implements OnInit {
  private greeting = '';
  public companyname = '';
  public firstName = '';
  private displayLogedIn: boolean;

  constructor(private router: Router, public userService: UserService, private dataService: DataService,
              private domSanitizer: DomSanitizer) { }
  goToprofile() {
    this.router.navigate(['/UserProfile']);
  }
  gotoHwa() {
    this.router.navigate(['/createHWA']);
  }
  ngOnInit() {
    let user = this.userService.isLogedin();

    if (user) {
      let uidObj = {'user_id': user.uid};
      this.dataService.getDashboardData(uidObj).subscribe(
        res => {
          this.greeting = res.greeting;
          this.firstName = res.f_name;
          this.displayLogedIn = true;
        });
    }
  }
}
