import {Component, OnInit, Input, ElementRef, Inject} from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray} from '@angular/forms';
import { Router } from '@angular/router';
import {toArray} from 'rxjs/operator/toArray';
import { DataService } from '../../../services/data.service';
import { AlertService } from '../../../services/alert.service';
import { UserService } from '../../../services/user.service';
import {DOCUMENT} from '@angular/platform-browser';
declare let $: any;
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public  profileForm: FormGroup;
  public myModel = ''
  public mask = [/[0-9]/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public zipmask = [/[0-9]/, /\d/, /\d/, /\d/, /\d/];
  @Input() field_name_of_your_business = '';
  @Input() field_first_name = '';
  @Input() field_last_name = '';
  @Input() field_phone_number = '';
  @Input() field_your_website_address = '';
  @Input() field_your_social_media_url = '';
  @Input() address: Array<string> = [];
  @Input() members: Array<string> = [];
  @Input() email = '';
  private alertmsg: boolean;
  private deleteAddress: Array<any> = [];
  private displayProfile: boolean;
  private getCityinfoList= []
  private validZipStatus: string;
  private showSelect: Array<boolean> =[];
  private businessName: string;
  private showuserbox: boolean;
  private inviteUserForm: FormGroup;
  private userListArray = [];
  private borderClass = '';
  private addressFromgroup: FormGroup;
  public emailMask = /^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  public maxlengthofCar:number = 100;

  constructor(private formbuilder: FormBuilder,
              private el: ElementRef,
              private dataService: DataService,
              private userService: UserService,
              private alertService: AlertService,
              private router: Router,
              @Inject(DOCUMENT) private document: Document
  ) { }
  ngOnInit() {

    this.initFields();
    this.inviteUserForm = this.formbuilder.group({
      'name': ['', Validators.required],
      'lname': ['', Validators.required],
      'mail': ['', Validators.compose([Validators.required, Validators.pattern(this.emailMask)])]
  });

    let user  = this.userService.isLogedin();
    this.loadUserFormData(user);
    this.getUserLists(user);
  }

  loadUserFormData(userdata) {

    this.emptyAddresses();
    let userId = {'uid': userdata.uid };
    this.userService.accesData(userId).subscribe(
      res => {
        if (res['status']) {
          if (res['address'].length > 0) {
            this.borderClass = '';
            if (this.router.url !== '/UserProfile') {
              this.router.navigate(['/employer_dashboard', '0']);
            }
          } else {
            this.borderClass = 'redborder';
          }
// load details
          let user =  res;
          localStorage.setItem('currentUser', JSON.stringify(res));
          this.userService.setLogedIn(true);
          if (user['details'].field_name_of_your_business[0]) {
            this.field_name_of_your_business = user['details'].field_name_of_your_business[0].value;
            this.businessName = user['details'].field_name_of_your_business[0].value;
          }
          if (user['details'].field_first_name[0]) {
            this.field_first_name = user['details'].field_first_name[0].value;
          }
          if (user['details'].field_last_name[0]) {
            this.field_last_name = user['details'].field_last_name[0].value;
          }
          if (user['details'].field_phone_number[0]) {
            this.field_phone_number = user['details'].field_phone_number[0].value;
          }
          if (user['details'].field_your_website_address[0]) {
            this.field_your_website_address = user['details'].field_your_website_address[0].value;
          }
          if (user['details'].field_your_social_media_url[0]) {
            this.field_your_social_media_url =  user['details'].field_your_social_media_url[0].value;
          }
          if (user['details'].name[0]) {
            this.email =  user['details'].name[0].value;
          }
// load addresses
          if (user['address'].length) {
            for (let i = 0; i <= user['address'].length - 1; i++) {
              let Obj = user['address'][i];
              this.addAddress(Obj.nid, Obj.title, Obj.field_address_line_2, Obj.field_z, Obj.field_state, Obj.field_city);
            }
          } else {
            this.addAddress('', '', '', '', '', '');
          }
          this.displayProfile = true;
        } else {this.addAddress('', '', '', '', '', ''); }
      },
      error => {
      }
    );
  }

  initAddress(nid, addr1, addr2, zip, state, city) {
    this.addressFromgroup = this.formbuilder.group({
      'nid': [nid],
      'title': [addr1, Validators.required],
      'field_address_line_2': [addr2],
      'field_z': [zip, Validators.pattern(/(^\d{5}$)|(^\d{5}-\d{4}$)/)],
      'field_state': [state, Validators.required],
      'field_city': [city, Validators.required]

    });
    return this.addressFromgroup;
  }

  zipValidator(zip) {
    let valid = /^\d{5}$/.test(zip.value);
    if (valid) {
      return null;
    }
    return {'invalidZip': true};
  }

  addAddress(nid, addr1, addr2, zip, state, city) {
    // add address to the list
    const control = <FormArray>this.profileForm.controls['address'];
    control.push(this.initAddress(nid, addr1, addr2, zip, state, city));
  }
  removeAddress(i: number) {
    const control = <FormArray>this.profileForm.controls['address'];
    this.deleteAddress.push({'nid': control.value[i]['nid']});
    control.removeAt(i);
  }
  emptyAddresses() {
    const control = <FormArray>this.profileForm.controls['address'];
    let leng = control.value.length - 1;
    for (let j = leng; j >= 0; j--) {
      control.removeAt(j);
    }
  }
  onSubmit() {
    let udata = this.profileForm.value;
    let user = this.userService.isLogedin();
    this.alertmsg = true;
    udata.uid = user.uid;
    udata.address_delete = this.deleteAddress;
    this.dataService.getUserProfile(udata).subscribe(
      res => {
        this.loadUserFormData(user);
        this.alertmsg = true;
        this.alertService.success('Your Profile Saved Successfully', true);

        this.autoHideAlertMsg(6000);
        setTimeout(() => {
          this.router.navigate(['/']);
        }, 1000);
      },
      error => {
        this.alertService.error(error);
        // console.log(this.members[0]);
      });
  }
  autoHideAlertMsg(time) {
    setTimeout(() => {
      this.alertmsg = false;
    }, time);
  }
  initFields() {
    this.profileForm = this.formbuilder.group({
      'field_name_of_your_business': ['', Validators.required],
      'field_first_name': ['', Validators.required],
      'field_last_name': [''],
      'email': ['', Validators.required],
      'field_phone_number': ['', Validators.required],
      'field_your_website_address': [''],
      'field_your_social_media_url': [''],
      'address': this.formbuilder.array([
      ])
    });
  }


  zipCodeSearch(count) {
    let zipc = this.profileForm.value.address[count].field_z;
    if (!isNaN(zipc)) {

      this.userService.searchZip(zipc).subscribe(
        res => {
          if (this.validUsCountry(res)) {
            if (res['results'][0].postcode_localities) {
              this.showSelect[count] = true;
              this.getCityinfoList = [];
              for (let j = 0; j < res['results'][0].postcode_localities.length; j++) {
                this.getCityinfoList.push(res['results'][0].postcode_localities[j]);
              }
            } else {
              this.showSelect[count] = false;
            }

            for (let i = 0; i <= res['results'][0].address_components.length - 1; i++) {
              if (res['results'][0].address_components[i].types[0] === 'administrative_area_level_1') {
                let getstate = res['results'][0].address_components[i].short_name;
                let getCity = res['results'][0].address_components[1].long_name;
                //  const control = <FormArray>this.profileForm.controls['address'];
                let arryobj = [];
                arryobj[count] = {field_state: getstate, field_city: getCity};
                this.profileForm.controls['address'].patchValue(arryobj);

                this.validZipStatus = '';
              }
            }
          } else {
            this.validZipStatus = 'Please enter valid US zip code';
            this.showSelect[count] = false;
            let arryobj = [];
            arryobj[count] = {field_state: '', field_city: ''};
            this.profileForm.controls['address'].patchValue(arryobj);
          }
        });
    } else {
      this.validZipStatus = 'Please enter valid US zip code';
      this.showSelect[count] = false;
      let arryobj = [];
      arryobj[count] = {field_state: '', field_city: ''};
      this.profileForm.controls['address'].patchValue(arryobj);
    }
  }
  validUsCountry(res): boolean {
    let isUs = false;
    if (res['status'] === 'OK') {
      for (let i = 0; i <= res['results'][0].address_components.length - 1; i++) {
        if (res['results'][0].address_components[i].short_name === 'US') {
          isUs = true;
          break;
        }
      }
    }
    return isUs;
  }

  addnewUserbox() {
    this.showuserbox = true;
    $('.firstthingf').focus();
  }

  /* ---------------------------------------------------------------------------

   Invite User
   ------------------------------------------------------------------------------ */
  getUserLists(user): any {

    this.dataService.getinvitedUserList(user.uid).subscribe(
      res => {
        this.featchUserLists(res);
        if (res) {
          ///  this.userListArray = res;
        }else {
          //    this.userListArray = [];
        }
      });
  }

  inviteUser(inviteUser, isValid) {

    let user  = this.userService.isLogedin();
    let invaiteuserObj = {
      'uid': user.uid,
      'members': [{
        'name': inviteUser.name,
        'lname': inviteUser.lname,
        'mail': inviteUser.mail
      }]
    };
    this.alertmsg = true;
    this.dataService.sendInviteUsers(invaiteuserObj).subscribe(
      res => {
        if (res['account_created'] === null) {
          this.alertmsg = true;
          this.alertService.error('Email Already Exists.', true);
        } else {
          this.alertmsg = true;
          this.alertService.success('User Invited Successfully', true);
        }


        this.featchUserLists(res);
        this.inviteUserForm.reset();
        this.showuserbox = false;

        //


        this.autoHideAlertMsg(6000);

      });
  }

  featchUserLists(res) {
    if (res['member_list'][0]) {
      this.userListArray = [];
      let tempuList = [];
      let ulist = res['member_list'][0];
      for (let ul in ulist ) {
        tempuList.push(ulist[ul]);
      }
      // });
      this.userListArray =  tempuList.slice().reverse();
    }
  }

  collapsAddrss(num) {
    // console.log(num);
  }
}
