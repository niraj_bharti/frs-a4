import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AlertService} from "../../../../services/alert.service";
import {UserService} from "../../../../services/user.service";
import {HwaCommonService} from "../../../../services/hwa-common.service";


@Component({
  selector: 'app-edit-hwa',
  templateUrl: './list-edit-hwa.component.html',
  styleUrls: ['./list-edit-hwa.component.css']
})
export class ListEditHwaComponent implements OnInit {
  private editthing:any="editthing";
private hwaDraftArray = [];
private saveHwaArray = [];
private expiredHwaArray = [];
private displayListing = false;
  private alertmsg: boolean = false;
  private showStyle: number
  private showStyleExp: number
  private showStyleDraft: number
  private changeTexthwa: string = "keyboard_arrow_down";
  private changeTexthwaExp: string = "keyboard_arrow_down";
  private changeTexthwaDraft: string = "keyboard_arrow_down";

  private showListofSaved:boolean=false;
  private showListofExp:boolean=false;
  private showListofDraft:boolean=false;
  private discardOverlay:boolean = false;
  private currHwaId:any;
  constructor(
    private alertService: AlertService,
    private router:Router,
    private userService: UserService,
    private HwaServices: HwaCommonService) { }

  ngOnInit() {

   localStorage.removeItem('prof1_img_nid');
   localStorage.removeItem('prof2_img_nid')
   localStorage.removeItem('prof3_img_nid')
   localStorage.removeItem('prof4_img_nid');
   localStorage.removeItem('copyHwaNid');
   localStorage.removeItem('storeHwaNid');
   localStorage.removeItem('storeHwaFormData');
    localStorage.removeItem('profileNid');
    localStorage.removeItem('pamentData');
   this.getHWAList();
  }

getHWAList() {
  let user = this.userService.isLogedin();

   this.HwaServices.getAllHWAList(user.uid).subscribe(
        res => {
          console.log(res);
          this.displayListing = true;
          this.hwaDraftArray = res['hwa_draft'];
          this.saveHwaArray = res['hwa_published'];
          // this.expiredHwaArray = res['hwa_expired_listing'];
          this.toggleHighlight(1);
        });
}

makeAcopyOfHWA(hwaid){
  //alert(hwaid);
  this.router.navigate(['makeAedit', hwaid, '']);
  // this.router.navigate(['jobdescription', hwaid]);

}

viewHWA(hwaid, expDate){
  this.router.navigate(['viewad', hwaid, expDate, 'Edit'])
}

editHWA(hwaid, draftEdit) {

this.router.navigate(['makeAedit', hwaid, draftEdit])
}
copyFromEditHWA(hwaid) {

this.router.navigate(['makeAcopy', hwaid])
}
  toggleHighlight(newValue: number) {
    this.changeTexthwa = "keyboard_arrow_down"
    if (this.showStyle === newValue) {
      this.showStyle = 0;
      this.showListofSaved = false;
      this.changeTexthwa = "keyboard_arrow_down"
    }
    else {
      this.showStyle = newValue;
      this.showListofSaved = true;
      this.changeTexthwa = "keyboard_arrow_up"
    }
  }

toggleHighlightDraft(newValue: number) {
    this.changeTexthwaDraft = "keyboard_arrow_down"
    if (this.showStyleDraft === newValue) {
      this.showStyleDraft = 0;
      this.showListofDraft = false;
      this.changeTexthwaDraft = "keyboard_arrow_down"
    }
    else {
      this.showStyleDraft = newValue;
      this.showListofDraft = true;
      this.changeTexthwaDraft = "keyboard_arrow_up"
    }
  }

  toggleHighlightExp(newValue: number) {
    this.changeTexthwaExp = "keyboard_arrow_down"
    if (this.showStyleExp === newValue) {
      this.showStyleExp = 0;
      this.showListofExp = false;
      this.changeTexthwaExp = "keyboard_arrow_down"
    }
    else {
      this.showStyleExp = newValue;
      this.showListofExp = true;
      this.changeTexthwaExp = "keyboard_arrow_up"
    }
  }
  sorting: any = {
    column: 'field_hwa_post', //to match the variable of one of the columns
    descending: false
  };
  columns: any[] = [
    {
      display: 'Column 1', //The text to display
      variable: 'field_hwa_post', //The name of the key that's apart of the data array
      filter: 'text' //The type data type of the column (number, text, date, etc.)
    },
   /* {
      display: 'Column 2', //The text to display
      variable: 'Amount', //The name of the key that's apart of the data array
      filter: 'decimal : 1.0-2' //The type data type of the column (number, text, date, etc.)
    },*/
    {
      display: 'Column 3', //The text to display
      variable: 'created', //The name of the key that's apart of the data array
      filter: 'dateTime' //The type data type of the column (number, text, date, etc.)
    }
  ];
    selectedClass(columnName): any{
    return columnName == this.sorting.column ? 'sort-' + this.sorting.descending : false;
  }

  changeSorting(columnName): void{
    var sort = this.sorting;
    if (sort.column == columnName) {
      sort.descending = !sort.descending;
    } else {
      sort.column = columnName;
      sort.descending = false;
    }
  }

  convertSorting(): string{
    return this.sorting.descending ? '-' + this.sorting.column : this.sorting.column;
  }

  myDate(datestr):any {
    return new Date(Date.parse(datestr));
  }

  parseDate_BK(input) {
  var parts = input.split('-'); //'dd-mm-yyyy'
  // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
  return new Date(parts[2], parts[0]-1, parts[1]); // Note: months are 0-based
}

  parseDate(input) {
    var k;
    if (Date.parse(input)) {
      k = new Date(input);
    }else{
      k = '';
    }
  return  k;
}
//--------------------------discard-------------------------
confirmDiscard(hwaId){
  this.currHwaId = hwaId;
  this.discardOverlay = true;
}

discardHwa(){

let discardHwaObj = { "hwa_nid":[this.currHwaId ] }
// http://test-frslive.pantheonsite.io/hwa_discard.json
this.alertmsg = true;
 this.HwaServices.discardHwa(discardHwaObj).subscribe(
  res => {
  console.log(res);
 this.currHwaId = '';
  this.discardOverlay = false;
  if(res['hwa_discard'] == 'deleted'){
    this.getHWAList();
     this.alertService.success("deleted Successfully", true);
    this.autoHideAlertMsg(3000);
  }

  })
 // "hwa_discard": "deleted"


}

autoHideAlertMsg(time) {
  setTimeout(() => {
    this.alertmsg = false;

  }, time)
}

closeWin() {
  this.currHwaId = '';
  this.discardOverlay = false;

}
}
