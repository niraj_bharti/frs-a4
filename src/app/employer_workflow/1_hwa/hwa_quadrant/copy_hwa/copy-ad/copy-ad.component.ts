import { Component, OnInit, NgZone, ElementRef, ViewChild, Renderer } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from "@angular/forms";

import { DomSanitizer } from "@angular/platform-browser";

import { forEach } from "@angular/router/src/utils/collection";
import { toArray } from "rxjs/operator/toArray";
import { isNumber } from "util";
import { isNumeric } from "rxjs/util/isNumeric";

import { Router, ActivatedRoute } from "@angular/router";

import {Subscription } from 'rxjs';
import {EditableQuestions} from "../../../../../misce/global/directives/editable-question";
import {AlertService} from "../../../../../services/alert.service";
import {OverlayDataService} from "../../../../../services/overlay-data.service";
import {UserService} from "../../../../../services/user.service";
import {HwaCommonService} from "../../../../../services/hwa-common.service";


// import { Format } from './../../../../Pipes/format.pipe';
declare var $: any;
@Component({
  selector: 'app-copy-ad',
  templateUrl: './copy-ad.component.html',
  styleUrls: ['./copy-ad.component.css']
})
export class CopyAdComponent implements OnInit {
  private copyHWA2:any="copyHWA2";
  private oldHwaId:any='';
   private showDeleteBtn:boolean;
  @ViewChild('jd') jd:ElementRef;
  @ViewChild('ko') ko:ElementRef;
  @ViewChild('skill') skill:ElementRef;
  @ViewChild('buz') buz:ElementRef;
  private showFinalHWA:boolean;
  private buzProfine:any;
   private waitingImg:string = "data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==";
   private toolbarOptions = [
    'bold',
    'italic',
    'underline',
    { 'align': '' },
    { 'align': 'center' },
    { 'align': 'right' },
    { 'list': 'ordered' },
    { 'list': 'bullet' },
    { 'indent': '-1' },
    { 'indent': '+1' }
  ];
  public position = '';
  public posType = 'Full-Time';
  public describePos = '';
  public describeSkill = '';
  public jobDescription = {
    modules: {
      toolbar: this.toolbarOptions
    },
    placeholder: `Some things to consider in your job description;
•   Clearly spell out what the job's responsibilities will be.
•   It can be helpful to let the job seeker know who they will be interacting with.
•   Do you have an interesting work environment, make sure you describe what that is.
•   If  there are unique shifts or days to be worked, be sure to make mention of what they are
Remember to paint an accurate and enticing picture to ensure you attract candidates that are the best fit for you.

`
  };
  public jobskills = {
    modules: {
      toolbar: this.toolbarOptions
    },
    placeholder: `Some things to consider as you list Skills and Experience needed.
•	  What are the attributes that successful employees have had in this role.
•	  Are there any specific licenses or certifications the candidate needs to possess to do this job?
•	  Carefully weigh years of experience versus skill level, which is the best way to gauge the candidate's qualifications?
Be careful not to list out your wish for your "dream candidate", focus on the items that are most important to get the job done properly.

`
  };
  public HelpText = [
    {
      "label": "Don't want to waste time looking at a application from Candidates that don't meet your minimum requirements?"
    },
    {
      "label": "Take a few minutes to add Knockout Questions."
    }

  ];
  public city: any;
  public addressTitle:any;
  public state: any;
  public HwaForm: FormGroup;
  public addLocationForm: FormGroup;
  private noLocation: boolean = false;
  private multiLocation: boolean = false;
  private title: any;
  private location: any[];
  private checkboxval: boolean = false;
  private passAddressToWin: any[];
  private nodeId: any = [];
  private showDialog: boolean = false;
  private launchPopup: boolean = false;
  private emptyLocation: boolean = false;

  public myModel = ''
  public mask = [/[0-9]/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public zipmask = [/[0-9]/, /\d/, /\d/, /\d/, /\d/];
  private displayLocation: boolean=true;
  private alertmsg: boolean = false;
  private showStyle: number
  private changeTexthwa: string = "keyboard_arrow_down";
  private getstateinfo: string = '';
  private getCityinfo: string = '';
  private getCityinfoList = []
  private validZipStatus: string;
  private showSelect: Array<boolean> = [];
  private hideInput: boolean = false;
  private selectedNofPostion: any = "1";
  private shoBasicHwa: boolean = true;
  private fromHwa: any = "fromHwa";
  private validForOneAddrs: boolean;
  private myCompeyName: any;
  private noPostionval: any;
  private level2: boolean = false;
  private positionError: boolean = false;
  private submitAttempt: boolean = false;
  private addresvalid: boolean = false;
  private addressLenght1: number = 0;
  private checkValidity: boolean;
  private selectedLoc = [];
  private subscription: Subscription;
//-------------ko----------------------

 private defaultQLists=[];
  private customQList=[];
  private isEditable =[];
  private showTextfield:boolean;
  private enb:boolean;
  //private showDialog:boolean = false;
  private showKo:boolean;
  public koForm:FormGroup;
  private alertmsgko:boolean;

  private hwaId:number;
  private customlist:any;
  private displayQuestionn:boolean = true;
  private changeText = []; //"Edit";
  @ViewChild(EditableQuestions) input: EditableQuestions;
  private koData:any="koData";
  private show = false;
  private koDataStr:string="koData";
  private qusInpitValue:any;
  private level2ko:boolean = false;
  private btndisabled:boolean;
  //-------------Skill---------------------------------
  public skillExpForm:FormGroup;
  private skillDataStr:string="skillData";
  private showskill:boolean;
  private fromSkill:any="fromSkill";
  private hideExpArray = [];
  private hideExpertiseArray = [];
//-----------------------Business Profile---------------------
  private showProfile: boolean;
  private proFileObj: any;
  private image1Url:any;
  private image2Url:any;
  private image3Url:any;
  private image4Url:any;

  private titalText1:any;
  private titalText2:any;
  private titalText3:any;
  private titalText4:any;

  private bodyText1:any;
  private bodyText2:any;
  private bodyText3:any;
  private bodyText4:any;
//--------------------------

  private titalText1_isEdit:boolean = false;
  private titalText2_isEdit:boolean = false;
  private titalText3_isEdit:boolean = false;
  private titalText4_isEdit:boolean = false;

  private bodyText1_isEdit:boolean = false;
  private bodyText2_isEdit:boolean = false;
  private bodyText3_isEdit:boolean = false;
  private bodyText4_isEdit:boolean = false;

  private imgbase64src:any;
  private isThereImge:boolean;
  private imgLoading:boolean;
  private filesToUpload: Array<File>;
  private alldataofHwa:any;
  private ImageNumber:number = 0;
  private MAX_LENGTH = 1000;
  private currProfileNid:any;
private newHwaId:any;
  constructor( private ele: ElementRef,
    private alertService: AlertService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private renderer: Renderer,
    private hwaOverlayService: OverlayDataService,
    private formbuilder: FormBuilder,
    private userService: UserService,
    private HwaServices: HwaCommonService,
    private domSanitizer: DomSanitizer) { }

   ngOnInit() {
    let user = this.userService.isLogedin();
    //console.log(user.details.field_name_of_your_business[0].value)
    this.myCompeyName = user.details.field_name_of_your_business[0].value;
    this.HwaForm = this.formbuilder.group({
      'position': ['', Validators.required],
      'numberOfPosition': ['1'],
      'position_type': [''],
      'describePosition': ['', Validators.required],
      'describeSkill': ['', Validators.required],
      'locations': this.formbuilder.array([]),
    }, { validator: this.iScheckedLocation });
    //This form is form add location if blank
    this.addLocationForm = this.formbuilder.group({
      'addresses': this.formbuilder.array([
        this.initPopupLocation(),
      ]),
    })
//---------------ko---------------------
 this.koForm=this.formbuilder.group({
    'defaultQlists': this.formbuilder.array([]),
    'customQlist': this.formbuilder.array([])
  });
    this.getKoQuestion();

//-----------------Skill------------------------
  this.skillExpForm=this.formbuilder.group({
      'questionList':this.formbuilder.array([ ])
    })
 //-------------------------------------------

//console.log('Is exist copyHwaId here or not',localStorage.getItem('storeHwaNid'));
if(localStorage.getItem('storeHwaNid')) {
  let hwaId = localStorage.getItem('storeHwaNid');
  this.newHwaId = localStorage.getItem('storeHwaNid');
  console.log('this.newHwaId', this.newHwaId);
  this.loadHwaDraftData(hwaId);
   this.loadkoDraftData(hwaId);
   this.loadSkillDraftData(hwaId);
   this.loadBusinessProfile(hwaId);
} else {
 this.subscription = this.activatedRoute.params.subscribe(
      (param: any) => {
        // console.log('param[hwaId] ',param['hwaId'])
    		if (param['hwaId'] ) {
        this.oldHwaId = param['hwaId'];
        this.loadHwaDraftData(param['hwaId']);
        this.loadkoDraftData(param['hwaId']);
        this.loadSkillDraftData(param['hwaId']);
        this.loadBusinessProfile(param['hwaId']);
    		}
      });
 }

    this.getCheckedLenght();
    //  this.loadHwaDraftData();
  }


changeTab(tab){
  var hwaId = localStorage.getItem('storeHwaNid');
  if(tab == "jd"){
    // this.loadHwaDraftData(hwaId);
  }else if(tab == "ko"){
    // this.loadkoDraftData();
  }else if(tab == "skill"){
   // this.loadSkillDraftData();
  }

}



  loadHwaDraftData(hwaId) {
    let user = this.userService.isLogedin();

    this.HwaServices.loadDraftData(hwaId, user.uid).subscribe(
      res => {
console.log("hwa ", res)
        if (res) {
          this.position = res['title'][0].value;
          this.selectedNofPostion = res['field_how_many_people_do_you_nee'][0].value;
          this.posType = res['field_will_they_be_full_time_par'][0].value;
          this.describePos = res['field_how_would_you_describe_thi'][0].value;
          this.describeSkill = res['field_describe_the_skills_and_ex'][0].value;
           let location = res['field_which_location_s_are_you_h'];
           var locationNid = [];

            for(let i = 0; i < location.length; i++) {

            locationNid.push(location[i].nid[0].value);
           }

         this.checkAddress(locationNid);

         // localStorage.setItem('storeHwaNid', res[0].nid)
        }

      },
      error => {
        console.log(error)
      });
  }
 initPopupLocation() {
    return this.formbuilder.group({
      'title': ['', Validators.required], // , Validators.required
      'field_address_line_2': [''],
      'field_z': ['', [Validators.pattern(/(^\d{5}$)|(^\d{5}-\d{4}$)/), Validators.required]],
      'field_state': ['', Validators.required],
      'field_city': ['', Validators.required]
    });
  }
  //Event For popup Addresses
  zipValidator(zip) {
    var valid = /^\d{5}$/.test(zip.value);
    if (valid) {
      return null;
    }
    return { "invalidZip": true };
  }
  addPopLocation() {

    // add address to the list
    const control = <FormArray>this.addLocationForm.controls['addresses'];
    control.push(this.initPopupLocation());
  }
  removePopLocation(i: number) {
    const control = <FormArray>this.addLocationForm.controls['addresses'];
    control.removeAt(i);
  }
  //Save POpup address
  savePopupAddress() {
    $(".emptyLocation").hide()
    this.alertmsg = true;
    let user = this.userService.isLogedin();
    let locationObj = {
      "uid": user.uid,
      "address": this.addLocationForm.value.addresses
    }
    this.HwaServices.popUpAddLocation(locationObj).subscribe(
      res => {
        //this.removeAllLocation();
        this.displayLocation = true;
        this.checkAddress(null);
        this.addLocationForm.reset();

        this.getCityinfoList = [];
        this.alertService.success("Location Added Successfully", true);
        this.autoHideAlertMsg(7000);
        //
        if (this.launchPopup) { this.launchPopup = false }
        else { this.launchPopup = true }
      },
      error => {
        console.log(error)
      });
  }
  //
  getPositions(event) {
    var locationCount: number;
    this.checkboxval = event.target.checked;
    locationCount = this.getCheckedLenght().length;
    this.addressLenght1 = locationCount;

    if (this.HwaForm.controls['numberOfPosition'].value < this.addressLenght1) {
      this.checkValidity = true;
    }
    else {
      this.checkValidity = false;
    }
  }
  getIndex(event) {
    var locationCount: number;
    this.checkboxval = event.target.checked;
    locationCount = this.getCheckedLenght().length;
    this.addressLenght1 = locationCount;

    if (this.HwaForm.controls['numberOfPosition'].value < this.addressLenght1) {
      this.checkValidity = true
    }
    else {
      this.checkValidity = false
    }

    if (this.checkLocation()) {
      this.addresvalid = false;
    } else {
      this.addresvalid = true;
      //alert('You Have to select minimum one location');
    }

  }
  checkLocation(): boolean {
    var isSelectedLocation = false;
    //  if(this.HwaForm) {
    const control = <FormArray>this.HwaForm.controls['locations'];
    for (let i = 0; i < control.value.length; i++) {
      if (control.value[i].location) {
        isSelectedLocation = true;
        break;
      }
    }
    // }
    return isSelectedLocation;
  }

  iScheckedLocation(control: FormGroup): { [s: string]: boolean } {
    var isSelectedLocation = false;
    /*  if(this.validForOneAddrs) {
         return null;
      } */
    if (!control) {
      return { emailNotMatch: true };
    }

    //  const control = control;
    for (let i = 0; i < control.controls['locations'].value.length; i++) {
      if (control.controls['locations'].value[i].location) {
        isSelectedLocation = true;
        break;
      }
    }
    if (isSelectedLocation) {
      return null;
    }
    else {
      return { 'locationNotMatch': true }
    }
  }
  initAddress(value, lbl, nid) {
    return this.formbuilder.group({
      'location': new FormControl(value),
      'label': new FormControl(lbl),
      'nid': new FormControl(nid),
    }); //, {validator: this.isSingleCheck}
  }

  removeIt({ quill, html, text }) {
    //  console.log(quill, html, text);
  }

  isSingleCheck(control: FormGroup): { [s: string]: boolean } {
    if (!control) {
      return { emailNotMatch: true };
    }

    if (control.controls['location'].value) {
      return null;
    }
    else {
      return { 'emailNotMatch': true }
    }
  }

   onPreiview() {
    var selectAddress = [];

    for (var i = 0; i < this.HwaForm.value.locations.length; i++) {
      if (this.HwaForm.value.locations[i].location) {
        selectAddress.push(this.HwaForm.value.locations[i].nid)
      }
    }

    let HwaForm = { "addressLenght": selectAddress.length, "formData": this.HwaForm.value, "noOfPosition": this.HwaForm.value.numberOfPosition, "companyname": this.myCompeyName, "shoBasicHwa": this.shoBasicHwa = true, "bottomText": this.HelpText, "address1": this.city, "address3": this.addressTitle, "address2": this.state }
    //this.hwaOverlayService.hwaData(HwaForm);
return HwaForm;
  }

 //Add Addresses if no address
  addLocation() {
    if (this.launchPopup) {
      this.launchPopup = false
    }
    else {
      this.launchPopup = true
    }
  }
  cancelLocation() {
    if (this.launchPopup) {
      this.launchPopup = false
    }
    else {
      this.launchPopup = true
    }
    return false;
  }

  formvaladation(): boolean {
    var flag = true;
    if (!this.HwaForm.controls['position'].valid) {
      flag = false;
      this.positionError = true;
    } else {
      this.positionError = false;
    }
    return flag
  }
  clientValidation() {

    if (this.HwaForm.controls['position'].value === '') {
      $(".positionerror").show();
      $(".mdl-textfield__input").focus()
      $("body").animate({ scrollTop: 0 }, 500);
    }
    else if (this.emptyLocation === true) {
      $(".emptyLocation").show().focus();
      $('html, body').animate({ scrollTop: $(".realtimeLocation").offset().top }, 500);
    }

    else if (this.HwaForm.controls['describePosition'].invalid) {
      $(".describePositionFocus .ql-editor").focus()
    }
    else if (this.HwaForm.controls['describeSkill'].invalid) {
      $(".describeSkillFocus .ql-editor").focus()
    }



  }

   onSubmit(from) {

    this.clientValidation()
    this.submitAttempt = true;

    if (this.HwaForm.valid) { //this.formvaladation()
      this.alertmsg = true;
      let user = this.userService.isLogedin();
      //console.log(user.uid)

      var selectedNid = [];
      for (var i = 0; i < this.HwaForm.value.locations.length; i++) {
        if (this.HwaForm.value.locations[i].location) {
          selectedNid.push(this.HwaForm.value.locations[i].nid)
        }
      }

      var hid = localStorage.getItem('storeHwaNid');
      if(hid){

      }else{
        hid = "";
      }
      //this.alertService.success("Saving....", true);
      var CreteObj = {
        "hwa_nid": hid,
        "uid": user.uid,
        "title": this.HwaForm.value.position,
        "field_how_many_people_do_you_nee": this.HwaForm.value.numberOfPosition,
        "field_will_they_be_full_time_par": this.HwaForm.value.position_type,
        "field_how_would_you_describe_thi": this.HwaForm.value.describePosition,
        "field_describe_the_skills_and_ex": this.HwaForm.value.describeSkill,
        "nid": selectedNid
      }

     localStorage.setItem('storeHwaFormData', JSON.stringify(CreteObj));
      //let getHwaData()
      // console.log(user.uid)
      this.HwaServices.createHWA(CreteObj).subscribe(
        res => {
         // localStorage.setItem('storeHwaNid', res.details[0].nid[0]['value']);
          localStorage.setItem('storeHwaNid', res.details[0].nid[0]['value']);
         console.log('storeHwaNid------- ', res.details[0].nid[0]['value']);

           this.alertService.success("Saving...", true);
         // this.autoHideAlertMsg(2000);
          this.onSubmitko(from);
           //this.router.navigate(['makeAcopy', localStorage.getItem('storeHwaNid')])
         //  let event = new MouseEvent('click', {bubbles: true});
         // this.renderer.invokeElementMethod(this.ko.nativeElement, 'dispatchEvent', [event] );
        },
        error => {
          console.log(error)
          this.alertService.success(error, true);
        });
    } else {
        alert("Please choose an address")
    }

  }
   getCheckedLenght(): any {
    var isSelectedLocation = false;
    var locArr = [];
    const control = <FormArray>this.HwaForm.controls['locations'];
    for (let i = 0; i < control.value.length; i++) {
      if (control.value[i].location) {
        locArr.push(control.value[i].location);
      }
    }
    // }
    return locArr;
  }

autoHideAlertMsg(time) {
  setTimeout(() => {
    this.alertmsg = false;

  }, time)
}

  toggleHighlight(newValue: number) {
    this.changeTexthwa = "keyboard_arrow_down"
    if (this.showStyle === newValue) {
      this.showStyle = 0;
      this.changeTexthwa = "keyboard_arrow_down"
    }
    else {
      this.showStyle = newValue;
      this.changeTexthwa = "keyboard_arrow_up"
    }
  }

  googleMapApi() {

    //let uid=uid

  }

  //OnChange


  zipCodeSearch(count) {
    let zipc = this.addLocationForm.value.addresses[count].field_z;

    if (!isNaN(zipc)) {

      this.userService.searchZip(zipc).subscribe(
        res => {
          if (this.validUsCountry(res)) {
            if (res['results'][0].postcode_localities) {
              this.showSelect[count] = true;
              var tempArry = [];
              this.getCityinfoList = [];
              for (let j = 0; j < res['results'][0].postcode_localities.length; j++) {
                this.getCityinfoList.push(res['results'][0].postcode_localities[j]);
              }
            } else {
              this.showSelect[count] = false;
            }

            for (let i = 0; i <= res['results'][0].address_components.length - 1; i++) {

              if (res['results'][0].address_components[i].types[0] == "administrative_area_level_1") {
                var getstate = res['results'][0].address_components[i].short_name;
                var getCity = res['results'][0].address_components[1].long_name
                let arryobj = [];
                arryobj[count] = { field_state: getstate, field_city: getCity }
                this.addLocationForm.controls['addresses'].patchValue(arryobj);

                this.validZipStatus = ""
              }
            }
          } else {
            this.validZipStatus = "Please enter valid US zip code";
            this.showSelect[count] = false;
            let arryobj = [];
            arryobj[count] = { field_state: '', field_city: '' }
            this.addLocationForm.controls['addresses'].patchValue(arryobj);
            //this.hideInput=true;
          }
        });
    } else {
      this.validZipStatus = "Please enter valid US zip code";
      this.showSelect[count] = false;
      let arryobj = [];
      arryobj[count] = { field_state: '', field_city: '' }
      this.addLocationForm.controls['addresses'].patchValue(arryobj);
    }
  }

  removeAllLocation() {
    var control = <FormArray>this.HwaForm.controls['locations'];
    var lengths = control.length;
    for (var i = control.length; i >= 0; i--) {
      control.removeAt(i);
    }

  }

  checkAddress(nidArray) {
    // console.log(nidArray)
    let user = this.userService.isLogedin();
    this.userService.getaddress(user.uid).subscribe(
      res => {

        this.removeAllLocation();
        if (Object.keys(res).length === 0) {
          this.emptyLocation = true;
          this.noLocation = true;
          this.displayLocation = true;
          this.validForOneAddrs = false
        }
        if (Object.keys(res).length > 1) {
          this.emptyLocation = false;
          this.multiLocation = true;
          this.validForOneAddrs = false
          this.title = res;

          var control = <FormArray>this.HwaForm.controls['locations'];
          for (var i = 0; i <= this.title.length - 1; i++) {

            let addresslbl = this.title[i].title + ", " + this.title[i].field_city + ", " + this.title[i].field_state;

            if (nidArray) {

              if (this.findSelectedAddr(this.title[i].nid, nidArray)) {
                control.push(this.initAddress(true, addresslbl, this.title[i].nid));

              } else {
                control.push(this.initAddress(false, addresslbl, this.title[i].nid));
              }

            } else {

              if (i === 0) {
                control.push(this.initAddress(true, addresslbl, this.title[i].nid));
                //  control[i]= this.initAddress(true, addresslbl, this.title[i].nid);
              } else {
                control.push(this.initAddress(false, addresslbl, this.title[i].nid));
                //  control[i] = this.initAddress(false, addresslbl, this.title[i].nid);
              }
            }

            this.noLocation = true;
            this.displayLocation = true;
          }
          this.city = "";
          this.state = "";
          this.addressTitle="";
        }
        if (Object.keys(res).length === 1) {
          this.emptyLocation = false;
          this.city = res[0].field_city;
          this.state = res[0].field_state;
          this.addressTitle = res[0].title;

          this.validForOneAddrs = true;
          this.noLocation = true;
          this.displayLocation = true;
          this.title = res;
          var control = <FormArray>this.HwaForm.controls['locations'];

          for (var i = 0; i <= this.title.length - 1; i++) {
            let addresslbl = this.title[i].title + ", " + this.title[i].field_city + ", " + this.title[i].field_state;

            if (i === 0) {
              control.push(this.initAddress(true, addresslbl, this.title[i].nid));

            } else {
              control.push(this.initAddress(false, addresslbl, this.title[i].nid));

            }
          }
        }
        else {
          this.city = "";
          this.state = "";
        }
      });
  }

  findSelectedAddr(ids, idArray) {
    var flag = false;
    for (var j = 0; j <= idArray.length - 1; j++) {
      if (idArray[j] == ids) {
        flag = true;
      }
    }
    return flag;
  }
  validUsCountry(res): boolean {
    var isUs = false;
    if (res['status'] === "OK") {
      for (let i = 0; i <= res['results'][0].address_components.length - 1; i++) {
        if (res['results'][0].address_components[i].short_name === "US") {
          isUs = true;
          break
        }
      }
    }
    return isUs;
  }

// --------------------------------KO-------------------------------------------------------
//ko
//------------------------------------------------------------------------------------------

  loadkoDraftData(hwaId) {
        let user = this.userService.isLogedin();
       // let hwaId = localStorage.getItem('storeHwaNid');
if(hwaId){

       }else{
          this.router.navigate(['/listhwa-copy']);
          return false;
       }
       let custm = <FormArray>this.koForm.controls['customQlist'];
       this.removedefaultQuestion(custm);
       this.HwaServices.loadkosDraftData(hwaId, user.uid).subscribe(
        res => {
// console.log('ko',res);
          if(res) {
            let qarray:Array<any> = res as Array<any>;
            for(var i =0; i < qarray.length; i++){

              if(qarray[i].field_question_type == "Default") {
                this.checkdefauldQuestion(qarray[i].title, qarray[i].nid, qarray[i].field_ko);
              } else{
                let todo = {"value":qarray[i].title};
               todo.value = qarray[i].title;
                this.addQuestion(todo, qarray[i].nid, qarray[i].field_ko);
                this.btndisabled = false;
              }
            }
          //  forEach(qarray, function (value, prop, obj) {
            // console.log(value);


          //  });
          }
            this.displayQuestionn=true;
        },
        error =>{
          console.log(error)
        });

    }
checkdefauldQuestion(textval, nid, isSelected) {
  for(let i = 0; i < this.koForm.controls['defaultQlists'].value.length; i++) {

  if(this.koForm.controls['defaultQlists'].value[i].inputval == textval){
    let selectedval = false;
  if(isSelected == 1){selectedval = true} else if(isSelected == 0){selectedval = false}else{selectedval = isSelected}
    var control = this.koForm.controls['defaultQlists']['controls'][i];
    control.patchValue({'defaultQlist': selectedval, 'nid': nid})
  }
  }
}
  removedefaultQuestion(cntrl) {
    var control = cntrl; //;
    var lengths = control.length;
    for (var i = control.length; i >= 0; i--) {
      control.removeAt(i);
    }
  }
  getKoQuestion() {
    let d = <FormArray>this.koForm.controls['defaultQlists'];
    this.removedefaultQuestion(d);
    this.HwaServices.defaultQuestion().subscribe(
      res =>{

        this.defaultQLists=res;
        for(let j=0; j < this.defaultQLists.length; j++) {
        const control = <FormArray>this.koForm.controls['defaultQlists'];
        //console.log(this.defaultQLists[j].name);
        control.push(this.initdefaultQuestion(this.defaultQLists[j].name, this.defaultQLists[j].nid));
        }
       // this.loadkoDraftData();
      },
      error=>{

      }
    )
  }
  initdefaultQuestion(val, qnid) {
    return this.formbuilder.group({
      'defaultQlist': [true], // , Validators.required
      'inputval':[val],
      'nid': [qnid]
    });
  }
  resetAllEditbutton(i){
    for(let j=0; j < this.isEditable.length; j++) {
      if(j !== i){
        this.changeText[j] = "Edit";
        this.isEditable[j] = false;
      }
      }
  }
  checkEmpty(e) {
    if(e.value === '') {
      this.btndisabled = true;
    } else {
      this.btndisabled = false;
    }

  }
  toggleShow(i)  {
    this.resetAllEditbutton(i);
      // this.changeText[i] = "Edit";
    if(this.isEditable[i] === true){
      this.changeText[i] = "Edit";
    }
    else{
      this.changeText[i] = "Save";
    }
    this.isEditable[i] = !this.isEditable[i];
  };

  editText(evt, i) {
    if(this.isEditable[i] === true){
      this.changeText[i] = "Edit";
    }
    else{
      this.changeText[i]="Save";
    }
    this.isEditable[i] = !this.isEditable[i];
    this.customQList[i] = evt.target.value;

  }
  finshCode(){
    alert("test")
  }
  //Add Custom question
addCustomQuestion() {
  if(this.showTextfield) {
    this.showTextfield = false;
    this.btndisabled = false;
  }
  else{
    this.showTextfield = true;
    this.btndisabled = true;
    //this.enb = false;
  }
//console.log(this.qusInpitValue)
  //  this.btndisabled = true;
}

addQuestion(todo: any, qnid: any, selected: any) {
if(todo.value !== '') {
    this.customQList.push(todo.value);
    this.isEditable.push(false);
    this.changeText.push('Edit');
    const control = <FormArray>this.koForm.controls['customQlist'];
    if(selected == 1){selected = true} else if(selected == 0){selected = false}
    control.push(this.initQuestion(todo.value, qnid, selected));
    todo.value = null;
  //  if(qnid){
    this.btndisabled = true;
  //  }

  //  this.showTextfield = false;
    this.resetAllEditbutton(9999);
  }
    return false;
  }
  initQuestion(val:any, qnid: any, selected: boolean) {
    return this.formbuilder.group({
      'inputval': [val], // , Validators.required
      'costomQus': [selected],
      'nid':[qnid]
    });
  }
  onPreiviewKo(){

    let koPreive = {"showKo":this.showKo=true, "bottomText":this.HelpText, "defaultQ": this.getSelectedDfaultQus(), "customQ":this.getSelectedQuestion()}
    // this.hwaOverlayService.koData(koPreive);
    return koPreive;
  }

getSelectedQuestion():Array<any> {
  var customArray = [];
  var control = this.koForm.controls['customQlist'].value;
  for(let j=0; j < control.length; j++) {
    if(control[j].costomQus){
    customArray.push(control[j].inputval);
  }
  }
  return customArray;
}

getSelectedDfaultQus():Array<any> {
  var customArray = [];
  var control = this.koForm.controls['defaultQlists'].value;
  for(let j=0; j < control.length; j++) {
    //console.log(control[j])
    if(control[j].defaultQlist){
    customArray.push(control[j].inputval);
  }
  }
  return customArray;
}


  onSubmitko(from) {

    if(localStorage.getItem('storeHwaNid')) {
      this.alertmsg = true;
      let user = this.userService.isLogedin();
      // console.log(this.HwaServices.hwaNid)

      let dummyObj=[];
      let dummyCustom=[];

      var control = this.koForm.controls['customQlist'].value;
      for(let j=0; j < control.length; j++) {
        var nid = "";
        if(this.newHwaId) {
          nid = control[j].nid;
        }else{
          nid = "";
        }

        let custObj = {
          "nid": nid,
          "question":control[j].inputval,
          "field_question_type":"custom",
          "field_ko": control[j].costomQus? 1: 0,
          "status":"1"
        }
        dummyCustom.push(custObj)
      //}
      }
      var control = this.koForm.controls['defaultQlists'].value
      for(let i=0; i < control.length; i++) {
        //if(control[i].defaultQlist) {
          var nid = "";
          if(this.newHwaId) {
              nid = control[i].nid;
          }else{
              nid = "";
          }
        let newObj = {
          "nid": nid,
          "question":control[i].inputval,
          "field_question_type":"default",
          "field_ko": control[i].defaultQlist? 1: 0,
          "status":"1"
        }
        dummyObj.push(newObj);
      //}
      }

      let newObjValue=dummyObj.concat(dummyCustom)
      let objTemplate = {
        "uid":user.uid,
        "hwa_nid":localStorage.getItem('storeHwaNid'),
        "ko":newObjValue
      }
console.log('save Ko', localStorage.getItem('storeHwaNid'))
      this.HwaServices.customQuestion(objTemplate).subscribe(
        res => {


          //this.alertService.success("Knockout Questions Created Successfully", true);
          //this.autoHideAlertMsg(2000);
          this.onSubmitskill(from);
          //let event = new MouseEvent('click', {bubbles: true});
         // this.renderer.invokeElementMethod(this.skill.nativeElement, 'dispatchEvent', [event] );

        },
        error =>{
          console.log(error)
        }
      )
    }

    else{
      alert('You must have to create and save first step of "Help Wanted Ad"')
      //this.router.navigate(['/createHWA']);

    }
   }


/* -------------------------------------------------------------------------------------------------------------

              Skills Tab

----------------------------------------------------------------------------------------------------------------*/
loadSkillDraftData(hwaId) {
      let user = this.userService.isLogedin();
      //let hwaId = localStorage.getItem('storeHwaNid');
      if(hwaId){

       }else{
          this.router.navigate(['/listhwa-copy']);
          return false;
       }
    this.HwaServices.loadSkillsDraftData(hwaId, user.uid).subscribe(
      res => {
 // console.log('skills ',res)
        if(res) {
          let qarray:Array<any> = res as Array<any>;
          if(qarray.length > 0) {
          for(var i =0; i < qarray.length; i++){

          this.addUser(qarray[i].nid, qarray[i].title, qarray[i].field_expertise_needed, qarray[i].field_experience_needed);
          }
        }else{
          for(var i =0; i < 1; i++){

          this.addUser('', '', 'N/A', 'N/A');
          }
        }
        }
        this.displayQuestionn = true;
      },
      error =>{
        console.log(error)
      });
  }

 onPreiviewSkill() {

    let skillData = {"showskill":this.showskill=true,"bottomText":this.HelpText,"skillQusetion":this.skillExpForm.value.questionList}
    //this.hwaOverlayService.skillData(skillData);
    return skillData;
  }
  initQuestionSkill(nid:any, tital:string, expertis: any, experen: any, expertisDisabled: boolean, experenDisabled: boolean ){
    return this.formbuilder.group({
      'nid':[nid],
      'typeQuestion':[tital],
      'expertiseLevel':[{value: expertis, disabled: false}],
      'experenceLevel':[{value: experen, disabled: false}]
    })
  }

  hideExperenceLevel(i) {

    const control = <FormArray>this.skillExpForm.controls['questionList']
      if(control.controls[i]['controls'].expertiseLevel.value === "N/A"){
        this.hideExpArray[i]=false;
        this.hideExpertiseArray[i] =false;
      } else {
        this.hideExpArray[i]=false;
        this.hideExpertiseArray[i] =true;
      }
  }
  hideExpertiseLevel(i) {

    const control = <FormArray>this.skillExpForm.controls['questionList']
      if(control.controls[i]['controls'].experenceLevel.value === "N/A"){
        this.hideExpArray[i]=false;
        this.hideExpertiseArray[i] =false;
      } else {
        this.hideExpArray[i]=true;
        this.hideExpertiseArray[i] =false;
      }

  }

  addUser(nid:any, tital:string, expertis: any, experen: any ){
    const control = <FormArray>this.skillExpForm.controls['questionList'];
    var experenDisabled = false;
    var expertisDisabled = false;
    if((experen === 'N/A') && (expertis === 'N/A')){
      experenDisabled = false;
      expertisDisabled  = false;
    }else{
        if(experen === 'N/A'){
          this.hideExpArray.push(false);
          experenDisabled = false
        }else{
          this.hideExpArray.push(true);
            experenDisabled = true;
        }
        if(expertis === 'N/A'){
        this.hideExpertiseArray.push(false);
         expertisDisabled= false;
        }else{
          this.hideExpertiseArray.push(true);
           expertisDisabled= true;
        }
  }
      control.push(this.initQuestionSkill(nid, tital, expertis, experen, expertisDisabled, experenDisabled));
  }



  onSubmitskill(from) {

    if(localStorage.getItem('storeHwaNid')){
      this.alertmsg = true;
      let user = this.userService.isLogedin();
      var deleteskills = [];
      let pushSkillSet=[]
      for(let i=0; i <= this.skillExpForm.value.questionList.length-1; i++) {

  if((this.skillExpForm.value.questionList[i].experenceLevel === 'N/A') && (this.skillExpForm.value.questionList[i].expertiseLevel === 'N/A')){
      if(this.skillExpForm.value.questionList[i].nid){
      deleteskills.push(this.skillExpForm.value.questionList[i].nid);
      }
    } else{
         var nid = "";
          if(this.newHwaId) {
              nid = this.skillExpForm.value.questionList[i].nid;
          }else{
              nid = "";
          }
        let skillQ={
          "nid": nid,
          "skill_exp_ques":this.skillExpForm.value.questionList[i].typeQuestion,
          "field_experience_needed":this.skillExpForm.value.questionList[i].experenceLevel,
          "field_expertise_needed":this.skillExpForm.value.questionList[i].expertiseLevel,
          "status":"0"
        }
        pushSkillSet.push(skillQ)
    }
      }

      let skillObj = {
        "uid":user.uid,
        "hwa_nid":localStorage.getItem('storeHwaNid'),
        "seq":pushSkillSet,
        "delete_nid": deleteskills
      }
console.log('skillObj', skillObj);
      this.HwaServices.skillQusestion(skillObj).subscribe(
        res => {
         // localStorage.setItem('storeHwaNid', localStorage.getItem('storeHwaNid'));
          //localStorage.removeItem('storeHwaNid');
          //this.alertService.success("You have created Skills Or Experience Questions Successfully", true);
          //this.autoHideAlertMsg(2000);
          this.onsubmitBuz(from)

          //this.oldHwaId
         // this.linkToProfile(this.oldHwaId, localStorage.getItem('storeHwaNid'))
        },
        error =>{
          console.log(error)
        }
      )
    }
    else {
      //this.alertService.error("You need to create help wanted Ad");
      alert('You must have to create and save first step of "Help Wanted Ad"')

    //  this.router.navigate(['/businessprofile/step-one']);
    }
  }

/* -------------------------------------------------------------------------------------
Business profile
-----------------------------------------------------------------------------------*/

loadBusinessProfile(hwaid) {

    let hwaObj = {
      "hwa_nid": hwaid
    }

    this.HwaServices.getAllDataOfHwa(hwaObj).subscribe(
      res => {
        console.log('buz', res);
       //if (res['business_profile']){
        if(res['business_profile'].nid) {

          this.showProfile = true;
          this.proFileObj = res['business_profile'];

     console.log('profileNid', this.proFileObj.nid[0].value)
 // localStorage.setItem('profileNid', this.proFileObj.nid[0].value);
 this.currProfileNid = this.proFileObj.nid[0].value;
          if(this.proFileObj.field_image1){
             this.image1Url = this.proFileObj.field_image1[0].url;
             localStorage.setItem('prof1_img_nid', this.proFileObj.field_image1[0].target_id);
             //this.showDeleteBtn = true;
          }else {
             this.image1Url = "";
          }
          if(this.proFileObj.field_image2){
             this.image2Url = this.proFileObj.field_image2[0].url;
             localStorage.setItem('prof2_img_nid', this.proFileObj.field_image2[0].target_id);
          }else {
             this.image2Url = "";
          }
          if(this.proFileObj.field_image3){
             this.image3Url = this.proFileObj.field_image3[0].url;
             localStorage.setItem('prof3_img_nid', this.proFileObj.field_image3[0].target_id);
          }else {
             this.image3Url = "";
          }
          if(this.proFileObj.field_image4){
             this.image4Url = this.proFileObj.field_image4[0].url;
             localStorage.setItem('prof4_img_nid', this.proFileObj.field_image4[0].target_id);
          }else {
             this.image4Url = "";
          }
//----------------------------------------------------------------
          if(this.proFileObj.field_title1){
             this.titalText1 = this.proFileObj.field_title1[0].value;
          }else {
             this.titalText1 = ""; //Write about your business.
             this.titalText1_lbl = "Save";
             this.titalText1_isEdit = true;
          }
           if(this.proFileObj.field_title2){
             this.titalText2 = this.proFileObj.field_title2[0].value;
          }else {

            this.titalText2 = ""; //What our customers like about us.
             this.titalText2_lbl = "Save";
             this.titalText2_isEdit = true;
          }
           if(this.proFileObj.field_title3){
             this.titalText3 = this.proFileObj.field_title3[0].value;
          }else {

              this.titalText3 = ""; // Sell them on the job.
             this.titalText3_lbl = "Save";
             this.titalText3_isEdit = true;
          }
           if(this.proFileObj.field_title4){
             this.titalText4 = this.proFileObj.field_title4[0].value;
          }else {

              this.titalText4 = ""; // Describe the Job's Benefits.
             this.titalText4_lbl = "Save";
             this.titalText4_isEdit = true;
          }
//---------------------------------------------------------------------------
          if(this.proFileObj.body) {
             this.bodyText1 = this.proFileObj.body[0].value;
          }else {
             this.bodyText1 = "";
          }
          if(this.proFileObj.field_body2) {
             this.bodyText2 = this.proFileObj.field_body2[0].value;
          }else {
             this.bodyText2 = "";
          }
          if(this.proFileObj.field_body3) {
             this.bodyText3 = this.proFileObj.field_body3[0].value;
          }else {
             this.bodyText3 = "";
          }
          if(this.proFileObj.field_body4) {
             this.bodyText4 = this.proFileObj.field_body4[0].value;
          }else {
             this.bodyText4 = "";
          }

        }else {
          this.image1Url = "";
          this.titalText1 = "";
          this.titalText1_lbl = "Save";
          this.titalText1_isEdit = true;

          this.image2Url = "";
          this.titalText2 = "";
          this.titalText2_lbl = "Save";
          this.titalText2_isEdit = true;

          this.image3Url = "";
          this.titalText3 = "";
          this.titalText3_lbl = "Save";
          this.titalText3_isEdit = true;

          this.image4Url = "";
          this.titalText4 = "";
          this.titalText4_lbl = "Save";
          this.titalText4_isEdit = true;
        }

      });
}

  onsubmitBuz(from) {
    var allStepData = this.getObjectForSave();
    if(allStepData){

    }
  // console.log(allStepData);
    this.HwaServices.createProfile(allStepData).subscribe(
      res => {

        // localStorage.setItem('profileNid', res['details'][0].nid[0]['value']);
        //localStorage.removeItem('step1Profile');
        this.alertService.success("HWA Created Successfully", true);
        this.autoHideAlertMsg(1000);
         if(from == "postMyAd") {
          this.router.navigate(['/postmyad']);
        } else {
          this.router.navigate(['/listhwa-copy']);
        }

      },
      error => {
        console.log(error)
        this.alertService.success(error, true);
      });

  }

getObjectForSave():any {
  let prof1_img_nid =  localStorage.getItem('prof1_img_nid');
  let prof2_img_nid =  localStorage.getItem('prof2_img_nid');
  let prof3_img_nid =  localStorage.getItem('prof3_img_nid');
  let prof4_img_nid =  localStorage.getItem('prof4_img_nid');

  var stepData = {
          "bptitle1":this.titalText1,
          "bpbody1": this.bodyText1,
          "image1_fid":prof1_img_nid,
          "bptitle2":this.titalText2,
          "bpbody2": this.bodyText2,
          "image2_fid":prof2_img_nid,
          "bptitle3":this.titalText3,
          "bpbody3": this.bodyText3,
          "image3_fid":prof3_img_nid,
          "bptitle4":this.titalText4,
          "bpbody4": this.bodyText4,
          "image4_fid":prof4_img_nid,
          }
  return stepData;
}


deleteThis(field){
 //alert(this.currProfileNid)
    if(this.currProfileNid){
    var deletImgObj = null;
      this.showDeleteBtn = true;
      if(field == '1'){
        deletImgObj = {
        "bp_nid":this.currProfileNid, "image1_fid":"yes", "image2_fid":"", "image3_fid":"", "image4_fid":""
        }
      }else if(field == '2'){
        deletImgObj = {
        "bp_nid":this.currProfileNid, "image1_fid":"", "image2_fid":"yes", "image3_fid":"", "image4_fid":""
        }
      }else if(field == '3'){
        deletImgObj = {
        "bp_nid":this.currProfileNid, "image1_fid":"", "image2_fid":"", "image3_fid":"yes", "image4_fid":""
        }
      }else if(field == '4'){
        deletImgObj = {
        "bp_nid":this.currProfileNid, "image1_fid":"", "image2_fid":"", "image3_fid":"", "image4_fid":"yes"
        }
      }



this.HwaServices.deleteImg(deletImgObj).subscribe(
    res => {
    console.log(res);
              this.showDeleteBtn = false;
              this.imgbase64src = '';
              this.isThereImge = false;
              this.imgLoading=false;

              var profImgId = 'prof'+field+'_img_nid';
             // localStorage.setItem(profImgId, '');
              //localStorage.removeItem(profImgId);
              if(field == '1'){
              this.image1Url = "";
              localStorage.setItem('prof1_img_nid', '');
              localStorage.removeItem('prof1_img_nid');
            }else if(field == '2'){
               this.image2Url = "";
              localStorage.setItem('prof2_img_nid', '');
              localStorage.removeItem('prof2_img_nid');
            }else if(field == '3'){
               this.image3Url = "";
              localStorage.setItem('prof3_img_nid', '');
              localStorage.removeItem('prof3_img_nid');
            }else if(field == '4'){
               this.image4Url = "";
              localStorage.setItem('prof4_img_nid', '');
              localStorage.removeItem('prof4_img_nid');
              }


    })
  }else{
      if(field == '1'){
              this.image1Url = "";
              localStorage.setItem('prof1_img_nid', '');
              localStorage.removeItem('prof1_img_nid');
            }else if(field == '2'){
               this.image2Url = "";
              localStorage.setItem('prof2_img_nid', '');
              localStorage.removeItem('prof2_img_nid');
            }else if(field == '3'){
               this.image3Url = "";
              localStorage.setItem('prof3_img_nid', '');
              localStorage.removeItem('prof3_img_nid');
            }else if(field == '4'){
               this.image4Url = "";
              localStorage.setItem('prof4_img_nid', '');
              localStorage.removeItem('prof4_img_nid');
              }
  }

}

// -------------------------------link exist profile to new created HWA-----------------

/* linkToProfile(oldHwaId, newHwaId) {
   let user = this.userService.isLogedin();
      let obj ={
        "old_hwa_nid":oldHwaId,
        "new_hwa_nid":newHwaId
      }

      this.HwaServices.linkToProfile(obj).subscribe(
        res => {
          console.log("finallllllll", res)
          var msg = res;
          this.alertService.success("Copy Hwa Successfully", true);
          this.autoHideAlertMsg(4000);
          localStorage.setItem('storeHwaNid', localStorage.getItem('storeHwaNid'));
          localStorage.removeItem('storeHwaNid');
          this.router.navigate(['/postmyad'])
        },
        error =>{
          console.log(error)
        }
      )
 } */

 onSubmitAll(from) {
  this.onSubmit(from);
 }
setfocus(e) {
  e.target.focus();
console.log(e.target)
}

fileChangeEvent (fileInput: any, image) {
  this.imgLoading=true;
  this.ImageNumber = image;
       this.filesToUpload = <Array<File>> fileInput.target.files;
       //var formData: any = new FormData();
       var file:File = this.filesToUpload[0];
       //formData.append( "myfile", file, file.name );
      var myReader:FileReader = new FileReader();
      myReader.onloadend = (e) => {
        //this.imgbase64src = myReader.result;
       // console.log( file.name);
        var imgData = {
        "image_data":myReader.result.split(',')[1],
        "name":file.name
        }
        this.HwaServices.uploadProfileImage(imgData).subscribe(
           res => {
            // console.log(res);
             if(res['fid']) {
             //  alert(image);
              if(image == '1'){
                localStorage.setItem('prof1_img_nid', res['fid']);
               this.image1Url = res['image'];
              }else if(image == '2'){
                localStorage.setItem('prof2_img_nid', res['fid']);
               this.image2Url = res['image'];
              }else if(image == '3'){
                localStorage.setItem('prof3_img_nid', res['fid']);
               this.image3Url = res['image'];
              }else if(image == '4'){
                localStorage.setItem('prof4_img_nid', res['fid']);
               this.image4Url = res['image'];
              }

                this.imgbase64src = res['image'];

                this.isThereImge = true;
               setTimeout(() => {
                 this.imgLoading = false;
                 this.ImageNumber = 0;
               }, 1500);
             }else{
               //this.isThereImge = false;
             }

           })
      }

      myReader.readAsDataURL(file);
}

onPreiviewAll() {
this.buzProfine = {'image1Url':this.image1Url, 'image2Url':this.image2Url,}
this.buzProfine.image1Url = this.image1Url;
this.buzProfine.image2Url = this.image2Url;
this.buzProfine.image3Url = this.image3Url;
this.buzProfine.image4Url = this.image4Url;

this.buzProfine.titalText1 = this.titalText1;
this.buzProfine.titalText2 = this.titalText2;
this.buzProfine.titalText3 = this.titalText3;
this.buzProfine.titalText4 = this.titalText4;

this.buzProfine.bodyText1 = this.bodyText1;
this.buzProfine.bodyText2 = this.bodyText2;
this.buzProfine.bodyText3 = this.bodyText3;
this.buzProfine.bodyText4 = this.bodyText4;

    this.alldataofHwa = {'Hwa':this.onPreiview(), 'kq':this.onPreiviewKo(), 'skill_expirence': this.onPreiviewSkill(), 'business_profile': this.buzProfine}
    //console.log(this.alldataofHwa);
    this.showFinalHWA = true;
}

private titalText1_lbl:string = 'Edit';
  toggleShow_1(i)  {
    if(this.titalText1_isEdit === true) {
      this.titalText1_lbl = "Edit";
       this.titalText1_isEdit = false;
    } else {
      this.titalText1_lbl = "Save";
       this.titalText1_isEdit = true;
    }
    //this.titalText3_isEdit = !this.titalText3_isEdit;
  };
  private titalText2_lbl:string = 'Edit';
  toggleShow_2(i)  {
    if(this.titalText2_isEdit === true) {
      this.titalText2_lbl = "Edit";
       this.titalText2_isEdit = false;
    } else {
      this.titalText2_lbl = "Save";
       this.titalText2_isEdit = true;
    }
    //this.titalText3_isEdit = !this.titalText3_isEdit;
  };


  private titalText3_lbl:string = 'Edit';
  toggleShow_3(i)  {
    if(this.titalText3_isEdit === true) {
      this.titalText3_lbl = "Edit";
       this.titalText3_isEdit = false;
    } else {
      this.titalText3_lbl = "Save";
       this.titalText3_isEdit = true;
    }
  };

  private titalText4_lbl:string = 'Edit';
  toggleShow_4(i)  {
    if(this.titalText4_isEdit === true) {
      this.titalText4_lbl = "Edit";
       this.titalText4_isEdit = false;
    } else {
      this.titalText4_lbl = "Save";
       this.titalText4_isEdit = true;
    }
    //this.titalText3_isEdit = !this.titalText3_isEdit;
  };

 // const limit = 1000;

/*quill.on('text-change', function (delta, old, source) {
  if (quill.getLength() > limit) {
    quill.deleteText(limit, quill.getLength());
  }
});*/
changetext(e) {

// console.log('length', e.text);
if(e.text.length > 300){
//e.deleteText(300, e.text.length);
return true;
};
}

 textChanged($event) {
  // console.log($event.editor.getLength())
    if ($event.editor.getLength() > this.MAX_LENGTH) {
      $event.editor.deleteText(this.MAX_LENGTH, $event.editor.getLength());
      alert('You can Entered only 1000 characters');
    }
  }
}
