import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import {AlertService} from '../../../../services/alert.service';
import {UserService} from '../../../../services/user.service';
import {HwaCommonService} from '../../../../services/hwa-common.service';



@Component({
  selector: 'app-copy-hwa',
  templateUrl: './list-copy-hwa.component.html',
  styleUrls: ['./list-copy-hwa.component.css']
})
export class ListCopyHwaComponent implements OnInit {
  private copyhwa1: any= 'copyhwa1';
  private hwaDraftArray = [];
  private saveHwaArray = [];
  private expiredHwaArray = [];
  private displayListing = false;
  private showStyle: number;
  private showStyleExp: number
  private showStyleDraft: number
  private changeTexthwa: string = 'keyboard_arrow_down';
  private changeTexthwaExp: string = 'keyboard_arrow_down';
  private changeTexthwaDraft: string = 'keyboard_arrow_down';
  private alertmsg: boolean;
  private showListofSaved: boolean;
  private showListofExp: boolean;
  private showListofDraft: boolean;
  public draftItem: FormGroup;
  private discardOverlay: boolean;
  private currHwaId: any;
  constructor(
    private alertService: AlertService,
    private router: Router,
    private formbuilder: FormBuilder,
    private userService: UserService,
    private HwaServices: HwaCommonService) { }

  ngOnInit() {
    let utcSeconds = 1491071421;
    let d = new Date(0); //  The 0 there is the key, which sets the date to the epoch
    d.setUTCSeconds(utcSeconds);
    localStorage.removeItem('prof1_img_nid');
    localStorage.removeItem('prof2_img_nid')
    localStorage.removeItem('prof3_img_nid')
    localStorage.removeItem('prof4_img_nid');
    localStorage.removeItem('copyHwaNid');
    localStorage.removeItem('storeHwaNid');
    localStorage.removeItem('storeHwaFormData');
    localStorage.removeItem('profileNid');
    localStorage.removeItem('pamentData');
    this.getHWAList();
  }

  toTimestampParse(strDate) {
    let datum = Date.parse(strDate);
    return datum / 1000;
  }
  getHWAList() {
    let user = this.userService.isLogedin();

    this.HwaServices.getAllHWAList(user.uid).subscribe(
      res => {
        console.log(res);
        this.displayListing = true;
        //  this.alertService.success('Your have submited Help Wanted Ad Successfully', true);
        // this.router.navigate(['/']);
        this.hwaDraftArray = res['hwa_draft'];
        this.saveHwaArray = res['hwa_published'];
        this.expiredHwaArray = res['hwa_expired_listing'];
        this.toggleHighlight(1);
      });
  }

  makeAcopyOfHWA(hwaid){
    // alert(hwaid);
    this.router.navigate(['makeAcopy', hwaid]);
    //  this.router.navigate(['jobdescription', hwaid]);

  }

  viewHWA(hwaid, expDate){
    this.router.navigate(['viewad', hwaid, expDate, 'Copy'])
  }
  editHWA(hwaid) {
    this.router.navigate(['makeAcopy', hwaid]);
  }
  copyFromEditHWA(hwaid) {
    this.router.navigate(['makeAcopy', hwaid]);
  }
  toggleHighlight(newValue: number) {
    this.changeTexthwa = 'keyboard_arrow_down';
    if (this.showStyle === newValue) {
      this.showStyle = 0;
      this.showListofSaved = false;
      this.changeTexthwa = 'keyboard_arrow_down';
    } else {
      this.showStyle = newValue;
      this.showListofSaved = true;
      this.changeTexthwa = 'keyboard_arrow_up';
    }
  }

  toggleHighlightDraft(newValue: number) {
    this.changeTexthwaDraft = 'keyboard_arrow_down';
    if (this.showStyleDraft === newValue) {
      this.showStyleDraft = 0;
      this.showListofDraft = false;
      this.changeTexthwaDraft = 'keyboard_arrow_down';
    } else {
      this.showStyleDraft = newValue;
      this.showListofDraft = true;
      this.changeTexthwaDraft = 'keyboard_arrow_up';
    }
  }

  toggleHighlightExp(newValue: number) {
    this.changeTexthwaExp = 'keyboard_arrow_down';
    if (this.showStyleExp === newValue) {
      this.showStyleExp = 0;
      this.showListofExp = false;
      this.changeTexthwaExp = 'keyboard_arrow_down';
    }
    else {
      this.showStyleExp = newValue;
      this.showListofExp = true;
      this.changeTexthwaExp = 'keyboard_arrow_up';
    }
  }
  sorting: any = {
    column: 'field_hwa_post', // to match the letiable of one of the columns
    descending: false
  };


  columns: any[] = [
    {
      display: 'Column 1', // The text to display
      letiable: 'field_hwa_post', // The name of the key that's apart of the data array
      filter: 'text' // The type data type of the column (number, text, date, etc.)
    },
    /* {
       display: 'Column 2', // The text to display
       letiable: 'Amount', // The name of the key that's apart of the data array
       filter: 'decimal : 1.0-2' // The type data type of the column (number, text, date, etc.)
     },*/
    {
      display: 'Column 3', // The text to display
      letiable: 'created', // The name of the key that's apart of the data array
      filter: 'dateTime' // The type data type of the column (number, text, date, etc.)
    }
  ];

  selectedClass(columnName): any {
    return columnName === this.sorting.column ? 'sort-' + this.sorting.descending : false;
  }

  changeSorting(columnName): void {
    let sort = this.sorting;
    if (sort.column === columnName) {
      sort.descending = !sort.descending;
    } else {
      sort.column = columnName;
      sort.descending = false;
    }
  }
  convertSorting(): string {
    return this.sorting.descending ? '-' + this.sorting.column : this.sorting.column;
  }

  // -----------------------------published-----------------------------

  sorting_publi: any = {
    column: 'field_hwa_post', // to match the letiable of one of the columns
    descending: false
  };
  columns_publi: any[] = [
    {
      display: 'Column 1', // The text to display
      letiable: 'field_hwa_post', // The name of the key that's apart of the data array
      filter: 'text' // The type data type of the column (number, text, date, etc.)
    },
    {
      display: 'Column 3', // The text to display
      letiable: 'field_extend_from_date', // The name of the key that's apart of the data array
      filter: 'dateTime' // The type data type of the column (number, text, date, etc.)
    }
  ];
  changeSorting_publi(columnName): void {
    let sort = this.sorting_publi;
    if (sort.column === columnName) {
      sort.descending = !sort.descending;
    } else {
      sort.column = columnName;
      sort.descending = false;
    }
  }
  selectedClass_publi(columnName): any {
    return columnName === this.sorting_publi.column ? 'sort-' + this.sorting_publi.descending : false;
  }
  convertSorting_publi(): string{
    return this.sorting_publi.descending ? '-' + this.sorting_publi.column : this.sorting_publi.column;
  }


  myDate(datestr) {
    let sd = new Date(datestr);
    let humDate = new Date(Date.UTC(sd.getFullYear(),
      (this.stripLeadingZeroes(sd.getMonth())),
      this.stripLeadingZeroes(sd.getDate()),
      this.stripLeadingZeroes(sd.getHours()),
      this.stripLeadingZeroes(sd.getMinutes()),
      this.stripLeadingZeroes(sd.getSeconds())));
    let theDate = (humDate.getTime() / 1000.0);

    // let theDate = new Date( Date.parse(datestr) * 1000);
    // theDate.toGMTString();
    // new Date();
    // let utcSeconds = Date.parse(datestr);
// let d = new Date(0); //  The 0 there is the key, which sets the date to the epoch
//  return d.setUTCSeconds(utcSeconds);
    return new Date(theDate);
  }

  parseDate(input) {
    //  console.log('input date', input)
    //  let parts = input.split('-'); // 'dd-mm-yyyy'
    //  new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
    // let k = new Date(parts[2], parts[0]-1, parts[1]);
    let k; // = new Date(input);
    if (Date.parse(input)) {
      k = new Date(input); // +' GMT'
    } else {
      k = '';
    }
    return  k;
  }

  stripLeadingZeroes(input) {
    if((input.length > 1) && (input.substr(0,1) == '0'))
      return input.substr(1);
    else
      return input;
  }
  // --------------------------discard-------------------------
  confirmDiscard(hwaId) {
    this.currHwaId = hwaId;
    this.discardOverlay = true;
  }
  discardHwa() {
    let discardHwaObj = { 'hwa_nid': [this.currHwaId] };
    this.alertmsg = true;
    this.HwaServices.discardHwa(discardHwaObj).subscribe(
      res => {
        console.log(res);
        this.currHwaId = '';
        this.discardOverlay = false;
        if (res['hwa_discard'] === 'deleted') {
          this.getHWAList();
          this.alertService.success('deleted Successfully', true);
          this.autoHideAlertMsg(3000);
        }
      });
  }

  autoHideAlertMsg(time) {
    setTimeout(() => {
      this.alertmsg = false;

    }, time);
  }
  closeWin() {
    this.currHwaId = '';
    this.discardOverlay = false;

  }
}
