import {Component, OnInit, ElementRef} from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormArray} from '@angular/forms';
import {Router} from '@angular/router';
import {OverlayDataService} from '../../../../../services/overlay-data.service';
import {UserService} from '../../../../../services/user.service';
import {HwaCommonService} from '../../../../../services/hwa-common.service';
import {AlertService} from '../../../../../services/alert.service';


@Component({
  selector: 'app-skill-and-experience',
  templateUrl: './skill-and-experience.component.html',
  styleUrls: ['./skill-and-experience.component.css']
})
export class SkillAndExperienceComponent implements OnInit {
  public skillExpForm: FormGroup;
  private alertmsg: boolean;
  private skillDataStr:string='skillData';
  private showDialog: boolean;
  private showskill: boolean;
  private fromSkill: any = 'fromSkill';
  private hideExpArray = [];
  private hideExpertiseArray = [];
  private displayQuestionn: boolean;
  private showActiveBtn: boolean;
  private HelpText = [
    {
      'label': 'How do companies get the best possible candidates to apply?'
    },
    {
      'label': 'They take the time to sell job seekers on what makes the company a great place to work, what might be exciting about the actual job, what your colleagues might be like or even what unexpected benefits the job may have. Talentrackr™ can help you create these items and show them to job seekers that look at your Help Wanted Ad.'
    }

  ];
  private disableUntil: boolean;
  private btntext = 'Save & Continue';
  constructor(
    private formbuilder: FormBuilder,
    private ele: ElementRef,
    private alertService: AlertService,
    private router: Router,
    private hwaOverlayService: OverlayDataService,
    private userService: UserService,
    private HwaServices: HwaCommonService
  ) { }

  ngOnInit() {
    this.skillExpForm = this.formbuilder.group({
      'questionList': this.formbuilder.array([
      ])
    });

    this. loadSkillDraftData();
  }

  loadSkillDraftData() {
    let user = this.userService.isLogedin();
    let hwaId = localStorage.getItem('storeHwaNid');
    if (hwaId) {

    } else {
      this.router.navigate(['/hwa_workflow']);
      return false;
    }
    this.HwaServices.loadSkillsDraftData(hwaId, user.uid).subscribe(
      res => {
        if (res) {
          let qarray: Array<any> = res as Array<any>;
          if (qarray.length > 0) {
            for (let i = 0; i < qarray.length; i++) {

              this.addUser(qarray[i].nid, qarray[i].title, qarray[i].field_expertise_needed, qarray[i].field_experience_needed);
              //  nid:any, tital:string, expertis: any, experen: any, disabledVal: boolean
            }
          } else {
            for (let i = 0; i < 1; i++) {

              this.addUser('', '', 'N/A', 'N/A');
            }
          }
        }
        this.displayQuestionn = true;
      },
      error => {
      });
  }

  onPreiview() {
    if (this.showDialog) {
      this.showDialog = false;
    } else {
      this.showDialog = true;
    }

    let skillData = {'showskill': this.showskill = true,
      'bottomText': this.HelpText,
      'skillQusetion': this.skillExpForm.value.questionList};
    this.hwaOverlayService.skillData(skillData);
  }
  initQuestion(nid: any, tital: string, expertis: any, experen: any, expertisDisabled: boolean, experenDisabled: boolean ){
    return this.formbuilder.group({
      'nid': [nid],
      'typeQuestion': [tital, Validators.required],
      'expertiseLevel': [expertis , Validators.required],
      'experenceLevel': [experen , Validators.required]
    });
  }

  hideExperenceLevel(i) {
    const control = <FormArray>this.skillExpForm.controls['questionList'];
    if (control.controls[i]['controls'].expertiseLevel.value === 'N/A') {
      this.hideExpArray[i] = false;
      this.hideExpertiseArray[i] = false;
      this.showActiveBtn = false;
    } else {
      this.hideExpArray[i] = false;
      this.hideExpertiseArray[i] = true;
      this.showActiveBtn = true;
    }
  }
  hideExpertiseLevel(i) {

    const control = <FormArray>this.skillExpForm.controls['questionList'];
    if (control.controls[i]['controls'].experenceLevel.value === 'N/A') {
      this.hideExpArray[i] = false;
      this.hideExpertiseArray[i] = false;
    } else {
      this.hideExpArray[i] = true;
      this.hideExpertiseArray[i] = false;
    }

  }
  addUser(nid: any, tital: string, expertis: any, experen: any ) {
    const control = <FormArray>this.skillExpForm.controls['questionList'];
    let experenDisabled = false;
    let expertisDisabled = false;
    if ((experen === 'N/A') && (expertis === 'N/A')) {
      experenDisabled = false;
      expertisDisabled  = false;
    }else {
      if (experen === 'N/A') {
        this.hideExpArray.push(false);
        experenDisabled = false;
      }else {
        this.hideExpArray.push(true);
        experenDisabled = true;
      }
      if (expertis === 'N/A') {
        this.hideExpertiseArray.push(false);
        expertisDisabled = false;
      }else {
        this.hideExpertiseArray.push(true);
        expertisDisabled = true;
      }
    }
    control.push(this.initQuestion(nid, tital, expertis, experen, expertisDisabled, experenDisabled));
  }
  autoHideAlertMsg(time) {
    setTimeout(() => {
      this.alertmsg = false;
    }, time);
  }

  skipThis() {

    this.router.navigate(['/businessprofile/step-one']);
    return false;
  }
  onSubmit(from) {
    this.disableUntil = true;
    this.btntext = 'Processing...';
    if (localStorage.getItem('storeHwaNid')) {
      this.alertmsg = true;
      let user = this.userService.isLogedin();
      let deleteskills = [];
      let pushSkillSet = [];
      for (let i = 0; i <= this.skillExpForm.value.questionList.length - 1; i++) {
        if ((this.skillExpForm.value.questionList[i].experenceLevel === 'N/A') && (this.skillExpForm.value.questionList[i].expertiseLevel === 'N/A')){
          if (this.skillExpForm.value.questionList[i].nid) {
            deleteskills.push(this.skillExpForm.value.questionList[i].nid);
          }
        } else {
          let skillQ = {
            'nid': this.skillExpForm.value.questionList[i].nid,
            'skill_exp_ques': this.skillExpForm.value.questionList[i].typeQuestion,
            'field_experience_needed': this.skillExpForm.value.questionList[i].experenceLevel,
            'field_expertise_needed': this.skillExpForm.value.questionList[i].expertiseLevel,
            'status': '0'
          };
          pushSkillSet.push(skillQ);
        }
      }

      let skillObj = {
        'uid': user.uid,
        'hwa_nid': localStorage.getItem('storeHwaNid'),
        'seq': pushSkillSet,
        'delete_nid': deleteskills
      };
      this.HwaServices.skillQusestion(skillObj).subscribe(
        res => {

          if (from === 'postMyad') {
            this.router.navigate(['/postmyad']);
          } else {
            setTimeout(() => {
              this.router.navigate(['/businessprofile/step-one']);
              this.disableUntil = false;
            }, 1500);
            this.alertService.success('You have created Skills Or Experience Questions Successfully', true);
            this.autoHideAlertMsg(1500);
          }
          this.btntext = 'Save & Continue';

        },
        error => {
        }
      );
    } else {
      alert ('You must have to create and save first step of \'Help Wanted Ad\'');
    }
  }

}
