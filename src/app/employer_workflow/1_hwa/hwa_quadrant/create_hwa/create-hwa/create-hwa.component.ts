import { Component, OnInit, NgZone, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';

import { DomSanitizer } from '@angular/platform-browser';
import { forEach } from '@angular/router/src/utils/collection';
import { toArray } from 'rxjs/operator/toArray';
import { isNumber } from 'util';
import { isNumeric } from 'rxjs/util/isNumeric';
import { Router } from '@angular/router';
import {AlertService} from '../../../../../services/alert.service';
import {OverlayDataService} from '../../../../../services/overlay-data.service';
import {UserService} from '../../../../../services/user.service';
import {HwaCommonService} from '../../../../../services/hwa-common.service';
declare let $: any;

@Component({
  selector: 'create-hwa',
  templateUrl: './create-hwa.component.html',
  styleUrls: ['./create-hwa.component.css']
})
export class CreateHwaComponent implements OnInit {
  private toolbarOptions = [
    'bold',
    'italic',
    'underline',
    { 'align': '' },
    { 'align': 'center' },
    { 'align': 'right' },
    { 'list': 'ordered' },
    { 'list': 'bullet' },
    { 'indent': '-1' },
    { 'indent': '+1' }
  ];
  public position = '';
  public posType = 'Full-Time';
  public describePos = '';
  public describeSkill = '';
  public jobDescription = {
    modules: {
      toolbar: this.toolbarOptions
    },
    placeholder: `•   Explain Job Responsibilities.
•   Add your work culture.
•   Add specific requirements like shifts, days etc.

`
  };
  public jobskills = {
    modules: {
      toolbar: this.toolbarOptions
    },
    placeholder: `Add Traits your employee must have.
•	  Add specific License or certification needed.

`
  };
  public HelpText = [
    {
      'label': 'Don\'t want to waste time looking at a application from Candidates that don\'t meet your minimum requirements?'
    },
    {
      'label': 'Take a few minutes to add Knockout Questions.'
    }

  ];
  public city: any;
  public addressTitle: any;
  public state: any;
  public HwaForm: FormGroup;
  public addLocationForm: FormGroup;
  private noLocation: boolean = false;
  private multiLocation: boolean = false;
  private title: any;
  private location: any[];
  private checkboxval: boolean = false;
  private passAddressToWin: any[];
  private nodeId: any = [];
  private showDialog: boolean = false;
  private launchPopup: boolean = false;
  private emptyLocation: boolean = false;

  public myModel = ''
  public mask = [/[0-9]/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public zipmask = [/[0-9]/, /\d/, /\d/, /\d/, /\d/];
  private displayLocation: boolean;
  private alertmsg: boolean = false;
  private showStyle: number
  private changeText: string = 'keyboard_arrow_down';
  private getCityinfoList = [];
  private validZipStatus: string;
  private showSelect: Array<boolean> = [];
  private selectedNofPostion: any = '1';
  private shoBasicHwa: boolean = true;
  private fromHwa: any = 'fromHwa';
  private validForOneAddrs: boolean;
  private myCompeyName: any;
  private level2: boolean;
  private positionError: boolean;
  private submitAttempt: boolean;
  private addresvalid: boolean;
  private addressLenght1: number = 0;
  private checkValidity: boolean;
  private MAX_LENGTH = 1000;
  private disableUntil: boolean;
  private btntext = 'Save & Continue';
  private addKoText= 'Add Knockout Questions';
  constructor(
    private ngZone: NgZone,
    private ele: ElementRef,
    private alertService: AlertService,
    private router: Router,
    private hwaOverlayService: OverlayDataService,
    private formbuilder: FormBuilder,
    private userService: UserService,
    private HwaServices: HwaCommonService,
    private domSanitizer: DomSanitizer) {
  }
  ngOnInit() {
    let user = this.userService.isLogedin();
    this.myCompeyName = user.details.field_name_of_your_business[0].value;
    this.HwaForm = this.formbuilder.group({
      'position': ['', Validators.required],
      'numberOfPosition': ['1'],
      'position_type': [''],
      'describePosition': ['', Validators.required],
      'describeSkill': ['', Validators.required],
      'locations': this.formbuilder.array([]),
    }, { validator: this.iScheckedLocation });
    // This form is form add location if blank
    this.addLocationForm = this.formbuilder.group({
      'addresses': this.formbuilder.array([
        this.initPopupLocation(),
      ]),
    });


    this.loadformdataIfExist();
    this.getCheckedLenght();
  }

  loadHwaDraftData() {
    let user = this.userService.isLogedin();
    this.HwaServices.loadDraftData('', user.uid).subscribe(
      res => {
        if (res) {
          this.position = res[0].title;
          this.selectedNofPostion = res[0].field_how_many_people_do_you_nee;
          this.posType = res[0].field_will_they_be_full_time_par;
          this.describePos = res[0].field_how_would_you_describe_thi;
          this.describeSkill = res[0].field_describe_the_skills_and_ex;
          this.checkAddress(res[0].nid);

          localStorage.setItem('storeHwaNid', res[0].nid);
        }

      },
      error => {
      });
  }
  // POPUP AddreSSS on button click
  loadformdataIfExist() {
    if (localStorage.getItem('storeHwaFormData')) {
      let formdata = JSON.parse(localStorage.getItem('storeHwaFormData'));
      this.position = formdata.title;
      this.selectedNofPostion = formdata.field_how_many_people_do_you_nee;
      this.posType = formdata.field_will_they_be_full_time_par;
      this.describePos = formdata.field_how_would_you_describe_thi;
      this.describeSkill = formdata.field_describe_the_skills_and_ex;

      this.checkAddress(formdata.nid);
    } else {
      this.checkAddress(null);
    }
  }


  initPopupLocation() {
    return this.formbuilder.group({
      'title': ['', Validators.required], // , Validators.required
      'field_address_line_2': [''],
      'field_z': ['', [Validators.pattern(/(^\d{5}$)|(^\d{5}-\d{4}$)/), Validators.required]],
      'field_state': ['', Validators.required],
      'field_city': ['', Validators.required]
    });
  }
  // Event For popup Addresses
  zipValidator(zip) {
    let valid = /^\d{5}$/.test(zip.value);
    if (valid) {
      return null;
    }
    return { 'invalidZip': true };
  }
  addPopLocation() {

    // add address to the list
    const control = <FormArray>this.addLocationForm.controls['addresses'];
    control.push(this.initPopupLocation());
  }
  removePopLocation(i: number) {
    const control = <FormArray>this.addLocationForm.controls['addresses'];
    control.removeAt(i);
  }
  // Save POpup address
  savePopupAddress() {
    $('.emptyLocation').hide();
    this.alertmsg = true;
    let user = this.userService.isLogedin();
    let locationObj = {
      'uid': user.uid,
      'address': this.addLocationForm.value.addresses
    };
    this.HwaServices.popUpAddLocation(locationObj).subscribe(
      res => {
        this.displayLocation = true;
        this.checkAddress(null);
        this.addLocationForm.reset();

        this.getCityinfoList = [];
        this.alertService.success('Location Added Successfully', true);
        this.autoHideAlertMsg(7000);
        //
        if (this.launchPopup) { this.launchPopup = false;
        } else { this.launchPopup = true; }
      },
      error => {
      });
  }
  //
  getPositions(event) {
    let locationCount: number;
    this.checkboxval = event.target.checked;
    locationCount = this.getCheckedLenght().length;
    this.addressLenght1 = locationCount;

    if (this.HwaForm.controls['numberOfPosition'].value < this.addressLenght1) {
      this.checkValidity = true;
    } else {
      this.checkValidity = false;
    }
  }
  getIndex(event) {
    let locationCount: number;
    this.checkboxval = event.target.checked;
    locationCount = this.getCheckedLenght().length;
    this.addressLenght1 = locationCount;

    if (this.HwaForm.controls['numberOfPosition'].value < this.addressLenght1) {
      this.checkValidity = true;
    } else {
      this.checkValidity = false;
    }

    if (this.checkLocation()) {
      this.addresvalid = false;
    } else {
      this.addresvalid = true;
    }

  }
  checkLocation(): boolean {
    let isSelectedLocation = false;
    const control = <FormArray>this.HwaForm.controls['locations'];
    for (let i = 0; i < control.value.length; i++) {
      if (control.value[i].location) {
        isSelectedLocation = true;
        break;
      }
    }
    return isSelectedLocation;
  }

  iScheckedLocation(control: FormGroup): { [s: string]: boolean } {
    let isSelectedLocation = false;
    if (!control) {
      return { emailNotMatch: true };
    }

    //  const control = control;
    for (let i = 0; i < control.controls['locations'].value.length; i++) {
      if (control.controls['locations'].value[i].location) {
        isSelectedLocation = true;
        break;
      }
    }
    if (isSelectedLocation) {
      return null;
    } else {
      return { 'locationNotMatch': true };
    }
  }
  initAddress(value, lbl, nid) {
    return this.formbuilder.group({
      'location': new FormControl(value),
      'label': new FormControl(lbl),
      'nid': new FormControl(nid),
    });
  }
  //

  isSingleCheck(control: FormGroup): { [s: string]: boolean } {
    if (!control) {
      return { emailNotMatch: true };
    }

    if (control.controls['location'].value) {
      return null;
    } else {
      return { 'emailNotMatch': true };
    }
  }
  skipThis() {
    this.router.navigate(['/addKnockoutQuestion']);
  }
  onPreiview() {
    if (this.showDialog) {
      this.showDialog = false;
    } else {
      this.showDialog = true;
    }
    let selectAddress = [];

    for (let i = 0; i < this.HwaForm.value.locations.length; i++) {
      if (this.HwaForm.value.locations[i].location) {
        selectAddress.push(this.HwaForm.value.locations[i].nid);
      }
    }
    let HwaForm = { 'addressLenght': selectAddress.length,
      'formData': this.HwaForm.value,
      'noOfPosition': this.HwaForm.value.numberOfPosition,
      'companyname': this.myCompeyName,
      'shoBasicHwa': this.shoBasicHwa = true,
      'bottomText': this.HelpText,
      'address1': this.city,
      'address3': this.addressTitle,
      'address2': this.state };
    this.hwaOverlayService.hwaData(HwaForm);
  };

  // Add Addresses if no address
  addLocation() {
    if (this.launchPopup) {
      this.launchPopup = false;
    } else {
      this.launchPopup = true;
    }
  }
  cancelLocation() {
    if (this.launchPopup) {
      this.launchPopup = false;
    } else {
      this.launchPopup = true;
    }
    return false;
  }

  formvaladation(): boolean {
    let flag = true;
    if (!this.HwaForm.controls['position'].valid) {
      flag = false;
      this.positionError = true;

    } else {
      this.positionError = false;
    }
    return flag;
  }
  clientValidation() {
    if (this.HwaForm.controls['position'].value === '') {
      $('.positionerror').show();
      $('.mdl-textfield__input').focus();
      $('body').animate({ scrollTop: 0 }, 500);
      this.btntext = 'Save & Continue';
      this.disableUntil = false;
    } else if (this.emptyLocation === true) {
      $('.emptyLocation').show().focus();
      $('html, body').animate({ scrollTop: $('.realtimeLocation').offset().top }, 500);
      this.btntext = 'Save & Continue';
      this.disableUntil = false;
    } else if (this.HwaForm.controls['describePosition'].invalid) {
      $('.describePositionFocus .ql-editor').focus();
      this.btntext = 'Save & Continue';
      this.disableUntil = false;
    } else if (this.HwaForm.controls['describeSkill'].invalid) {
      $('.describeSkillFocus .ql-editor').focus();
      this.btntext = 'Save & Continue';
      this.disableUntil = false;
    }



  }
  onSubmit(from) {
    this.btntext = 'Processing...';
    this.addKoText = 'Processing';
    this.disableUntil = true;
    this.clientValidation();
    this.submitAttempt = true;

    if (this.HwaForm.valid) {
      this.alertmsg = true;
      let user = this.userService.isLogedin();

      let selectedNid = [];
      for (let i = 0; i < this.HwaForm.value.locations.length; i++) {
        if (this.HwaForm.value.locations[i].location) {
          selectedNid.push(this.HwaForm.value.locations[i].nid);
        }
      }

      let hid = localStorage.getItem('storeHwaNid');
      if (hid) {

      } else {
        hid = '';
      }
      let CreteObj = {
        'hwa_nid': hid,
        'uid': user.uid,
        'title': this.HwaForm.value.position,
        'field_how_many_people_do_you_nee': this.HwaForm.value.numberOfPosition,
        'field_will_they_be_full_time_par': this.HwaForm.value.position_type,
        'field_how_would_you_describe_thi': this.HwaForm.value.describePosition,
        'field_describe_the_skills_and_ex': this.HwaForm.value.describeSkill,
        'nid': selectedNid
      };

      localStorage.setItem('storeHwaFormData', JSON.stringify(CreteObj));

      this.HwaServices.createHWA(CreteObj).subscribe(
        res => {
          localStorage.setItem('storeHwaNid', res.details[0].nid[0]['value']);
          this.alertService.success('Your have created Help Wanted Ad Successfully', true);
          this.autoHideAlertMsg(1500);
          if (from === 'fromOverlay') {
            this.router.navigate(['/addKnockoutQuestion']);
          } else {
            setTimeout(() => {
              this.level2 = true;
            }, 1500);
          }
          if (from === 'postMyad') {
            this.router.navigate(['/postmyad']);
          } else {
            setTimeout(() => {
              this.level2 = true;
            }, 1500);
          }

setTimeout(() => {
            this.btntext = 'Save & Continue';
            this.disableUntil = false;
            this.addKoText = 'Add Knockout Questions';
          }, 1500);
        },
        error => {
          this.alertService.success(error, true);
        });
    } else {

    }

  }


  getCheckedLenght(): any {
    let locArr = [];
    const control = <FormArray>this.HwaForm.controls['locations'];
    for (let i = 0; i < control.value.length; i++) {
      if (control.value[i].location) {
        locArr.push(control.value[i].location);
      }
    }
    return locArr;
  }

  autoHideAlertMsg(time) {
    setTimeout(() => {
      this.alertmsg = false;
    }, time);
  }

  toggleHighlight(newValue: number) {
    this.changeText = 'keyboard_arrow_down';
    if (this.showStyle === newValue) {
      this.showStyle = 0;
      this.changeText = 'keyboard_arrow_down';
    } else {
      this.showStyle = newValue;
      this.changeText = 'keyboard_arrow_up';
    }
  }

  zipCodeSearch(count) {
    let zipc = this.addLocationForm.value.addresses[count].field_z;

    if (!isNaN(zipc)) {

      this.userService.searchZip(zipc).subscribe(
        res => {
          if (this.validUsCountry(res)) {
            if (res['results'][0].postcode_localities) {
              this.showSelect[count] = true;
              this.getCityinfoList = [];
              for (let j = 0; j < res['results'][0].postcode_localities.length; j++) {
                this.getCityinfoList.push(res['results'][0].postcode_localities[j]);
              }
            } else {
              this.showSelect[count] = false;
            }

            for (let i = 0; i <= res['results'][0].address_components.length - 1; i++) {

              if (res['results'][0].address_components[i].types[0] === 'administrative_area_level_1') {
                let getstate = res['results'][0].address_components[i].short_name;
                let getCity = res['results'][0].address_components[1].long_name;
                let arryobj = [];
                arryobj[count] = { field_state: getstate, field_city: getCity };
                this.addLocationForm.controls['addresses'].patchValue(arryobj);

                this.validZipStatus = '';
              }
            }
          } else {
            this.validZipStatus = 'Please enter valid US zip code';
            this.showSelect[count] = false;
            let arryobj = [];
            arryobj[count] = { field_state: '', field_city: '' };
            this.addLocationForm.controls['addresses'].patchValue(arryobj);
          }
        });
    } else {
      this.validZipStatus = 'Please enter valid US zip code';
      this.showSelect[count] = false;
      let arryobj = [];
      arryobj[count] = { field_state: '', field_city: '' };
      this.addLocationForm.controls['addresses'].patchValue(arryobj);
    }
  }

  removeAllLocation() {
    let control = <FormArray>this.HwaForm.controls['locations'];
    for (let i = control.length; i >= 0; i--) {
      control.removeAt(i);
    }
  }

  checkAddress(nidArray) {
    // console.log(nidArray)
    let user = this.userService.isLogedin();
    this.userService.getaddress(user.uid).subscribe(
      res => {
        this.removeAllLocation();
        if (Object.keys(res).length === 0) {
          this.emptyLocation = true;
          this.noLocation = true;
          this.displayLocation = true;
          this.validForOneAddrs = false;
        }
        if (Object.keys(res).length > 1) {
          this.emptyLocation = false;
          this.multiLocation = true;
          this.validForOneAddrs = false;
          this.title = res;
          //
          let control = <FormArray>this.HwaForm.controls['locations'];
          for (let i = 0; i <= this.title.length - 1; i++) {

            let addresslbl = this.title[i].title + ', ' + this.title[i].field_city + ', ' + this.title[i].field_state;
            if (nidArray) {
              if (this.findSelectedAddr(this.title[i].nid, nidArray)) {
                control.push(this.initAddress(true, addresslbl, this.title[i].nid));
              } else {
                control.push(this.initAddress(false, addresslbl, this.title[i].nid));
              }
            } else {
              if (i === 0) {
                control.push(this.initAddress(true, addresslbl, this.title[i].nid));
              } else {
                control.push(this.initAddress(false, addresslbl, this.title[i].nid));
              }
            }

            this.noLocation = true;
            this.displayLocation = true;
          }
          this.city = '';
          this.state = '';
          this.addressTitle = '';
        }
        if (Object.keys(res).length === 1) {
          this.emptyLocation = false;
          this.city = res[0].field_city;
          this.state = res[0].field_state;
          this.addressTitle = res[0].title;

          this.validForOneAddrs = true;
          this.noLocation = true;
          this.displayLocation = true;
          this.title = res;
          let control = <FormArray>this.HwaForm.controls['locations'];

          for (let i = 0; i <= this.title.length - 1; i++) {
            let addresslbl = this.title[i].title + ', ' + this.title[i].field_city + ', ' + this.title[i].field_state;
            // this.nodeId.push(this.title[i].nid);
            if (i === 0) {
              control.push(this.initAddress(true, addresslbl, this.title[i].nid));
              //  control[i]=this.initAddress(true, addresslbl, this.title[i].nid);
            } else {
              control.push(this.initAddress(false, addresslbl, this.title[i].nid));
              //  control[i]=this.initAddress(false, addresslbl, this.title[i].nid);
            }
          }
        } else {
          this.city = '';
          this.state = '';
        }
      });
  }

  findSelectedAddr(ids, idArray) {
    let flag = false;
    for (let j = 0; j <= idArray.length - 1; j++) {
      if (idArray[j] === ids) {
        flag = true;
      }
    }
    return flag;
  }
  validUsCountry(res): boolean {
    let isUs = false;
    if (res['status'] === 'OK') {
      for (let i = 0; i <= res['results'][0].address_components.length - 1; i++) {
        if (res['results'][0].address_components[i].short_name === 'US') {
          isUs = true;
          break;
        }
      }
    }
    return isUs;
  }
  goToNext() {
    this.router.navigate(['/addKnockoutQuestion']);
  }
  Close2() {
    if (this.level2) {
      this.level2 = false;
    } else {
      this.level2 = true;
    }
  }
  textChanged($event) {
    if ($event.editor.getLength() > this.MAX_LENGTH) {
      $event.editor.deleteText(this.MAX_LENGTH, $event.editor.getLength());
      alert('You can Entered only 1000 characters');
    }
  }

}
