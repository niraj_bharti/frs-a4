import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { forEach } from '@angular/router/src/utils/collection';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import {AlertService} from '../../../../../services/alert.service';
import {OverlayDataService} from '../../../../../services/overlay-data.service';
import {UserService} from '../../../../../services/user.service';
import {HwaCommonService} from '../../../../../services/hwa-common.service';
import {EditableQuestions} from '../../../../../misce/global/directives/editable-question';

@Component({
  selector: 'app-knockout-question',
  templateUrl: './knockout-question.component.html',
  styleUrls: ['./knockout-question.component.css']
})
export class KnockoutQuestionComponent implements OnInit {
  private HelpText = [
    {
      'label': 'Now that you\'re saving time by having unqualif ied applicants screened out, would you like to have Talentrackr™'
},
{
  'label': 'Automatically score the remaining ones so you know exactly which candidates to spend time on?'
}

];
private defaultQLists = [];
private customQList = [];
private isEditable = [];
private showTextfield: boolean;
private enb: boolean;
private showDialog: boolean;
private showKo: boolean;
public koForm: FormGroup;
private alertmsg: boolean;
private nodeId: any;
private hwaId: number;
private customlist: any;
private displayQuestionn: boolean;
private changeText = []; // 'Edit';
@ViewChild(EditableQuestions) input: EditableQuestions;
private koData: any = 'koData';
private show = false;
private koDataStr: string = 'koData';
private qusInpitValue: any;
private level2: boolean;
private btndisabled: boolean;
private disableUntil: boolean;
private btntext = 'Save & Continue';
constructor(
  private alertService: AlertService,
  private router: Router,
  private hwaOverlayService: OverlayDataService,
  private formbuilder: FormBuilder,
  private userService: UserService,
  private HwaServices: HwaCommonService,
  private domSanitizer: DomSanitizer) { }


ngOnInit() {


  this.koForm = this.formbuilder.group({
    'defaultQlists': this.formbuilder.array([]),
    'customQlist': this.formbuilder.array([])
  });
  this.getKoQuestion();
  //   this.loadkoDraftData();

}

loadkoDraftData() {
  let user = this.userService.isLogedin();
  let hwaId = localStorage.getItem('storeHwaNid');
  if  (hwaId) {

  } else {
    this.router.navigate(['/hwa_workflow']);
    return false;
  }
  this.HwaServices.loadkosDraftData(hwaId, user.uid).subscribe(
    res => {
      if  (res) {
        let qarray: Array<any> = res as Array<any>;
        for (let i = 0; i < qarray.length; i++) {
          if  (qarray[i].field_question_type === 'Default') {
            this.checkdefauldQuestion(qarray[i].title, qarray[i].nid, qarray[i].field_ko);
          } else {
            let todo = { 'value': qarray[i].title };
            todo.value = qarray[i].title;
            this.addQuestion(todo, qarray[i].nid, qarray[i].field_ko);
            this.btndisabled = false;
          }
        }
      }
      this.displayQuestionn = true;
    },
    error => {
    });

}
checkdefauldQuestion(textval, nid, isSelected) {
  for (let i = 0; i < this.koForm.controls['defaultQlists'].value.length; i++) {

    if  (this.koForm.controls['defaultQlists'].value[i].inputval === textval) {
      let selectedval = false;
      if  (isSelected === 1) { selectedval = true; } else if  (isSelected === 0) { selectedval = false } else { selectedval = isSelected }
      let control = this.koForm.controls['defaultQlists']['controls'][i];
      control.patchValue({ 'defaultQlist': selectedval, 'nid': nid });
    }
  }
}
getKoQuestion() {
  this.HwaServices.defaultQuestion().subscribe(
    res => {
     console.log(res)
      this.defaultQLists = res;

      for (let j = 0; j < this.defaultQLists.length; j++) {
        const control = <FormArray>this.koForm.controls['defaultQlists'];
        // console.log(this.defaultQLists[j].name);
        control.push(this.initdefaultQuestion(this.defaultQLists[j].name, this.defaultQLists[j].nid));
      }
      this.loadkoDraftData();
    },
    error => {

    }
  );
}
initdefaultQuestion(val, qnid) {
  return this.formbuilder.group({
    'defaultQlist': [true], //  , Validators.required
    'inputval': [val],
    'nid': [qnid]
  });
}
resetAllEditbutton(i) {
  for (let j = 0; j < this.isEditable.length; j++) {
    if  (j !== i) {
      this.changeText[j] = 'Edit';
      this.isEditable[j] = false;
    }
  }
}
checkEmpty(e) {
  if  (e.value === '') {
    this.btndisabled = true;
  } else {
    this.btndisabled = false;
  }

}
toggleShow(i) {
  this.resetAllEditbutton(i);
  //  this.changeText[i] = 'Edit';
  if  (this.isEditable[i] === true) {
    this.changeText[i] = 'Edit';
  } else {
    this.changeText[i] = 'Save';
  }
  this.isEditable[i] = !this.isEditable[i];
};

editText(evt, i) {
  if  (this.isEditable[i] === true) {
    this.changeText[i] = 'Edit';
  } else {
    this.changeText[i] = 'Save';
  }
  this.isEditable[i] = !this.isEditable[i];
  this.customQList[i] = evt.target.value;

}
finshCode() {
}
// Add Custom question
addCustomQuestion() {
  if  (this.showTextfield) {
    this.showTextfield = false;
    this.btndisabled = false;
  } else {
    this.showTextfield = true;
    this.btndisabled = true;
    // this.enb = false;
  }
  // console.log(this.qusInpitValue)
  //   this.btndisabled = true;
}
addQuestion(todo: any, qnid: any, selected: any) {
  if  (todo.value !== '') {
    this.customQList.push(todo.value);
    this.isEditable.push(false);
    this.changeText.push('Edit');
    const control = <FormArray>this.koForm.controls['customQlist'];
    if  (selected === 1) { selected = true; } else if  (selected === 0) { selected = false; }
    control.push(this.initQuestion(todo.value, qnid, selected));
    todo.value = null;
    //   if (qnid){
    this.btndisabled = true;
    //   }

    //   this.showTextfield = false;
    this.resetAllEditbutton(9999);
  }
  return false;
}
initQuestion(val: any, qnid: any, selected: boolean) {
  return this.formbuilder.group({
    'inputval': [val], //  , Validators.required
    'costomQus': [selected],
    'nid': [qnid]
  });
}
onPreiview() {

  if  (this.showDialog) {
    this.showDialog = false;
  } else {
    this.showDialog = true;
  }

  let koPreive = { 'showKo': this.showKo = true,
    'bottomText': this.HelpText,
    'defaultQ': this.getSelectedDfaultQus(),
    'customQ': this.getSelectedQuestion() };
  this.hwaOverlayService.koData(koPreive);
}

getSelectedQuestion(): Array<any> {
let customArray = [];
let control = this.koForm.controls['customQlist'].value;
for (let j = 0; j < control.length; j++) {
  if  (control[j].costomQus) {
    customArray.push(control[j].inputval);
  }
}
return customArray;
}

getSelectedDfaultQus(): Array<any> {
  let customArray = [];
let control = this.koForm.controls['defaultQlists'].value;
for (let j = 0; j < control.length; j++) {
  // console.log(control[j])
  if  (control[j].defaultQlist) {
    customArray.push(control[j].inputval);
  }
}
return customArray;
}

autoHideAlertMsg(time) {
  setTimeout(() => {
    this.alertmsg = false;
  }, time);
}
onSubmit(from) {
  this.disableUntil = true;
  this.btntext = 'Processing...';
  if  (localStorage.getItem('storeHwaNid')) {
    this.alertmsg = true;
    let user = this.userService.isLogedin();
    //  console.log(this.HwaServices.hwaNid)
  // start sssssssss
    let dummyObj = [];
    let dummyCustom = [];

    let control = this.koForm.controls['customQlist'].value;
    for (let j = 0; j < control.length; j++) {

      let custObj = {
        'nid': control[j].nid,
        'question': control[j].inputval,
        'field_question_type': 'custom',
        'field_ko': control[j].costomQus ? 1 : 0,
        'status': '1'
      };
      dummyCustom.push(custObj);
    }
    let control1 = this.koForm.controls['defaultQlists'].value;
    for (let i = 0; i < control1.length; i++) {

      let newObj = {
        'nid': control1[i].nid,
        'question': control1[i].inputval,
        'field_question_type': 'default',
        'field_ko': control1[i].defaultQlist ? 1 : 0,
        'status': '1'
      };
      dummyObj.push(newObj);
    }

    let newObjValue = dummyObj.concat(dummyCustom);
    let objTemplate = {
      'uid': user.uid,
      'hwa_nid': localStorage.getItem('storeHwaNid'),
      'ko': newObjValue
    };

    this.HwaServices.customQuestion(objTemplate).subscribe(
      res => {
        if  (from === 'fromKoOverlay') {


          this.disableUntil = false;
          this.router.navigate(['/AddSkillsAndExpertiseQuestions']);
        } else {
          setTimeout(() => {
            this.level2 = true;
          }, 1500);
        }

        if  (from === 'postMyad') {
          this.router.navigate(['/postmyad']);
        } else {
          setTimeout(() => {
            this.level2 = true;
            this.alertService.success('Knockout Questions Created Successfully', true);
            this.autoHideAlertMsg(2500);
            this.btntext = 'Save & Continue';
          }, 1500);
        }
      },
      error => {
      }
    );
  } else {
    alert('You must have to create and save first step of Help Wanted Ad');

  }
}
onSkip() {
  this.router.navigate(['/AddSkillsAndExpertiseQuestions']);
}
goToNext() {
  this.router.navigate(['/AddSkillsAndExpertiseQuestions']);

}
Close2() {
  if (this.level2) {
    this.level2 = false;
  } else {
    this.level2 = true;
  }
}
}


