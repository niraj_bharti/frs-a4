import {Component, OnInit, ElementRef} from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormControl} from "@angular/forms";
import {Router} from "@angular/router";

import {forEach} from "@angular/router/src/utils/collection";
import {toArray} from "rxjs/operator/toArray";
import {HwaCommonService} from "../../../../../../services/hwa-common.service";
import {UserService} from "../../../../../../services/user.service";
import {AlertService} from "../../../../../../services/alert.service";
import {OverlayDataService} from "../../../../../../services/overlay-data.service";



@Component({
  selector: 'app-step3',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.css']
})
export class Step3Component implements OnInit {
  private toolbarOptions = [
    'bold',
    'italic',
    'underline',
    {'align':''},
    {'align':'center'},
    {'align':'right'},
    {'list': 'ordered'},
    {'list': 'bullet' },
    {'indent': '-1'},
    {'indent': '+1' }
    ];
    public jobDescription = {
      modules: {
        toolbar: this.toolbarOptions
      },
      placeholder: `write about your business... `
    };
    public  buzProfile:FormGroup;
    private fromProfile:any="fromProfile";
    private filesToUpload: Array<File>;
    private imgbase64src:any;
    private alertmsg:boolean;
    private titalText:any;
    private descriptionText:any;
    private showDialog:boolean;
    private myCompeyName:any;
    private isThereImge:boolean;
  private imgLoading:boolean;
  private showDeleteBtn:boolean;
    private tooltipWin:boolean = false;
  constructor(
              private ele: ElementRef,
              private alertService: AlertService,
              private userService: UserService,
              private router:Router,
              private hwaOverlayService: OverlayDataService,
              private formbuilder: FormBuilder,
              private HwaServices: HwaCommonService
            ) { }

  ngOnInit() {
       let hwaId = localStorage.getItem('storeHwaNid');
        if(hwaId){

       }else{
          this.router.navigate(['/hwa_workflow']);
         // return false;
       }
    if(localStorage.getItem('profileNid') && localStorage.getItem('prof3_img_nid')){
     this.showDeleteBtn = true;
      }else{
        this.showDeleteBtn = false;
      }
    this.imgbase64src = "";
    this.isThereImge = false;
    let user = this.userService.isLogedin();
    this.myCompeyName = user.details.field_name_of_your_business[0].value;

    this.buzProfile = this.formbuilder.group({
      'tital':['',Validators.required],
      'description':['',Validators.required],
      'profileImage':[''],
    //  'locations':this.formbuilder.array([]),
    }); //{validator:this.iScheckedLocation}
    let prof_nid = localStorage.getItem('profileNid');
  //  prof_nid = '1095';
    if(prof_nid){
      this.loadProfileDraftData(prof_nid);
    }else{
      this.titalText = "Sell them on the job"; //res[0]['title'][0].value
    }
  }
loadProfileDraftData(profId) {

  this.HwaServices.getProfileDraftData(profId).subscribe(
     res => {
       console.log(res[0]);
       if(res[0]['field_title3'][0]) {
       this.titalText = res[0]['field_title3'][0].value
      }else{
        this.titalText = "Sell them on the job"; //res[0]['title'][0].value
      }
      if(res[0]['field_body3'][0]) {
       this.descriptionText  = res[0]['field_body3'][0].value;
     }
       if(res[0]['field_image3'][0]) {
         this.imgbase64src = res[0]['field_image3'][0].url;
         this.isThereImge = true;
         this.imgLoading=false;
          localStorage.setItem('prof3_img_nid', res[0]['field_image3'][0].target_id);
          if( this.imgbase64src){
            this.showDeleteBtn = true;
          }else{
            this.showDeleteBtn = false;
          }
       }else{
         this.showDeleteBtn = false;
         this.isThereImge = false;
       }

       if(this.titalText !== '' || this.descriptionText !== '' || this.imgbase64src !== '' ) {
       let profForm = {"tital": this.titalText, "description":this.descriptionText, "image":this.imgbase64src, "companyname":this.myCompeyName, "showProfile":true}
       this.hwaOverlayService.profileData(profForm, 2);
       this.removenextSteps();
     }else{
        this.hwaOverlayService.profileData(null, 2);
        this.removenextSteps()
     }
     })
}
SaveBusinessProfile() {

var step3Data = this.getObjectForSave();
//localStorage.setItem('step3Profile', step3Data);
//let mergeddata = this.HwaServices.mergeAllStaps();
 if(this.titalText !== '' || this.descriptionText !== '' || this.imgbase64src !== '' ) {
let profForm = {"tital": this.titalText, "description":this.descriptionText, "image":this.imgbase64src, "companyname":this.myCompeyName, "showProfile":true}
this.hwaOverlayService.profileData(profForm, 2);
this.removenextSteps()
}else{
   this.hwaOverlayService.profileData(null, 2);
   this.removenextSteps()
}
        this.HwaServices.createProfile(step3Data).subscribe(
           res => {

             localStorage.setItem('profileNid', res['details'][0].nid[0]['value'] );
             //localStorage.removeItem('step3Profile');
             this.alertService.success("Business Profile Created Successfully", true);
             this.autoHideAlertMsgAndGoNext(600);

          },
           error => {
             console.log(error)
             this.alertService.success(error, true);
           });

    }
  skipThis() {
     this.hwaOverlayService.profileData(null, 2);
    this.router.navigate(['/businessprofile/step-four']);
    return false;
  }

fileChangeEvent (fileInput: any) {
  this.imgLoading=true;
       this.filesToUpload = <Array<File>> fileInput.target.files;
       //var formData: any = new FormData();
       var file:File = this.filesToUpload[0];
       //formData.append( "myfile", file, file.name );
      var myReader:FileReader = new FileReader();
      myReader.onloadend = (e) => {
        //this.imgbase64src = myReader.result;
        console.log( file.name);
        var imgData = {
        "image_data":myReader.result.split(',')[1],
        "name":file.name
        }
        this.HwaServices.uploadProfileImage(imgData).subscribe(
           res => {
             console.log(res);
             if(res['fid']) {
               localStorage.setItem('prof3_img_nid', res['fid']);
                this.imgbase64src = res['image'];
                this.isThereImge = true;
                this.showDeleteBtn = true;
               setTimeout(() => {
                 this.imgLoading = false;
               }, 1500);
             }else{
               //this.isThereImge = false;
             }

           })
      }

      myReader.readAsDataURL(file);
}

    autoHideAlertMsgAndGoNext(time) {
      setTimeout(() => {
        this.alertmsg = false;
        this.router.navigate(['/businessprofile/step-four'])
      }, time)
    }

previewData() {
  this.showDialog = true;
//  var step3Obj = this.getObjectForSave();
//  localStorage.setItem('step3Profile', step3Obj);
 if(this.titalText !== '' || this.descriptionText !== '' || this.imgbase64src !== '' ) {
  let profForm = {"tital": this.titalText, "description":this.descriptionText, "image":this.imgbase64src, "companyname":this.myCompeyName, "showProfile":true}
  this.hwaOverlayService.profileData(profForm, 2);
  this.removenextSteps();
}else{
   this.hwaOverlayService.profileData(null, 2);
   this.removenextSteps();
}
  //let objArray = this.hwaOverlayService.allStapesData;
//  objArray[3] = profForm;
//  this.hwaOverlayService.allStapesData = objArray;

}
removenextSteps() {

    //this.hwaOverlayService.profileData(null, 2);
    this.hwaOverlayService.profileData(null, 3);
}
getObjectForSave():any {
  let prof3_img_nid =  localStorage.getItem('prof3_img_nid');
  var stepData = {
          "bptitle3":this.buzProfile.value.tital,
          "bpbody3": this.buzProfile.value.description,
          "image3_fid":prof3_img_nid,
          }
  return stepData;
}

deleteThis(field){
    if(localStorage.getItem('profileNid')){
      this.showDeleteBtn = true;
    let deletImgObj ={
    "bp_nid":localStorage.getItem('profileNid'),
    "image1_fid":"",
    "image2_fid":"",
    "image3_fid":"yes",
    "image4_fid":""
    }

this.HwaServices.deleteImg(deletImgObj).subscribe(
    res => {
    console.log(res);
              this.showDeleteBtn = false;
              this.imgbase64src = '';
              this.isThereImge = false;
              this.imgLoading=false;
              localStorage.setItem('prof3_img_nid', '');
              localStorage.removeItem('prof3_img_nid');
    })
  }else{
    this.showDeleteBtn = false;
  }
  this.imgbase64src = "";
this.isThereImge = false;
this.imgLoading=false;
localStorage.setItem('prof3_img_nid', '');
localStorage.removeItem('prof3_img_nid');
}

//--------------------------------------------------------------------
closeWin() {
this.tooltipWin = false;
}

openWin(){
  this.tooltipWin = true;
}

}
