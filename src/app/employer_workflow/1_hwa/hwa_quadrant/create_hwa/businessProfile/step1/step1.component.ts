import {Component, OnInit, ElementRef} from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormControl} from "@angular/forms";
import {Router} from "@angular/router";
import {forEach} from "@angular/router/src/utils/collection";
import {toArray} from "rxjs/operator/toArray";
import {AlertService} from '../../../../../../services/alert.service';
import {UserService} from '../../../../../../services/user.service';
import {OverlayDataService} from '../../../../../../services/overlay-data.service';
import {HwaCommonService} from '../../../../../../services/hwa-common.service';


@Component({
  selector: 'app-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.css']
})
export class Step1Component implements OnInit {
  private toolbarOptions = [
    'bold',
    'italic',
    'underline',
    {'align':''},
    {'align':'center'},
    {'align':'right'},
    {'list': 'ordered'},
    {'list': 'bullet' },
    {'indent': '-1'},
    {'indent': '+1' }
    ];
    public jobDescription = {
      modules: {
        toolbar: this.toolbarOptions
      },
      placeholder: `write about your business... `
    };
    public  buzProfile:FormGroup;
    private fromProfile:any="fromProfile";
    private filesToUpload: Array<File>;
    private imgbase64src:any;
    private alertmsg:boolean;
    private titalText:any;
    private descriptionText:any;
    private showDialog:boolean;
    private myCompeyName:any;
    private isThereImge:boolean;
    private imgLoading:boolean;
    private showDeleteBtn:boolean;
    private tooltipWin:boolean = false;
  constructor(
              private ele: ElementRef,
              private alertService: AlertService,
              private userService: UserService,
              private router:Router,
              private hwaOverlayService:OverlayDataService,
              private formbuilder: FormBuilder,
              private HwaServices: HwaCommonService
            ) { }

  ngOnInit() {
    let hwaId = localStorage.getItem('storeHwaNid');
        if(hwaId){

       }else{
        // this.alertService.success("Need to create HWA", true);
          this.router.navigate(['/hwa_workflow']);

          // return false;
       }
console.log('prof1_img_nid ', localStorage.getItem('prof1_img_nid'));
if(localStorage.getItem('profileNid') && localStorage.getItem('prof1_img_nid')){
 // if(localStorage.getItem('prof1_img_nid')){
     this.showDeleteBtn = true;
  //}
}else{
  this.showDeleteBtn = false;
}
    this.imgbase64src = "";
    this.isThereImge = false;
    let user = this.userService.isLogedin();
    this.myCompeyName = user.details.field_name_of_your_business[0].value;

    this.buzProfile = this.formbuilder.group({
      'tital':['',Validators.required],
      'description':['',Validators.required],
      'profileImage':[''],
    //  'locations':this.formbuilder.array([]),
    }); //{validator:this.iScheckedLocation}
    let prof_nid = localStorage.getItem('profileNid');
    console.log(prof_nid);
  //  prof_nid = '1095';
    if(prof_nid){
      this.loadProfileDraftData(prof_nid);
    }else{
       this.titalText = "";
    }


  }
loadProfileDraftData(profId) {

  this.HwaServices.getProfileDraftData(profId).subscribe(
     res => {
     //  console.log(res[0]);
    if(res[0]['field_title1'][0]) {
       this.titalText = res[0]['field_title1'][0].value
     }else{
       this.titalText = ""; //res[0]['title'][0].value
     }
      if(res[0]['body'][0]) {
       this.descriptionText  = res[0]['body'][0].value;
     }
       if(res[0]['field_image1'][0]) {
         this.imgbase64src = res[0]['field_image1'][0].url;
          this.isThereImge = true;
          this.imgLoading=false;

          localStorage.setItem('prof1_img_nid', res[0]['field_image1'][0].target_id);
          if( this.imgbase64src){
            this.showDeleteBtn = true;
          }else{
            this.showDeleteBtn = false;
          }

       }else{
         this.showDeleteBtn = false;
         this.isThereImge = false;
       }
      if(this.titalText !== '' || this.descriptionText !== '' || this.imgbase64src !== '' ) {
       let profForm = {"tital": this.titalText, "description":this.descriptionText, "image":this.imgbase64src, "companyname":this.myCompeyName, "showProfile":true}
       this.hwaOverlayService.profileData(profForm, 0);
          this.removenextSteps();
       } else {
          this.hwaOverlayService.profileData(null, 0);
             this.removenextSteps();
       }
     })
}
SaveBusinessProfile() {

var step1Data = this.getObjectForSave();
//localStorage.setItem('step1Profile', step1Data);
//let mergeddata = this.HwaServices.mergeAllStaps();
 if(this.titalText !== '' || this.descriptionText !== '' || this.imgbase64src !== '' ) {
let profForm = {"tital": this.titalText, "description":this.descriptionText, "image":this.imgbase64src, "companyname":this.myCompeyName, "showProfile":true}
this.hwaOverlayService.profileData(profForm, 0);
 this.removenextSteps();
}else{
   this.hwaOverlayService.profileData(null, 0);
      this.removenextSteps();
}
        this.HwaServices.createProfile(step1Data).subscribe(
           res => {

             localStorage.setItem('profileNid', res['details'][0].nid[0]['value'] );
             //localStorage.removeItem('step1Profile');
             this.alertService.success("Business Profile Created Successfully", true);
             this.autoHideAlertMsgAndGoNext(600);

          },
           error => {
             console.log(error)
             this.alertService.success(error, true);
           });

    }
  skipThis() {
      this.hwaOverlayService.profileData(null, 0);
    this.router.navigate(['/businessprofile/step-two']);
    return false;
  }

fileChangeEvent (fileInput: any) {
        this.imgLoading=true;
       this.filesToUpload = <Array<File>> fileInput.target.files;
       //var formData: any = new FormData();
       var file:File = this.filesToUpload[0];
       //formData.append( "myfile", file, file.name );
      var myReader:FileReader = new FileReader();
      myReader.onloadend = (e) => {
        //this.imgbase64src = myReader.result;
        console.log( file.name);
        var imgData = {
        "image_data":myReader.result.split(',')[1],
        "name":file.name
        }
        this.HwaServices.uploadProfileImage(imgData).subscribe(
           res => {
             console.log(res);
             if(res['fid']) {
               localStorage.setItem('prof1_img_nid', res['fid']);
                this.imgbase64src = res['image'];
                this.isThereImge = true;
                this.showDeleteBtn = true;
               setTimeout(() => {
                 this.imgLoading = false;
               }, 1500);
             }else{
               //this.isThereImge = false;
             }

           })
      }

      myReader.readAsDataURL(file);
}

    autoHideAlertMsgAndGoNext(time) {
      setTimeout(() => {
        this.alertmsg = false;
        this.router.navigate(['/businessprofile/step-two'])
      }, time)
    }

previewData() {
  this.showDialog = true;
//  var step1Obj = this.getObjectForSave();
//  localStorage.setItem('step1Profile', step1Obj);
 if(this.titalText !== '' || this.descriptionText !== '' || this.imgbase64src !== '' ) {
  let profForm = {"tital": this.titalText, "description":this.descriptionText, "image":this.imgbase64src, "companyname":this.myCompeyName, "showProfile":true}
  this.hwaOverlayService.profileData(profForm, 0);
  this.removenextSteps();
}else{
   this.hwaOverlayService.profileData(null, 0);
   this.removenextSteps();
}
  //let objArray = this.hwaOverlayService.allStapesData;
//  objArray[1] = profForm;
//  this.hwaOverlayService.allStapesData = objArray;

}
removenextSteps() {
    this.hwaOverlayService.profileData(null, 1);
    this.hwaOverlayService.profileData(null, 2);
    this.hwaOverlayService.profileData(null, 3);
}
getObjectForSave():any {
  let prof1_img_nid =  localStorage.getItem('prof1_img_nid');
  var stepData = {
          "bptitle1":this.buzProfile.value.tital,
          "bpbody1": this.buzProfile.value.description,
          "image1_fid":prof1_img_nid,
          }
  return stepData;
}

deleteThis(field){

    if(localStorage.getItem('profileNid')){

      this.showDeleteBtn = true;
    let deletImgObj ={
    "bp_nid":localStorage.getItem('profileNid'),
    "image1_fid":"yes",
    "image2_fid":"",
    "image3_fid":"",
    "image4_fid":""
    }


this.HwaServices.deleteImg(deletImgObj).subscribe(
    res => {
    console.log(res);
              this.showDeleteBtn = false;
              this.imgbase64src = '';
              this.isThereImge = false;
              this.imgLoading=false;
              localStorage.setItem('prof1_img_nid', '');
              localStorage.removeItem('prof1_img_nid');
    })
  }else{
    this.showDeleteBtn = false;
  }
  this.imgbase64src = "";
  this.isThereImge = false;
  this.imgLoading=false;
localStorage.setItem('prof1_img_nid', '');
localStorage.removeItem('prof1_img_nid');
}
//--------------------------------------------------------------------
closeWin() {
this.tooltipWin = false;
}

openWin(){
  this.tooltipWin = true;
}



}
