import { Component, OnInit, AfterViewInit, Input, Output, trigger, transition, style, animate, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { window } from 'rxjs/operator/window';
import {UserService} from "../../../../../services/user.service";
import {HwaCommonService} from "../../../../../services/hwa-common.service";
import {OverlayDataService} from "../../../../../services/overlay-data.service";


@Component({
  selector: 'app-final-overlay',
  templateUrl: './final-overlay.component.html',
  styleUrls: ['./final-overlay.component.css'],
  animations: [
    trigger('sad', [
      transition('void => *', [
        style({ transform: 'scale3d(.3, .3, .3)' }),
        animate(100)
      ]),
      transition('* => void', [
        animate(100, style({ transform: 'scale3d(.0, .0, .0)' }))
      ])
    ])
  ]
})

export class FinalOverlayComponent implements OnInit {
  @Input() position: any;
  @Input() posType: any;
  @Input() describePos: any;
  @Input() describeSkill: any;
  @Input() helpTextD: any[];
  @Input() City: any[];
  @Input() address1: any;
  @Input() address2: any;
  @Input() address3: any;
  @Input() State: any;
  @Input() closable = true;
  @Input() visible: boolean;
  @Output() visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() SaveBusinessProfile: EventEmitter<any> = new EventEmitter<any>();
  @Output() onSubmit: EventEmitter<any> = new EventEmitter<any>();
  @Output() onSubmitKo: EventEmitter<any> = new EventEmitter<any>();
  @Output() onSubmitSkill: EventEmitter<any> = new EventEmitter<any>();
  @Input() showDialog: boolean;
  public companyname: string = '';
  public cityName: string = '';
  public stateName: string = '';
  private user: any;
  private level2: boolean;
  @Input() shoBasicHwa = false;
  @Input() showKo = false;
  @Input() showskill = false;
  @Input() checkFlag: string;
  private bottomDynamicButton: string;
  private defaultQ = [];
  private customQ = [];
  private overlayTitleLevel2: string;
  private overlayLevel2Htmldata: any;
  private locationLenght: any;
  // skill section
  @Input() onlyforLevel2: boolean;
  private profAllStapsData = new Array<any>();
  private skillQusetion = [];
  private noOfPosition: number;
  private showProfile: boolean;
  private addressLenght: number;
  private proFileObj: any;
  private image1Url: any;
  private image2Url: any;
  private image3Url: any;
  private image4Url: any;

  private titalText1: any;
  private titalText2: any;
  private titalText3: any;
  private titalText4: any;
  private bodyText1: any;
  private bodyText2: any;
  private bodyText3: any;
  private bodyText4: any;
  constructor(
    private router: Router,
    private userService: UserService,
    private hwaServices: HwaCommonService,
    private hwaOverlayService: OverlayDataService) { }

  ngOnInit() {

    let user = this.userService.isLogedin();
    let hwaid = localStorage.getItem('storeHwaNid');
    this.companyname = user.details.field_name_of_your_business[0].value;
    if (hwaid) {
      this.showAllDataOfHwa(hwaid);
    } else {
      alert('required HWA ID');
    }

    this.hwaOverlayService.getAllHWAData().subscribe((hwaid: any) => {
      this.showAllDataOfHwa(hwaid);
    });
  }
  public showAllDataOfHwa(hwaid) {
    let hwaObj = {
      'hwa_nid': hwaid
    };

    this.hwaServices.getAllDataOfHwa(hwaObj).subscribe(
      res => {
        if (res['Hwa']) {
          this.shoBasicHwa = true;
          this.City = [];
          this.addressLenght = res['Hwa'].field_which_location_s_are_you_h.length;
          this.locationLenght = res['Hwa'].field_which_location_s_are_you_h.length;
          this.position = res['Hwa'].title[0].value;
          this.posType = res['Hwa'].field_will_they_be_full_time_par[0].value;
          this.noOfPosition = Number(res['Hwa'].field_how_many_people_do_you_nee[0].value);
          if (this.locationLenght > 1) {
            this.City = [];
            this.City = res['Hwa'].field_which_location_s_are_you_h;
            this.address1 = '';
            this.address2 = '';
            this.address3 = '';
          }
          this.describePos = res['Hwa'].field_how_would_you_describe_thi[0].value;
          this.describeSkill = res['Hwa'].field_describe_the_skills_and_ex[0].value;

          if (res['Hwa'].field_which_location_s_are_you_h.length === 1) {
            this.City = [];
            if (res['Hwa'].field_which_location_s_are_you_h[0].field_city[0].value) {
              this.address1 = res['Hwa'].field_which_location_s_are_you_h[0].field_city[0].value + ',';
            } else {
              this.address1 = res['Hwa'].field_which_location_s_are_you_h[0].field_city[0].value;
            }
            if (res['Hwa'].field_which_location_s_are_you_h[0].field_city[0].value) {
              this.address2 = res['Hwa'].field_which_location_s_are_you_h[0].field_state[0].value + ',';
            } else {
              this.address2 = res['Hwa'].field_which_location_s_are_you_h[0].field_state[0].value;
            }
            if (res['Hwa'].field_which_location_s_are_you_h[0].title[0].value) {
              this.address3 = res['Hwa'].field_which_location_s_are_you_h[0].title[0].value + ',';
            } else {
              this.address3 = res['Hwa'].field_which_location_s_are_you_h[0].title[0].value;
            }
          } else {
            this.address1 = '';
            this.address2 = '';
            this.address3 = '';
          }
        }

        if (res['kq']) {
          if (res['kq'].length === 0) {
            this.showKo = false;
          } else {
            this.showKo = true;
          }

          this.defaultQ = [];
          this.customQ = [];
          //  this.helpTextD=koData.bottomText;

          for (let i = 0; i < res['kq'].length; i++) {
            if (res['kq'][i].field_question_type === 'Default') {
              if (res['kq'][i].field_ko === '1') {
                this.defaultQ.push(res['kq'][i].title);
              }
            } else {
              if (res['kq'][i].field_ko === '1') {
                this.customQ.push(res['kq'][i].title);
              }
            }
          }

        }

        if (res['skill_expirence']) {
          if (res['skill_expirence'].length === 0) {
            this.showskill = false;
          } else {
            this.showskill = true;
          }

          this.skillQusetion = [];
          this.skillQusetion = res['skill_expirence'];

        }
        if (res['business_profile']) {

          this.showProfile = true;
          this.proFileObj = res['business_profile'];

          if (this.proFileObj.field_image1) {
            this.image1Url = this.proFileObj.field_image1[0].url;
          }else {
            this.image1Url = '';
          }
          if (this.proFileObj.field_image2) {
            this.image2Url = this.proFileObj.field_image2[0].url;
          }else {
            this.image2Url = '';
          }
          if (this.proFileObj.field_image3) {
            this.image3Url = this.proFileObj.field_image3[0].url;
          }else {
            this.image3Url = '';
          }
          if (this.proFileObj.field_image4) {
            this.image4Url = this.proFileObj.field_image4[0].url;
          } else {
            this.image4Url = '';
          }
// ----------------------------------------------------------------
          if (this.proFileObj.field_title1) {
            this.titalText1 = this.proFileObj.field_title1[0].value;
          } else {
            this.titalText1 = 'Write about your business.';
          }
          if (this.proFileObj.field_title2) {
            this.titalText2 = this.proFileObj.field_title2[0].value;
          } else {
            this.titalText2 = 'What our customers like about us';
          }
          if (this.proFileObj.field_title3) {
            this.titalText3 = this.proFileObj.field_title3[0].value;
          } else {
            this.titalText3 = 'Sell them on the job';
          }
          if (this.proFileObj.field_title4) {
            this.titalText4 = this.proFileObj.field_title4[0].value;
          } else {
            this.titalText4 = 'Describe the Job\'s Benefits';
          }
// ---------------------------------------------------------------------------
          if (this.proFileObj.body) {
            this.bodyText1 = this.proFileObj.body[0].value;
          }else {
            this.bodyText1 = 'No description has been added';
          }
          if (this.proFileObj.field_body2) {
            this.bodyText2 = this.proFileObj.field_body2[0].value;
          } else {
            this.bodyText2 = 'No description has been added';
          }
          if (this.proFileObj.field_body3) {
            this.bodyText3 = this.proFileObj.field_body3[0].value;
          }else {
            this.bodyText3 = 'No description has been added';
          }
          if (this.proFileObj.field_body4) {
            this.bodyText4 = this.proFileObj.field_body4[0].value;
          }else {
            this.bodyText4 = 'No description has been added';
          }
        }
      });
  }

  postHwa() {
    this.router.navigate(['/postmyad']);
  }
  close() {
    this.visible = false;
    this.visibleChange.emit(this.visible);
  }
}
