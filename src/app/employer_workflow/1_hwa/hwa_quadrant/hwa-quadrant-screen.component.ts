import { Component, OnInit } from '@angular/core';
import {forEach} from '@angular/router/src/utils/collection';
import {Router} from '@angular/router';
import {AlertService} from '../../../services/alert.service';
import {UserService} from '../../../services/user.service';
import {HwaCommonService} from '../../../services/hwa-common.service';

@Component({
  selector: 'app-hwa-quadrant-screen',
  templateUrl: './hwa-quadrant-screen.component.html',
  styleUrls: ['./hwa-quadrant-screen.component.css']
})
export class HwaQuadrantScreenComponent implements OnInit {
  private hwa_quad: any = 'hwa_quad';
  private hwaCounter: number = 0;
  //  private disableButton:boolean;
  private disableMode:any = '';
  private editDisableMode = '';
  private expireDisableMode = '';
  private displayAfterLoad: boolean;
  private editHwaCounter: number = 0;
  private expireHwaCounter: number = 0;

  constructor( private alertService: AlertService,
               private router: Router,
               private userService: UserService,
               private HwaServices: HwaCommonService) { }

  ngOnInit() {
    let user = this.userService.isLogedin();
    this.getCountOfHWA(user.uid);
  }

  private getCountOfHWA(uid) {
    let obj = {
      'uid': uid,
      'status_published': 'Published',
      'status_draft': 'Draft',
      'status_expired': 'Expired'
    };
    this.HwaServices.loadCountOfHwa(obj).subscribe(
      res => {
        if ((res['Hwa_counter_published'] === 0) && (res['Hwa_counter_Draft'] === 0) && (res['hwa_count_expired'] === 0)) {
          this.hwaCounter = 0;
          this.disableMode = 'frs-card-wide2 mdl-card mdl-shadow--2dp disableMode';
          this.expireHwaCounter = 0;
          this.expireDisableMode = 'frs-card-wide2 mdl-card mdl-shadow--2dp disableMode';
          this.editHwaCounter = 0;
          this.editDisableMode = 'frs-card-wide2 mdl-card mdl-shadow--2dp disableMode';
        }else if ((res['Hwa_counter_published'] === 0) && (res['Hwa_counter_Draft'] > 0)) {
          this.hwaCounter = Number(res['Hwa_counter_Draft']);
          this.disableMode = 'frs-card-wide2 mdl-card mdl-shadow--2dp';
          this.editHwaCounter = Number(res['Hwa_counter_Draft']);
          this.editDisableMode = 'frs-card-wide2 mdl-card mdl-shadow--2dp';

          this.expireHwaCounter = 0;
          this.expireDisableMode = 'frs-card-wide2 mdl-card mdl-shadow--2dp disableMode';

        }else if(res['Hwa_counter_published'] > 0) {
          this.hwaCounter = Number(res['Hwa_counter_published']);
          this.disableMode = 'frs-card-wide2 mdl-card mdl-shadow--2dp';
          this.editHwaCounter = Number(res['Hwa_counter_published']);
          this.editDisableMode = 'frs-card-wide2 mdl-card mdl-shadow--2dp';
          this.expireHwaCounter = Number(res['Hwa_counter_published']);
          this.expireDisableMode = 'frs-card-wide2 mdl-card mdl-shadow--2dp';
        } else if ((res['Hwa_counter_published'] === 0) && (res['Hwa_counter_Draft'] === 0) && (res['hwa_count_expired'] > 0)) {
          this.hwaCounter = Number(res['hwa_count_expired']);
          this.disableMode = 'frs-card-wide2 mdl-card mdl-shadow--2dp';

          this.editHwaCounter = 0;
          this.editDisableMode = 'frs-card-wide2 mdl-card mdl-shadow--2dp disableMode';
          this.expireHwaCounter = 0;
          this.expireDisableMode = 'frs-card-wide2 mdl-card mdl-shadow--2dp disableMode';
        }
        this.displayAfterLoad = true;
      });
  }
  private gotToCreateHwa() {
    localStorage.removeItem('storeHwaFormData');
    localStorage.removeItem('storeHwaNid');
    //  clear profile data here
    localStorage.removeItem('profileNid');
    localStorage.removeItem('prof1_img_nid');
    localStorage.removeItem('prof2_img_nid');
    localStorage.removeItem('prof3_img_nid');
    localStorage.removeItem('prof4_img_nid');

    this.router.navigate(['/createHWA']);
  }

  private gotToCopyHwa() {
    if (this.hwaCounter) {
      this.router.navigate(['/listhwa-copy']);
    }

  }

  private gotToEditHwa() {
    if (this.editHwaCounter) {
      this.router.navigate(['/listhwa-edit']);
    }
  }

  private gotToExtendHwa() {
    if (this.expireHwaCounter) {
      this.router.navigate(['/listhwa-extend']);
    }
  }
}
