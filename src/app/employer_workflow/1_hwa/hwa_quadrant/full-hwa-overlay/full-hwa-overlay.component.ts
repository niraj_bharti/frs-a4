import { Component, OnInit, OnChanges, Input, SimpleChanges, Output, trigger, transition, style, animate, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import {UserService} from '../../../../services/user.service';
import {HwaCommonService} from '../../../../services/hwa-common.service';
import {OverlayDataService} from '../../../../services/overlay-data.service';

@Component({
  selector: 'app-full-hwa-overlay',
  templateUrl: './full-hwa-overlay.component.html',
  styleUrls: ['./full-hwa-overlay.component.css'],
  animations: [
    trigger('sad', [
      transition('void => *', [
        style({ transform: 'scale3d(.3, .3, .3)' }),
        animate(100)
      ]),
      transition('* => void', [
        animate(100, style({ transform: 'scale3d(.0, .0, .0)' }))
      ])
    ])
  ]
})
export class FullHwaOverlayComponent implements OnInit, OnChanges {
  @Input() alldata: any;
  @Input() position: any;
  @Input() posType: any;
  @Input() describePos: any;
  @Input() describeSkill: any;
  @Input() helpTextD: any[];
  @Input() City: any[];
  @Input() address1: any;
  @Input() address2: any;
  @Input() address3: any;
  @Input() State: any;
  @Input() closable = true;
  @Input() visible: boolean;
  @Output() visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() SaveBusinessProfile: EventEmitter<any> = new EventEmitter<any>();
  @Output() onSubmitAll: EventEmitter<any> = new EventEmitter<any>();

  @Input() showDialog: boolean;
  public companyname = '';
  public cityName = '';
  public stateName = '';
  private user: any;
  private level2 = false;
  @Input() shoBasicHwa = false;
  @Input() showKo = false;
  @Input() showskill = false;
  @Input() checkFlag: string;
  private bottomDynamicButton: string;
  private defaultQ = [];
  private customQ = [];
  private overlayTitleLevel2: string;
  private overlayLevel2Htmldata: any;
  private locationLenght: any;
  //skill section
  @Input() onlyforLevel2: boolean;
  private profAllStapsData = new Array<any>();
  private skillQusetion = [];
  private noOfPosition: number;
  private showProfile: boolean;
  private addressLenght: number;
  private proFileObj: any;
  private image1Url: any;
  private image2Url: any;
  private image3Url: any;
  private image4Url: any;

  private titalText1: any;
  private titalText2: any;
  private titalText3: any;
  private titalText4: any;

   private bodyText1: any;
   private bodyText2: any;
   private bodyText3: any;
   private bodyText4: any;


  constructor(
    private router: Router,
    private userService: UserService,
    private hwaServices: HwaCommonService,
    private hwaOverlayService: OverlayDataService) { }

  ngOnInit() {
console.log(this.alldata);
    let user = this.userService.isLogedin();
    let hwaid = localStorage.getItem('storeHwaNid');
    this.companyname = user.details.field_name_of_your_business[0].value;

    if (hwaid) {
     // this.showAllDataOfHwa(hwaid);
    } else {
     // alert('required HWA ID');
    }


this.hwaOverlayService.getAllHWAData().subscribe(( hwaid: any) => {
 this.showAllDataOfHwa(hwaid);
 });
  }

  ngOnChanges(changes: SimpleChanges) {
if (changes['alldata']) {
    if (changes['alldata'].currentValue) {
      this.showAllDataOfHwa(changes['alldata'].currentValue);
    }
}
  }
  public showAllDataOfHwa(res) {
       if (res['Hwa']) {
          this.shoBasicHwa = true;
           this.City = [];
          //  if(this.checkFlag == 'fromHwa') {
          this.addressLenght = res['Hwa'].addressLenght;
          this.locationLenght = res['Hwa'].addressLenght;
          this.companyname = res['Hwa'].companyname;
          this.position = res['Hwa'].formData.position;
          this.posType = res['Hwa'].formData.position_type;
          this.noOfPosition = Number(res['Hwa'].noOfPosition);
          if (res['Hwa'].formData.locations.length > 1) {
            this.City = res['Hwa'].formData.locations;

            this.address1 = '';
            this.address2 = '';
            this.address3 = '';
          }
          this.describePos = res['Hwa'].formData.describePosition;
          this.describeSkill = res['Hwa'].formData.describeSkill;

          if (res['Hwa'].formData.locations.length === 1) {
             if (res['Hwa'].address1) {
                this.address1 = res['Hwa'].address1 + ',';
              } else {
                this.address3 = res['Hwa'].address3;
              }
              if (res['Hwa'].address2) {
                this.address2 = res['Hwa'].address2 + ',';
              } else {
                this.address2 = res['Hwa'].address2;
              }
              if (res['Hwa'].address3) {
                this.address3 = res['Hwa'].address3 + ',';
              } else {
                this.address3 = res['Hwa'].address3;
              }
          } else {
            this.address1 = '';
            this.address2 = '';
            this.address3 = '';
          }

        }

        if (res['kq']) {
        this.showKo = true;
        this.defaultQ = [];
        this.customQ = [];
        this.showKo = res['kq'].showKo;
        this.helpTextD = res['kq'].bottomText;
        this.defaultQ = res['kq'].defaultQ;
        this.customQ = res['kq'].customQ;
        }

        if (res['skill_expirence']) {

          this.showskill = true;
          this.skillQusetion = [];
          for (let i = 0; i < res['skill_expirence'].skillQusetion.length; i++) {
if (this.checkstatus(res['skill_expirence'].skillQusetion[i].expertiseLevel, res['skill_expirence'].skillQusetion[i].experenceLevel)) {
            this.skillQusetion.push(res['skill_expirence'].skillQusetion[i]);
            }
          }
        }
        if (res['business_profile']) {

          this.showProfile = true;
          this.proFileObj = res['business_profile'];

          if (this.proFileObj.image1Url) {
             this.image1Url = this.proFileObj.image1Url;
          }else {
             this.image1Url = '';
          }
          if (this.proFileObj.image2Url) {
             this.image2Url = this.proFileObj.image2Url;
          }else {
             this.image2Url = '';
          }
          if (this.proFileObj.image3Url) {
             this.image3Url = this.proFileObj.image3Url;
          }else {
             this.image3Url = '';
          }
          if (this.proFileObj.image4Url) {
             this.image4Url = this.proFileObj.image4Url;
          }else {
             this.image4Url = '';
          }
// ----------------------------------------------------------------
          if (this.proFileObj.titalText1) {
             this.titalText1 = this.proFileObj.titalText1;
          }else {
             this.titalText1 = '';
          }
           if (this.proFileObj.titalText2) {
             this.titalText2 = this.proFileObj.titalText2;
          }else {
             this.titalText2 = '';
          }
           if (this.proFileObj.titalText3) {
             this.titalText3 = this.proFileObj.titalText3;
          }else {
             this.titalText3 = '';
          }
           if (this.proFileObj.titalText4) {
             this.titalText4 = this.proFileObj.titalText4;
          }else {
             this.titalText4 = '';
          }
// ---------------------------------------------------------------------------
          if (this.proFileObj.bodyText1) {
             this.bodyText1 = this.proFileObj.bodyText1;
          }else {
             this.bodyText1 = '';
          }
          if (this.proFileObj.bodyText2) {
             this.bodyText2 = this.proFileObj.bodyText2;
          }else {
             this.bodyText2 = '';
          }
          if (this.proFileObj.bodyText3) {
             this.bodyText3 = this.proFileObj.bodyText3;
          }else {
             this.bodyText3 = '';
          }
          if (this.proFileObj.bodyText4) {
             this.bodyText4 = this.proFileObj.bodyText4;
          }else {
             this.bodyText4 = '';
          }
        }
  }

  postHwa() {
     this.onSubmitAll.emit('postMyAd');
  }
  close() {
    this.visible = false;
    this.visibleChange.emit(this.visible);
  }

  checkstatus(expertiseLevel, experenceLevel): boolean {
    let flag = true;
    if ((expertiseLevel === 'N/A') && (experenceLevel === 'N/A')) {
      flag = false;
    }
    return flag;
  }
}
