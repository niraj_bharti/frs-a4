import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import {AlertService} from '../../../../services/alert.service';
import {UserService} from '../../../../services/user.service';
import {HwaCommonService} from '../../../../services/hwa-common.service';


@Component({
  selector: 'app-viewad',
  templateUrl: './viewad.component.html',
  styleUrls: ['./viewad.component.css']
})
export class ViewadComponent implements OnInit {
  private viewads1: any = 'viewads1';
  private subscription: Subscription;
  private position: any;
  private posType: any;
  private describePos: any;
  private describeSkill: any;
  private helpTextD: any[];
  private City: any[];
  private address1: any;
  private address2: any;
  private address3: any;
  private State: any;
  private closable = true;
  private visible: boolean;

  private showDialog: boolean;
  public companyname: string = '';
  public cityName: string = '';
  public stateName: string = '';
  private user: any;
  private level2: boolean = false;
  private shoBasicHwa = false;
  private showKo = false;
  private showskill = false;
  private checkFlag: string;
  private bottomDynamicButton: string;
  private defaultQ = [];
  private customQ = [];
  private overlayTitleLevel2: string;
  private overlayLevel2Htmldata: any;
  private locationLenght: any;
  // skill section

  private profAllStapsData = new Array<any>();
  private skillQusetion = [];
  private noOfPosition: number;
  private showProfile: boolean;
  private addressLenght: number;
  private proFileObj: any;
  private image1Url: any;
  private image2Url: any;
  private image3Url: any;
  private image4Url: any;

  private titalText1: any;
  private titalText2: any;
  private titalText3: any;
  private titalText4: any;

  private bodyText1: any;
  private bodyText2: any;
  private bodyText3: any;
  private bodyText4: any;
  private hideEntireBP: boolean;
  private hide4th: boolean;
  private koSentence: boolean;
  private skillSentence: boolean;
  private daysLimitOfalert = 5; 
  private viewHwaId: any;
  private closeDate: any;
  private btnlbl: string;
  private rdays:number;
  constructor( private alertService: AlertService,
               private router: Router,
               private activatedRoute: ActivatedRoute,
               private userService: UserService,
               private hwaServices: HwaCommonService) { }

  ngOnInit() {
    this.subscription = this.activatedRoute.params.subscribe(
      (param: any) => {
        if (param['hwaId'] ) {
          this.viewHwaId = param['hwaId'];
          this.closeDate =  param['expDate'];
          this.rdays = this.getDiffrencedays(new Date() , this.closeDate);
          this.btnlbl = param['btnlbl'];
          this.loadHwaData(param['hwaId']);
        }else {
          alert('You must have create HWA Ad' );
        }
      });

  }

getDiffrencedays(startDate, endDate) {
    let oneDay = 24 * 60 * 60 * 1000; //  hours*minutes*seconds*milliseconds.
    let firstDate = new Date(startDate);
    let secondDate = new Date(endDate);
    let currDate = new Date();
    if (firstDate.getTime() > currDate.getTime()) {
      currDate = firstDate;
    } else {
      currDate = new Date();
    }
    let diffDays = Math.round(Math.abs((currDate.getTime() - secondDate.getTime())/(oneDay)));
    return diffDays;
  }


  public loadHwaData(hwaid) {
    let hwaObj = {
      'hwa_nid': hwaid
    };

    this.hwaServices.getAllDataOfHwa(hwaObj).subscribe(
      res => {
        if (res['Hwa']) {
          let a = res.business_profile.field_image1 === undefined;
          let b = res.business_profile.field_image2 === undefined;
          let c = res.business_profile.field_image3 === undefined;
          let d = res.business_profile.field_image4 === undefined;
          if (a && b && c && d) {
            this.hideEntireBP = true;
          } else {
            this.hideEntireBP = false;
          }
          if (res.business_profile.field_image4 === undefined) {
            this.hide4th = true;
          } else {
            this.hide4th = false;
          }
          this.shoBasicHwa = true;
          this.City = [];
          //   if(this.checkFlag == 'fromHwa') {
          this.addressLenght = res['Hwa'].field_which_location_s_are_you_h.length;
          this.locationLenght = res['Hwa'].field_which_location_s_are_you_h.length;
          this.companyname = res['business'];
          this.position = res['Hwa'].title[0].value;
          this.posType = res['Hwa'].field_will_they_be_full_time_par[0].value;
          this.noOfPosition = Number(res['Hwa'].field_how_many_people_do_you_nee[0].value);
          if (this.locationLenght > 1) {
            this.City = [];
            this.City = res['Hwa'].field_which_location_s_are_you_h;
            console.log(this.City);
            this.address1 = '';
            this.address2 = '';
            this.address3 = '';
          }
          this.describePos = res['Hwa'].field_how_would_you_describe_thi[0].value;
          this.describeSkill = res['Hwa'].field_describe_the_skills_and_ex[0].value;

          if (res['Hwa'].field_which_location_s_are_you_h.length === 1) {
            this.City = [];
            if (res['Hwa'].field_which_location_s_are_you_h[0].field_city[0].value) {
              this.address1 = res['Hwa'].field_which_location_s_are_you_h[0].field_city[0].value + ',';
            } else {
              this.address1 = res['Hwa'].field_which_location_s_are_you_h[0].field_city[0].value;
            }
            if (res['Hwa'].field_which_location_s_are_you_h[0].field_city[0].value) {
              this.address2 = res['Hwa'].field_which_location_s_are_you_h[0].field_state[0].value + ',';
            } else {
              this.address2 = res['Hwa'].field_which_location_s_are_you_h[0].field_state[0].value;
            }
            if (res['Hwa'].field_which_location_s_are_you_h[0].title[0].value) {
              this.address3 = res['Hwa'].field_which_location_s_are_you_h[0].title[0].value + ',';
            } else {
              this.address3 = res['Hwa'].field_which_location_s_are_you_h[0].title[0].value;

            }
          } else {
            this.address1 = '';
            this.address2 = '';
            this.address3 = '';
          }

        }

        if (res['kq']) {
          this.showKo = true;
          this.defaultQ = [];
          this.customQ = [];

          if (res['kq'].length === 0) {
            this.koSentence = true;
          }   else {
            this.koSentence = false;
          }
          //  this.helpTextD=koData.bottomText;

          for (let i = 0; i < res['kq'].length; i++) {
            if (res['kq'][i].field_question_type === 'Default') {
              if (res['kq'][i].field_ko === '1') {
                this.defaultQ.push(res['kq'][i].title);
              }
            } else {
              if (res['kq'][i].field_ko === '1') {
                this.customQ.push(res['kq'][i].title);
              }
            }
          }

        }

        if (res['skill_expirence']) {

          this.showskill = true;
          this.skillQusetion = [];
          this.skillQusetion = res['skill_expirence'];
          if (res['skill_expirence'].length === 0) {
            this.skillSentence = true;
          }   else {
            this.skillSentence = false;
          }

        }
        if (res['business_profile']) {
          this.showProfile = true;
          this.proFileObj = res['business_profile'];

          if (this.proFileObj.field_image1) {
            this.image1Url = this.proFileObj.field_image1[0].url;
          }else {
            this.image1Url = '';
          }
          if (this.proFileObj.field_image2) {
            this.image2Url = this.proFileObj.field_image2[0].url;
          } else {
            this.image2Url = '';
          }
          if (this.proFileObj.field_image3) {
            this.image3Url = this.proFileObj.field_image3[0].url;
          } else {
            this.image3Url = '';
          }
          if (this.proFileObj.field_image4) {
            this.image4Url = this.proFileObj.field_image4[0].url;
          } else {
            this.image4Url = '';
          }
// ----------------------------------------------------------------
          if (this.proFileObj.field_title1) {
            this.titalText1 = this.proFileObj.field_title1[0].value;
          } else {
            this.titalText1 = '';
          }
          if (this.proFileObj.field_title2) {
            this.titalText2 = this.proFileObj.field_title2[0].value;
          } else {
            this.titalText2 = '';
          }
          if (this.proFileObj.field_title3) {
            this.titalText3 = this.proFileObj.field_title3[0].value;
          }else {
            this.titalText3 = '';
          }
          if (this.proFileObj.field_title4) {
            this.titalText4 = this.proFileObj.field_title4[0].value;
          } else {
            this.titalText4 = '';
          }
// ---------------------------------------------------------------------------
          if (this.proFileObj.body) {
            this.bodyText1 = this.proFileObj.body[0].value;
          } else {
            this.bodyText1 = '';
          }
          if (this.proFileObj.field_body2) {
            this.bodyText2 = this.proFileObj.field_body2[0].value;
          }else {
            this.bodyText2 = '';
          }
          if (this.proFileObj.field_body3) {
            this.bodyText3 = this.proFileObj.field_body3[0].value;
          } else {
            this.bodyText3 = '';
          }
          if (this.proFileObj.field_body4) {
            this.bodyText4 = this.proFileObj.field_body4[0].value;
          }else {
            this.bodyText4 = '';
          }
        }
      });
  }

  makeACopy() {
    if (this.btnlbl === 'Copy') {
      this.router.navigate(['makeAcopy', this.viewHwaId]);
    } else if (this.btnlbl === 'Edit') {
      this.router.navigate(['makeAedit', this.viewHwaId, '']);
    } else if (this.btnlbl === 'Extend') {
      localStorage.setItem('storeHwaNid', this.viewHwaId);
      this.router.navigate(['extendhwa', this.closeDate]);
    }

  }
}
