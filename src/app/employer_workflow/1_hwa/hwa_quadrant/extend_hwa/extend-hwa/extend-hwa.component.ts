import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from "@angular/forms";

import { forEach } from "@angular/router/src/utils/collection";
import { isNumeric } from "rxjs/util/isNumeric";
import { Router, ActivatedRoute } from '@angular/router';
import {IMyOptions, IMyDateModel} from 'mydatepicker';
import { Subscription } from 'rxjs';
import {AlertService} from "../../../../../services/alert.service";
import {UserService} from "../../../../../services/user.service";
import {HwaCommonService} from "../../../../../services/hwa-common.service";

 
@Component({
  selector: 'app-extend-hwa',
  templateUrl: './extend-hwa.component.html',
  styleUrls: ['./extend-hwa.component.css']
})
export class ExtendHwaComponent implements OnInit {
private post_ad1:any="post_ad1";
  private submitHwaForm:any;
  private datevalue:any;
  private endDatevalue:any;
  private additionalDays: number=7;
  private defaultDays:number = 0;
  private position;
  private selectedNofPostion;
  private posType;
  private defaultAmount:number = 0;
  private additionalAmt:number = 0;
  private totalAmt:number = 0;
  private for7DaysAmt:any = 12.50 ;
  private for14DaysAmt:any= 25;
  private isDteValid:boolean = false;
  private MS_PER_MINUTE = 60000;
  private durationInMinutes = 1;
  private startDate:any;
  private closeDate:any;
   private subscription: Subscription;
  private myDatePickerOptions: IMyOptions = {
       dateFormat: 'mm/dd/yyyy',
 };

  constructor(
    private alertService: AlertService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formbuilder: FormBuilder,
    private userService: UserService,
    private HwaServices: HwaCommonService) {
    }


  ngOnInit() {
    this.loadHwaDraftData(localStorage.getItem('storeHwaNid'));
       this.subscription = this.activatedRoute.params.subscribe(
      (param: any) => {
    		if (param['expDate']) {
         // this.viewHwaId = param['hwaId'];
          //this.closeDate =  param['expDate'];
          this.datevalue = param['expDate'];
console.log('param', this.datevalue);
          this.endDatevalue = new Date(this.datevalue);
          console.log(' this.endDatevalue',  this.endDatevalue);
          this.canculateEndDate();
          this.selectstartDate('');
         //this.loadHwaData(param['hwaId'])
    		}else{
          alert("Date is invalid Please try again." );
        }
      });
     this.startDate = this.formatDate(this.datevalue);
  }
loadHwaDraftData(hwaId) {
    let user = this.userService.isLogedin();

    this.HwaServices.loadDraftData(hwaId, user.uid).subscribe(
      res => {
console.log("hwa ", res)
        if (res) {
          this.position = res['title'][0].value;
          this.selectedNofPostion = res['field_how_many_people_do_you_nee'][0].value;
          this.posType = res['field_will_they_be_full_time_par'][0].value;
          //this.describePos = res['field_how_would_you_describe_thi'][0].value;
          //this.describeSkill = res['field_describe_the_skills_and_ex'][0].value;
           let location = res['field_which_location_s_are_you_h'];
           var locationNid = [];
    /*
            for(let i = 0; i < location.length; i++) {

            locationNid.push(location[i].nid[0].value);
           }
           */
        }

      },
      error => {
        console.log(error)
      });
  }
submitHWA() {
if(this.datevalue){

this.endDatevalue = new Date(this.datevalue);
 this.canculateEndDate();

let hwaid = localStorage.getItem('storeHwaNid');
let data = {
"hwa_nid": hwaid,
"start_date":  this.formatDate(this.datevalue), //"2000-01-20 12:00:00",
"end_date": this.endDatevalue,
"extend_date_days": this.additionalDays,
"total_amt":this.totalAmt,
"action":"extend"
}

console.log(data);
if(hwaid) {
 localStorage.setItem('pamentData', JSON.stringify(data));
  this.router.navigate(['/payment']);
}else{
  this.alertService.success("Your have to crate HWA", true);
  this.router.navigate(['/hwa_workflow']);
}

}else {
  alert("You need to select valid date");
}
}

selectstartDate(event: any){
  //console.log(Date.now())
   if(event.jsdate){
  var d1 = new Date();
  var d2 = new Date(event.jsdate);
  if((d1.getDate() == d2.getDate()) && (d1.getMonth() == d2.getMonth() ) && (d1.getFullYear() == d2.getFullYear()) ) {
    this.datevalue =  event.jsdate;
    this.isDteValid = true;
   }else{
     if(d1.getTime() < d2.getTime()) {
       this.datevalue = event.jsdate;
       this.isDteValid = true;
     }else{
       //alert("You need to select valid date");
       this.isDteValid = false;
     }

   }
  }

if(this.datevalue) {
 this.endDatevalue = new Date(this.datevalue);
 this.canculateEndDate();
 this.defaultAmount = 25.00;
 if(this.additionalDays == 0) {
  this.additionalAmt = 0;
  this.isDteValid = false;
 }else if(this.additionalDays == 7) {
  this.additionalAmt = this.defaultAmount / 2;
  this.isDteValid = true;
 }else if(this.additionalDays == 14) {
  this.additionalAmt = this.defaultAmount;
  this.isDteValid = true;
 }
 this.totalAmt =  Number(this.additionalAmt);
  }else{
    alert("Please Select Date.");
  }
}

canculateEndDate(){

 if(this.additionalDays != 0){
  var dafutDay = Number(this.defaultDays) + Number(this.additionalDays);
  this.endDatevalue.setDate(this.endDatevalue.getDate() + (dafutDay));
  this.endDatevalue = new Date(this.endDatevalue.getTime() - this.durationInMinutes * this.MS_PER_MINUTE);
  this.endDatevalue = this.formatDate(this.endDatevalue);
 }else{

  this.endDatevalue.setDate(this.endDatevalue.getDate() + (this.defaultDays));
  this.endDatevalue = new Date(this.endDatevalue.getTime() - this.durationInMinutes * this.MS_PER_MINUTE);
  this.endDatevalue = this.formatDate(this.endDatevalue);
 }

}
 formatDate(date) {


    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        h = '' + d.getHours(),
        m = '' + d.getMinutes(),
        s = '' + d.getSeconds();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    if (h.length < 2) h = '0' + h;
    if (m.length < 2) m = '0' + m;
    if (s.length < 2) s = '0' + s;
    if(s == '00'){s = '01'}
    let dat = [year, month, day].join('-');
    var dateFromat = dat +" "+[h, m, s].join(':');

  return dateFromat;
}
}
