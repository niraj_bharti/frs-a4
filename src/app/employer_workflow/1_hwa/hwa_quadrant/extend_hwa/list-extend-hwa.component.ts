import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AlertService} from '../../../../services/alert.service';
import {UserService} from '../../../../services/user.service';
import {HwaCommonService} from '../../../../services/hwa-common.service';


@Component({
  selector: 'app-extend-hwa',
  templateUrl: './list-extend-hwa.component.html',
  styleUrls: ['./list-extend-hwa.component.css']
})
export class ListExtendHwaComponent implements OnInit {
  private extendB:any = 'extendB';
  private extendHwaListing = [];
  private colorClass:string = 'errorLine';
  private stopConfirm:boolean = false;
  private daysLimitOfalert:number = 5;
  private selectedHwaId:any;
  private alertmsg:boolean = false;
  private displayLoding:boolean = false;
  private resumeLoding:boolean =  false;
  private resumeConfirm:boolean = false;
  private resumeHwaId:string = '';
  constructor(private router:Router,
              private alertService: AlertService,
              private userService: UserService,
              private HwaServices: HwaCommonService) { }

  ngOnInit() {
    this.getExtendList();
  }

  getExtendList(){
    let user = this.userService.isLogedin();
    this.HwaServices.getAllHWAList(user.uid).subscribe(
      res => {
        console.log('xxxxx', res);
        // this.displayListing = true;

        for (let i = 0; i < res['hwa_published'].length; i++) {
          // res['hwa_published'][i].field_extend_from_date
          //  res['hwa_published'][i].field_extend_to_date
res['hwa_published'][i].rdays = this.getDiffrencedays(res['hwa_published'][i].field_extend_from_date, res['hwa_published'][i].field_extend_to_date)
        }
        this.extendHwaListing = res['hwa_published'];

      });
  }

  viewHWA(hwaid, expDate){
    this.router.navigate(['viewad', hwaid, expDate, 'Extend'])
  }
  getDiffrencedays(startDate, endDate) {
    let oneDay = 24 * 60 * 60 * 1000; //  hours*minutes*seconds*milliseconds.
    let firstDate = new Date(startDate);
    let secondDate = new Date(endDate);
    let currDate = new Date();
    if (firstDate.getTime() > currDate.getTime()) {
      currDate = firstDate;
    } else {
      currDate = new Date();
    }
    let diffDays = Math.round(Math.abs((currDate.getTime() - secondDate.getTime())/(oneDay)));
    return diffDays;
  }

  extendHwa(hwaid, expDate){
    localStorage.setItem('storeHwaNid', hwaid);
    // let newED = expDate.split('-')[2]+'-'+expDate.split('-')[0]+'-'+expDate.split('-')[1];

    let newEDate = new Date(Date.parse(expDate));
    this.router.navigate(['extendhwa', newEDate.toDateString()])
  }

  stopHwa(hwaId) {
    console.log(hwaId);
    if (hwaId) {
      this.selectedHwaId = hwaId;
      this.stopConfirm = true;
    } else {
      this.selectedHwaId = '';
      alert('Hwa Id is Blank, You must have HWA Id');
    }

  }

  toTimestamp(year, month, day, hour, minute, second) {
    let datum = new Date(Date.UTC(year, month, day, hour, minute, second));
    return datum.getTime() / 1000.0;
  }

  gotoExtent() {
    if (this.selectedHwaId) {
      let d = new Date();
      let time  = d.getTime() / 1000;
      console.log(time);
      let Obj = {
        'hwa_nid': this.selectedHwaId,
        'hwa_stop_resume': 'Stop',
        'hwa_stop_date': time
      };
      console.log(Obj);
      this.alertmsg = true;
      this.displayLoding = true;
      this.HwaServices.stopHwa(Obj).subscribe(
        res => {
          console.log(res);
          this.stopConfirm = false;

          this.getExtendList();
          this.displayLoding = false;
          this.alertService.success('You have stoped HWA Successfully', true);
          this.autoHideAlertMsg(3000);
        },
        error => {
        }
      );
    } else {
      alert('HWA Id is missing, Please Try again');
    }
  }
  closeExtent() {
    this.displayLoding = false;
    this.stopConfirm = false;

  }


  autoHideAlertMsg(time) {
    setTimeout(() => {
      this.alertmsg = false;

    }, time);
  }

  sorting: any = {
    column: 'field_hwa_post', // to match the letiable of one of the columns
    descending: false
  };


  columns: any[] = [
    {
      display: 'Column 1', // The text to display
      letiable: 'field_hwa_post', // The name of the key that's apart of the data array
      filter: 'text' // The type data type of the column (number, text, date, etc.)
    },
    {
      display: 'Column 3', // The text to display
      letiable: 'field_extend_from_date', // The name of the key that's apart of the data array
      filter: 'dateTime' // The type data type of the column (number, text, date, etc.)
    }
  ];

  selectedClass(columnName): any {
    return columnName === this.sorting.column ? 'sort-' + this.sorting.descending : false;
  }

  changeSorting(columnName): void {
    let sort = this.sorting;
    if (sort.column === columnName) {
      sort.descending = !sort.descending;
    } else {
      sort.column = columnName;
      sort.descending = false;
    }
  }
  convertSorting(): string{
    return this.sorting.descending ? '-' + this.sorting.column : this.sorting.column;
  }
  // ----------------------------------------------------------------------------------
  sorting_resume: any = {
    column: 'field_hwa_post', // to match the letiable of one of the columns
    descending: false
  };
  columns_resume: any[] = [
    {
      display: 'Column 1', // The text to display
      letiable: 'field_hwa_post', // The name of the key that's apart of the data array
      filter: 'text' // The type data type of the column (number, text, date, etc.)
    },
    {
      display: 'Column 3', // The text to display
      letiable: 'field_stop_date', // The name of the key that's apart of the data array
      filter: 'dateTime' // The type data type of the column (number, text, date, etc.)
    }
  ];
  changeSorting_resume(columnName): void {
    let sort = this.sorting_resume;
    if (sort.column === columnName) {
      sort.descending = !sort.descending;
    } else {
      sort.column = columnName;
      sort.descending = false;
    }
  }
  selectedClass_resume(columnName): any {
    return columnName === this.sorting_resume.column ? 'sort-' + this.sorting_resume.descending : false;
  }
  convertSorting_resume(): string {
    return this.sorting_resume.descending ? '-' + this.sorting_resume.column : this.sorting_resume.column;
  }
  confirmResume(hwaId) {

    if (hwaId) {
      this.resumeConfirm = true;
      this.resumeHwaId = hwaId;
    }else{
      this.resumeHwaId = '';
      alert('Hwa Id is Blank, You must have HWA Id');
    }
  }
  resumeHwa() {
    if (this.resumeHwaId) {
      let Obj = {
        'hwa_nid': this.resumeHwaId,
        'hwa_stop_resume': 'Resume'
      };
      this.alertmsg = true;
      this.resumeLoding = true;
      this.HwaServices.stopHwa(Obj).subscribe(
        res => {
// console.log(res);
          this.resumeConfirm = false;
          this.resumeLoding = false;

          this.getExtendList();
          this.alertService.success('You have Resumed HWA Successfully', true);
          this.autoHideAlertMsg(3000);
        },
        error => {
        }
      );

    } else {
      alert('HWA Id is missing, Please Try again');
    }
  }
  closeResume() {
    this.resumeLoding =false;
    this.resumeConfirm = false;

  }

  myDate(datestr) {
    let sd = new Date(datestr);
    let humDate = new Date(Date.UTC(sd.getFullYear(),
      (this.stripLeadingZeroes(sd.getMonth())),
      this.stripLeadingZeroes(sd.getDate()),
      this.stripLeadingZeroes(sd.getHours()),
      this.stripLeadingZeroes(sd.getMinutes()),
      this.stripLeadingZeroes(sd.getSeconds())));
    let theDate = (humDate.getTime() / 1000.0);
    return new Date(theDate);
  }

  parseDate_bk(input) {
    let parts = input.split('-'); // 'dd-mm-yyyy'
    return new Date(parts[2], parts[0] - 1, parts[1]); //  Note: months are 0-based
  }
  parseDate(input) {
    let k;
    if (Date.parse(input)) {
      k = new Date(input);
    } else {
      k = '';
    }
    return  k;
  }
  stripLeadingZeroes(input)  {
    if((input.length > 1) && (input.substr(0,1) == '0'))
      return input.substr(1);
    else
      return input;
  }
}
