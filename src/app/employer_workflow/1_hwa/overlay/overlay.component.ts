import {Component, OnInit, Input, Output, trigger, transition, style, animate, EventEmitter} from '@angular/core';
// import {EventEmitter} from '@angular/common/src/facade/async';
import {Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {window} from 'rxjs/operator/window';
import {OverlayDataService} from '../../../services/overlay-data.service';
import {UserService} from '../../../services/user.service';
import {DataService} from '../../../services/data.service';

@Component({
  selector: 'overlay',
  templateUrl: './overlay.component.html',
  styleUrls: ['./overlay.component.css'],
  animations: [
    trigger('sad', [
      transition('void => *', [
        style({ transform: 'scale3d(.3, .3, .3)' }),
        animate(100)
      ]),
      transition('* => void', [
        animate(100, style({ transform: 'scale3d(.0, .0, .0)' }))
      ])
    ])
  ]
})
export class OverlayComponent implements OnInit{
  @Input() position: any;
  @Input() posType: any;
  @Input() describePos: any;
  @Input() describeSkill: any;
  @Input() helpTextD: any[];
  @Input() City: any[];
  @Input() address1: any;
  @Input() address2: any;
  @Input() address3: any;
  @Input() State: any;
  @Input() closable = true;
  @Input() visible: boolean;
  @Output() visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() SaveBusinessProfile: EventEmitter<any> = new EventEmitter<any>();
  @Output() onSubmit: EventEmitter<any> = new EventEmitter<any>();
  @Output() onSubmitKo: EventEmitter<any> = new EventEmitter<any>();
  @Output() onSubmitSkill: EventEmitter<any> = new EventEmitter<any>();
  @Input() showDialog: boolean;
  public companyname: string = '';
  public cityName: string = '';
  public stateName: string = '';
  private user: any;
  private level2: boolean;
  private beforePost: boolean;
  @Input() shoBasicHwa=false;
  @Input() showKo= false;
  @Input() showskill= false;
  @Input() checkFlag: string;
  @Input() checkFlagFOr: string = '';
  private bottomDynamicButton: string;
  private defaultQ = [];
  private customQ = [];
  private overlayTitleLevel2: string;
  private overlayLevel2Htmldata: any;
  private overlayTitleBeforePost: string;
  private overlayBeforePostHtmldata: any;
  private bottomDynamicButtonBeforePost: string;
  private locationLenght: any;
  // skill section
  @Input() onlyforLevel2: boolean;
  private profAllStapsData = new Array<any>();
  private skillQusetion = [];
  private noOfPosition: number;
  private showProfile: boolean;
  private addressLenght: number;
  private laleOfSteps: string = 'Save & Continue';
  constructor(
    private hwaOverlayService: OverlayDataService,
    private router: Router,
    public userService: UserService,
    private dataService: DataService,
    private domSanitizer: DomSanitizer) {

  }

  ngOnInit() {
    this.locationLenght;
    console.log(this.checkFlagFOr);
    if (this.checkFlag === 'fromHwa') {

      this.bottomDynamicButton = 'Add Knockout Questions';
      this.overlayTitleLevel2 = 'Knockout Questions';
      this.overlayLevel2Htmldata = `
          <p>Knockout Questions are answered by your candidates as they apply online.
          The questions can only be answered with a response of yes or no.
           We can start you off with a couple of basic ones, 
           but then think of what else you would want to know of a candidate before talking with them such as.</p>
                <ul>
                    <li>A specific license they might need to do the job.</li>
                    <li>A certain level of expertise in a specific area of knowledge. </li>
                </ul>
            `;
      this.overlayTitleBeforePost = 'Before You Post Your Ad';
      this.overlayBeforePostHtmldata = ` <p>We’d recommend you take an extra moment to add Knockout Questions 
for your job applicants to answer.</p>
            <p>These are questions that are answered with either a Yes or No by the job seeker. 
            We will automatically filter out applicants that answer with a No, 
            saving you lots of time in reviewing applications of people that will not meet your 
            minimum requirements. </p>
            <p>Click on the Add Questions button and we’ll start you off with 
            some basic ones that all of your job candidates should answer Yes to. From there you can add your own for items such as;</p>
            <p>  • A specific type of license they would need for the job.<br>
  • An additional language that you would need them to speak.</p>
            `;
   this.bottomDynamicButtonBeforePost = 'Add Questions';

    } else if (this.checkFlag === 'koData') {
      this.bottomDynamicButton = 'Add Skills Questions';
      this.overlayTitleLevel2 = 'Your Skills Or Experience Questions';
      this.overlayLevel2Htmldata = `
          <p>By using Skills Or Experience questions you can learn a lot about your 
          applicants before you even interview them. Focus in on critical areas of 
          knowledge your new employee would need from day one to be a productive member of your team.</p>
        `;
      this.overlayTitleBeforePost = 'Before You Post Your Ad';
      this.overlayBeforePostHtmldata = `
            <p>We also recommend you take an extra moment to add Skill Questions
            for your job applicants to answer. </p>
            <p>These are questions that help you determine how good of a fit your
            candidate is before you even speak with them. We will automatically score and rank
            all of your applicants, saving you precious time in needing to review
            each applicant’s credentials. 
</p>
            <p>Click on the Add Questions button to get started. You can add up to 10 questions.
             Some examples of questions you can ask include;</p>
            <p>• How many years of experience do they have with a particular required skill?<br>
  • What level of expertise do they possess of a required skill?  </p>
            `;
      this.bottomDynamicButtonBeforePost = 'Add Questions';
    } else if (this.checkFlag === 'fromSkill') {
      this.bottomDynamicButton = 'Tell Your Company\'s Story';
      this.overlayTitleLevel2 = '';
      this.overlayTitleBeforePost = 'Before You Post Your Ad';
      this.overlayBeforePostHtmldata = `
            <p>The last item we’d recommend is to take a few extra moments to build out your Business
             Profile to help attract more and higher quality candidates. </p>
            <p>The Business Profile consists of four sections where you can upload your own
             image of your business and sell job seekers on;</p>
            <p>• What your business does.<br>
  • Why customers like you.<br>
   • What your employees like about working there (culture, perks, etc.).<br>
     • Why the job seeker would want to work there. 
  </p>
  <p>Click here to see an online example of a completed Business Profile. </p>
            `;
      this.bottomDynamicButtonBeforePost = 'Build Company Profile';

    }
    this.hwaOverlayService.previewData().subscribe((shwData: any) => {

      if (this.checkFlag === 'fromHwa') {
        this.addressLenght = shwData.addressLenght;
        this.locationLenght = shwData.formData.locations.length;
        this.companyname = shwData.companyname;
        this.shoBasicHwa = shwData.shoBasicHwa;
        this.showKo = shwData.showKo;
        this.position = shwData.formData.position;
        this.posType = shwData.formData.position_type;
        this.noOfPosition = Number(shwData.noOfPosition);
        if (shwData.formData.locations.length > 1 ) {
          this.City = shwData.formData.locations;
          this.address1 = '';
          this.address2 = '';
          this.address3 = '';
        }
        this.describePos = shwData.formData.describePosition;
        this.describeSkill = shwData.formData.describeSkill;

        if (shwData.formData.locations.length === 1) {
          if (shwData.address1) {
            this.address1 = shwData.address1 + ',';
          } else {
            this.address3 = shwData.address3;
          }
          if (shwData.address2) {
            this.address2 = shwData.address2 + ',';
          } else {
            this.address2 = shwData.address2;
          }
          if (shwData.address3) {
            this.address3 = shwData.address3 + ',';
          } else {
            this.address3 = shwData.address3;
          }
        } else {
          this.address1 = '';
          this.address2 = '';
          this.address3 = '';
        }
        this.helpTextD = shwData.bottomText;
        //  console.log(shwData.bottomText);
      }


    });

//  for kodata
    this.hwaOverlayService.previewkoData().subscribe((koData: any) => {
      if (this.checkFlag === 'koData') {
        this.showKo = koData.showKo;
        this.helpTextD = koData.bottomText;
        this.defaultQ = koData.defaultQ;
        this.customQ = koData.customQ;
      }
    });
    //  Skill Data
    this.hwaOverlayService.previewSkillData().subscribe((skillData: any) => {
      if (this.checkFlag === 'fromSkill') {
        this.showskill = skillData.showskill;
        this.helpTextD = skillData.bottomText;
        this.skillQusetion = [];
        for (let i = 0; i < skillData.skillQusetion.length; i++) {
          if (this.checkstatus(skillData.skillQusetion[i].expertiseLevel, skillData.skillQusetion[i].experenceLevel)) {
            this.skillQusetion.push(skillData.skillQusetion[i]);
          }
        }
      }
    });
// profile DATA
    this.hwaOverlayService.previewProfileData().subscribe((profileData: any) => {
      if (this.checkFlagFOr === 'fromProfile4') {
        this.laleOfSteps = 'Final Ad Review';
      } else {
        this.laleOfSteps = 'Save & Continue';
      }
      this.showProfile = true;
      this.profAllStapsData = [];
      this.profAllStapsData = profileData;
    });


  }
  close() {
    this.visible = false;
    this.visibleChange.emit(this.visible);
  }
  print() {
    window.bind(print());
  }
  goToNext() {
    if (this.checkFlag === 'fromHwa') {
      //   this.router.navigate(['/addKnockoutQuestion']);
      this.onSubmit.emit('fromOverlay');
    } else if (this.checkFlag === 'koData') {
      this.onSubmitKo.emit('fromKoOverlay');
    } else if (this.checkFlag === 'fromSkill') {
      this.onSubmitSkill.emit('fromSkillOverlay');
      return false;
    }
  }
  saveBusinessProfile() {
    this.SaveBusinessProfile.emit();
  }
  leranMore() {
    if (this.level2) {
      this.level2 = false;
    } else {
      this.level2 = true;
    }
  }
 
  postMyAd() {
    if (this.checkFlag === 'fromHwa') {
      this.onSubmit.emit('postMyad');
    } else if (this.checkFlag === 'koData') {

      this.onSubmitKo.emit('postMyad');
    } else if (this.checkFlag === 'fromSkill') {
      this.onSubmitSkill.emit('postMyad');
      return false;
    }
  }
  postHwa() {
    if (this.beforePost) {
      this.beforePost = false;
    } else {
      this.beforePost = true;
    }
  }
  Close2() {
    if (this.level2) {
      this.level2 = false;
    } else {
      this.level2 = true;
    }
  }
  Close3() {
    if (this.beforePost) {
      this.beforePost = false;
    } else {
      this.beforePost = true;
    }
  }
  checkstatus(expertiseLevel, experenceLevel): boolean {
    let flag = true;
    if ((expertiseLevel === 'N/A') && (experenceLevel === 'N/A')) {
      flag = false;
    }
    return flag;
  }
}
