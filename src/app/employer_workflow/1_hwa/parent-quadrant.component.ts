import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl, SafeHtml } from '@angular/platform-browser';
import {Router, ActivatedRoute} from '@angular/router';
import {Subscription } from 'rxjs';
import {AlertService} from '../../services/alert.service';
import {UserService} from '../../services/user.service';
import {DataService} from '../../services/data.service';
import {AllApplicantServicesService} from '../2_allapplicant/services/all-applicant-services.service';


@Component({
  selector: 'app-parent-quadrant',
  templateUrl: './parent-quadrant.component.html',
  styleUrls: ['./parent-quadrant.component.css']
})
export class ParentQuadrantComponent implements OnInit {
  private greeting:string = '';
  public companyname: string = '';
  private pageHtml:SafeHtml = '';
  public firstName: string = '';
  private displayLogedIn: boolean;
  private alertmsg: boolean;
  private subscription: Subscription;
  private enableAllApplicant: boolean;
  constructor(private activatedRoute: ActivatedRoute,
              private alertService: AlertService,
              private router: Router,
              public userService: UserService,
              private dataService: DataService,
              private domSanitizer: DomSanitizer,
              private allApplicantService: AllApplicantServicesService) { }

  ngOnInit() {

    this.subscription = this.activatedRoute.params.subscribe(
      (param: any) => {
        if (param['frmpaypal'] === '1') {
          localStorage.removeItem('storeHwaFormData');
          localStorage.removeItem('storeHwaNid');
          localStorage.removeItem('profileNid');
          localStorage.removeItem('prof1_img_nid');
          localStorage.removeItem('prof2_img_nid');
          localStorage.removeItem('prof3_img_nid');
          localStorage.removeItem('prof4_img_nid');
          localStorage.removeItem('pamentData');
          localStorage.removeItem('copyHwaNid');
        }
      });
    let user = this.userService.isLogedin();
    let empid = {'uid': user.uid };
    this.allApplicantService.checkQuardrantInfo(empid).subscribe(
      res => {
        console.log(res)
        if (res.applicant_status[2].label === 'New' && res.applicant_status[2].value === 'Yes') {
         this.enableAllApplicant = true;
        } else {
         this.enableAllApplicant = false;
          console.log('No candidate applied yet!');
        }
      }
    );
    if (user) {
      let uidObj = {'user_id': user.uid};
      this.dataService.getDashboardData(uidObj).subscribe(
        res => {
          let d = new Date();
          let hrs = d.getHours();
          let msg = '';
          console.log('greeting hrs ',hrs);
          //if (hrs >  0) msg = 'Good Morning'; //  REALLY early
          //if (hrs >  6) msg = 'Good Morning';      //  After 6am
          //if (hrs > 12) msg = 'Good Afternoon';    //  After 12pm
         // if (hrs > 17) msg = 'Good Evening';      //  After 5pm
         // if (hrs > 22) msg = 'Good Evening';        //  After 10pm

          if (hrs < 12) {
            msg = 'Good Morning';
          }else if (hrs >= 12 && hrs < 16){
            msg = 'Good Afternoon';
          }else if (hrs >= 16 && hrs <= 24){
            msg = 'Good Evening';
          }
          console.log('greeting msg ',msg);
          this.greeting = msg + ' ' + res.f_name;
          this.firstName = res.f_name;
        
        });
    }
  }

  goToHWAScreen() {
    this.router.navigate['/hwa_workflow'];
  }

}
