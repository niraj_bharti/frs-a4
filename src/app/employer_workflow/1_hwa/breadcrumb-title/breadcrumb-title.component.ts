import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import {OverlayDataService} from '../../../services/overlay-data.service';
import {UserService} from '../../../services/user.service';
import {DataService} from '../../../services/data.service';
import {DomSanitizer} from '@angular/platform-browser';


@Component({
  selector: 'breadcrumb-title',
  templateUrl: './breadcrumb-title.component.html',
  styleUrls: ['./breadcrumb-title.component.css']
})
export class BreadcrumbTitleComponent implements OnInit {
  @Input() checkFlag: string;
  private hwa_breadcrumb: boolean;
  private ko_breadcrumb: boolean;
  private skill_breadcrumb: boolean;
  private profile_breadcrumb: boolean;
  private hwaqurd_breadcrumb: boolean;
  private postad1_breadcrumb: boolean;
  private copyhwa_breadcrumb: boolean;
  private viewad_breadcrumb: boolean;
  private copyF_breadcrumb: boolean;
  private copyF_breadcrumb2: boolean;
  private extend_breadcrumb: boolean;
  private edithwa_breadcrumb: boolean;
  private biz: string = '';
  constructor(private hwaOverlayService: OverlayDataService,
              private router: Router,
              public userService: UserService,
              private dataService: DataService,
              private domSanitizer: DomSanitizer) { }

  ngOnInit() {
    if (this.checkFlag === 'fromHwa') {
      this.hwa_breadcrumb = true;
      this.biz = 'Create Help Wanted Ad';
    } else if (this.checkFlag === 'koData') {
      this.ko_breadcrumb = true;
      this.biz = 'Create Help Wanted Ad';

    } else if (this.checkFlag === 'fromSkill') {
      this.skill_breadcrumb = true;
      this.biz = 'Create Help Wanted Ad';
    } else if (this.checkFlag === 'fromProfile') {
      this.profile_breadcrumb = true;
      this.biz = 'Create Help Wanted Ad : Build Out Your Business Profile';
    } else if (this.checkFlag === 'hwa_quad') {
      this.hwaqurd_breadcrumb = true;
      this.biz = 'Help Wanted Ad';
    } else if (this.checkFlag === 'post_ad1') {
      this.postad1_breadcrumb = true;
      this.biz = 'Post My Help Wanted Ad';
    } else if (this.checkFlag === 'copyhwa1') {
      this.copyhwa_breadcrumb = true;
      this.biz = 'Your Help Wanted Ad Library';
    } else if (this.checkFlag === 'viewads1') {
      this.viewad_breadcrumb = true;
      this.biz = 'View Help Wanted Ad';
    } else if (this.checkFlag === 'copyHWA2') {
      this.copyF_breadcrumb = true;
      this.biz = 'Copy of My Help Wanted Ad';
    } else if (this.checkFlag === 'copyHWA3') {
      this.copyF_breadcrumb2 = true;
      this.biz = 'Edit My Help Wanted Ad';
    } else if (this.checkFlag === 'extendB') {
      this.extend_breadcrumb = true;
      this.biz = 'Your Help Wanted Ad Library';
    } else if (this.checkFlag === 'editthing') {
      this.edithwa_breadcrumb = true;
      this.biz = 'Your Help Wanted Ad Library';
    }
  }

}
