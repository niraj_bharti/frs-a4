import { Injectable, Input } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {BehaviorSubject} from 'rxjs/Rx';
@Injectable()
export class UserService {
  public switchUrl= 'http://dev-frslive.pantheonsite.io/';
  public paypalRedirect = 'http://dev.hurekadev.info';
  private registerUrl = this.switchUrl +'user_registration.json';
  private authValidUrl = this.switchUrl +'user_token_aunthentication.json/';
  private resetPassUrl = this.switchUrl +'user_reset_password.json'
 //  private loginUrl = this.switchUrl +"user/login?_format=json";
 // private loginUrl = this.switchUrl + 'login.json';
  private loginUrl = this.switchUrl + 'employer_login.json';
// forgot password urls
  private forgotPassUrl = this.switchUrl +'user_forgot_password_step_first.json';
  private tokenValidateUrl = this.switchUrl +'user_forgot_password_step_second.json';   // token validate
  private resetPassUrl_fotgot = this.switchUrl +'user_forgot_password_step_third.json'; // save password
  private userInfoUrl=this.switchUrl +'user_information.json';
  private getAddress=this.switchUrl +'address/api?uid=';
  private googeMap='https://maps.googleapis.com/maps/api/geocode/json?address=';
  private ifUserExistApi = this.switchUrl + 'user_exist/';



  private headers = new Headers();
  private _isLogedIn: boolean;
//  @Input()
  private subjectIsLogin: Subject<Boolean> = new BehaviorSubject<Boolean>(false);

  constructor(private http: Http, private router: Router) {

  }
  create(user):any { // Array<Object>
    return this.http.post(this.registerUrl, user, { headers: this.authHeaders() })
      .map((res:Response) => res.json())
      .catch((error:any) => error.json());
  }
  validateUrltoken(user) {
    return this.http.post(this.authValidUrl, user, { headers: this.authHeaders() })
      .map((response: Response) => response.json())
      .catch((error:any) => error.json());
  }
  resetPass(user) { // uid, tokenid // + tokenid + "/" + uid
    return this.http.post(this.resetPassUrl, user, { headers: this.authHeaders() })
      .map((response: Response) => response.json())
      .catch((error:any) => error.json());
  }

  login(user) {
    return this.http.post(this.loginUrl, user, { headers: this.authHeaders() })
      .map((response: Response) => response.json())
      .catch((error:any) => error.json());
  }
  forgotPassword(email) {
    return this.http.post(this.forgotPassUrl, email, { headers: this.authHeaders() })
      .map((response: Response) => response.json())
      .catch((error:any) => error.json());
  }
  accesData(userId){
    return this.http.post(this.userInfoUrl, userId, this.jwt())
      .map((response:Response) => response.json())
      .catch((error:any) => error.json())
  }
  isLogedin() {

    var token = JSON.parse(localStorage.getItem('userToken'));
    var user = JSON.parse(localStorage.getItem('currentUser'));
    if(token) {
      this.setLogedIn(true);
        // this.router.navigate(['/']);
        return user;
    } else {
      this.setLogedIn(false);
      this.router.navigate(['/login']);
      return null;
    }

  }
  //

  //
  ifUserExist(uid) {
    return this.http.get(this.ifUserExistApi+uid)
      .map((res:Response) => res.json())
      .catch((error:any) => error.json())
  }

  getaddress(uid){
    return this.http.get(this.getAddress+uid, this.jwt())
      .map((res:Response) => res.json())
      .catch((error:any) => error.json())
  }

  getUserData(){
    let user  = JSON.parse(localStorage.getItem('currentUser'));
      if(user) {
      //  console.log("USER data: ", user)
        var userId = {"uid": user.uid }
        this.accesData(userId).subscribe(
          res => {
            if(res) {
              localStorage.setItem('currentUser', JSON.stringify(res));
              this.setLogedIn(true);
              //console.log(res)
            }
          },
          error =>{
            console.log(error);
            this.setLogedIn(false);
          }
        )
      }
  }
  removeHWAstorage() {
    localStorage.removeItem('storeHwaFormData');
    localStorage.removeItem('storeHwaNid');
    localStorage.removeItem('profileNid');
    localStorage.removeItem('prof1_img_nid');
    localStorage.removeItem('prof2_img_nid');
    localStorage.removeItem('prof3_img_nid');
    localStorage.removeItem('prof4_img_nid');
    localStorage.removeItem('pamentData');
    localStorage.removeItem('copyHwaNid');
  }
  logout() {
    // remove user from local storage to log user out
    if(localStorage.getItem('userToken')){
    localStorage.removeItem('currentUser');
    localStorage.removeItem('userToken');
    localStorage.removeItem('storeHwaFormData');
    localStorage.removeItem('storeHwaNid');
    // clear profile data here
    localStorage.removeItem('profileNid');
    localStorage.removeItem('prof1_img_nid');
    localStorage.removeItem('prof2_img_nid');
    localStorage.removeItem('prof3_img_nid');
    localStorage.removeItem('prof4_img_nid');
    localStorage.removeItem('pamentData');
    localStorage.removeItem('copyHwaNid');

    this.setLogedIn(false);
    this.router.navigate(['/login']);
  }
  }

  setLogedIn(isLogedIn: boolean): void {
    this._isLogedIn = isLogedIn;
    this.subjectIsLogin.next(isLogedIn);
  }

  getLogedIn(): Observable<Boolean> {
    return this.subjectIsLogin.asObservable();
  }
  //

  searchZip(zipc){
    return this.http.get(this.googeMap+zipc)
      .map((res:Response) => res.json())
      .catch((error:any) => error.json())
  }
// just header setting

  authHeaders() {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    // headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return headers;
  }

// basic auth
  basicAuth() {
    //Temp hide untill basic auth
    let username = 'admin';
     let password = 'admin';
    let token = btoa(username + ':' + password);
    var headers = new Headers();
    headers.append('Authorization', 'Basic ' + token);
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    // headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return headers;
  }

// Token Auth here
  private jwt() {
    // create authorization header with jwt token
    let userToken = JSON.parse(localStorage.getItem('userToken'));
    if (userToken) {
      let headers = new Headers({ 'Authorization': 'Bearer ' + userToken });
      return new RequestOptions({ headers: headers });
    }
  }
}
