import {Directive, ElementRef} from '@angular/core';
declare var $: any;
@Directive({
  selector: '[appFileUpload]'
})
export class FileUploadDirective {

  constructor(private el: ElementRef) {
    $(this.el.nativeElement).change(function () {
     let getFilename = $(this).val().split('\\').pop();
     if (getFilename !== '') {
       $(this).next().find('strong').text(getFilename);
       localStorage.setItem('lastUsedFile' , getFilename);
     } else {
       $(this).next().find('strong').text('Upload Resume');
     }
    });
   }

}
