import { Component, NgZone } from '@angular/core';
import { AuthService } from 'angular2-social-login';
import { Router } from '@angular/router';
import { ApplicantService } from './services/applicants.service';

@Component({
  selector: 'applicant-login',
  templateUrl: './applicant-workflow.component.html',
  styleUrls: ['./applicant-workflow.component.css']
})
export class ApplicantWorkflowComponent {
  public user;
  sub: any;
  private userExsit;
  private loginConfirm: boolean;
  constructor(public _auth: AuthService,
              private router: Router,
              private applicantService: ApplicantService,
              private zone: NgZone
  ) { }

  signIn(provider) {
    this.sub = this._auth.login(provider).subscribe(
      (data) => {
        this.userExsit = '';
        this.loginConfirm = true;
        this.user = data;
        let socialObj = {
          'mail': data['email'],
          'field_first_name': data['name'].split(' ')[0],
          'field_last_name': data['name'].split(' ').pop(),
          'field_phone_number': '',
          'field_city': '',
          'field_state': '',
          'field_zip': '',
          'social': 'true'
        };
        this.applicantService.socialLogin(socialObj).subscribe(
          res => {
            this.loginConfirm = false;
            if (res['current_user']) {
              localStorage.removeItem('currentCandidateName');
              localStorage.setItem('loginCandidateUid', res['current_user'].uid);
              localStorage.setItem('loginCandidateName', data['name'].split(' ')[0]);
              localStorage.setItem('loginCandidateLastName', data['name'].split(' ').pop());
              localStorage.setItem('loginCandidateEmail',  data['email']);
              localStorage.setItem('applicantUserToken', res['jwt_token']);
              localStorage.setItem('applicantUser', res['current_user']);
              localStorage.setItem('applicantLogins', res['current_user'].name);
              this.zone.run(() => {
                this.router.navigate(['/moreinfo']);
              });
            } else {
              console.log(res);
              console.log(res['get_data']['user_status']);
              this.zone.run(() => {
                this.userExsit = 'This email ID is already Exist!';
              });
            }
          },
          error => {
            this.userExsit = 'This email ID is already Exist!';
          });
      });
  }

  logout() {
    localStorage.clear();
    this._auth.logout().subscribe(
      (data) => { this.user = null; }
    );

  }
}
