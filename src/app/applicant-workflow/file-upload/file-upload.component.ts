import { Component, OnInit } from '@angular/core';
import { ApplicantService } from '../services/applicants.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {
  private showFilesucc;
  private showFileError;
  private resumeUploded: boolean;
  private imgLoading: boolean;
  private showModel: boolean;
  private dynamicText = 'Continue';
  private abs: boolean;

  public fileUploadForm: FormGroup;
  constructor(private HwaServicesforApplicant: ApplicantService,
              private formbuilder: FormBuilder,
              private router: Router,
              private activatedRoute: ActivatedRoute, ) { }

  ngOnInit() {

    // Forbuilder For Upload resume FormBuilder
    this.fileUploadForm = this.formbuilder.group({
      'fileup': ['', Validators.required]
    });

    // Checking whether resume id is on system or not
    if (localStorage.getItem('resumeId')) {
      $('#resumeFile').next().find('strong').text(localStorage.getItem('lastUsedFile'));
      this.resumeUploded = true;
    }

  }

  fileChange(event) {
    // Clearing text from message
    this.showFilesucc = '';
    this.showFileError = '';
    this.resumeUploded = false;
    this.imgLoading = true;
    this.dynamicText = 'Uploading';
    this.abs = true;

    // Remove the local data for build resume TRACK
    localStorage.removeItem('commingThroughBuildResume');

    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {

      let file: File = fileList[0];
      let formData: FormData = new FormData();
      formData.append('file', file, file.name);

      // getting value after dot
      let checkType = file.name.split('.').pop();
      if (checkType === 'pdf' || checkType === 'doc' || checkType === 'docx') {
        if (file.size <= 10000000) {
          this.showFilesucc = '';
          this.HwaServicesforApplicant.fileUpload(formData).subscribe(
            res => {
              if (res['fid']) {
                console.log('Getting FID', res.fid)
                this.imgLoading = false;
                this.dynamicText = 'Continue';
                this.abs = false;
                localStorage.setItem('resumeId', res.fid);
                this.showFilesucc = 'File uploaded Successfully !';
                this.resumeUploded = true;
              } else {
                this.imgLoading = true;
                this.dynamicText = 'Continue';
                this.abs = false;
              }
            });
        } else {
          this.imgLoading = false;
          this.dynamicText = 'Continue';
          this.abs = false;
          this.showFileError = 'Please choose a file <10mb size';
        }
      } else {
        this.imgLoading = false;
        this.abs = false;
        this.dynamicText = 'Continue';
        this.showFileError = 'Please upload valid File Format (Pdf, Doc, Docx)';
        this.showFilesucc = '';
      }
    }
   // this.dynamicText = 'Continue';
  }

  uploadResume() {
    this.dynamicText = 'Processing';
    this.abs = true;
    console.log('field_resume_upload', localStorage.getItem('resumeId'))
    if (localStorage.getItem('loginCandidateUid')) {
      let fileObj = {
        'uid': localStorage.getItem('loginCandidateUid'),
         'resume_type': 'File',
          'field_resume_upload': localStorage.getItem('resumeId')
      };

      this.HwaServicesforApplicant.updateCustomResume(fileObj).subscribe(
        res => {
          console.log(fileObj)
          let changeValue = {
            'hwa_id': localStorage.getItem('applicantHWAToken'),
            'uid': localStorage.getItem('loginCandidateUid'),
          };
          this.HwaServicesforApplicant.saveKnockoutQuestion(changeValue).subscribe(
            data => {
              this.alreadyAppliedHwa();
            });

        }
      );
    } else {
      this.router.navigate(['/basicinformation']);
    }
  }

  // Check if already applied
  alreadyAppliedHwa() {
    let obj = {
      'uid' : localStorage.getItem('loginCandidateUid'),
      'hwa_id': localStorage.getItem('applicantHWAToken')
    };
    this.HwaServicesforApplicant.ifApplied(obj).subscribe(
      response => {
        this.dynamicText = 'Continue';
        this.abs = false;
        console.log(response)
        if (response.applied.status === 'Yes') {
        this.showModel = true;
        } else {
          this.router.navigate(['/knockoutquestion']);
        }
      }
    );
  }
  closeModel() {
    this.showModel = false;
  }
}
