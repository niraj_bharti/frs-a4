import { Component, OnInit, Inject } from '@angular/core';
import { ApplicantService } from '../services/applicants.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';

@Component({
  selector: 'app-more-info',
  templateUrl: './more-info.component.html',
  styleUrls: ['../applicant.css']
})
export class MoreInfoComponent implements OnInit {
  private moreInfoForm: FormGroup;
  public myModel = '';
  public mask = [/[1-9]/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public zipmask = [/[0-9]/, /\d/, /\d/, /\d/, /\d/];
  public emailMask = /^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  private getCityinfoList = [];
  private validZipStatus: string;
  private showSelect: boolean;
  private pageLoading: boolean;
  private contentLoad: boolean;
  private localCandidateName = localStorage.getItem('loginCandidateName');
  private localCandidateLName = localStorage.getItem('loginCandidateLastName');
  private localCandidateEmail = localStorage.getItem('loginCandidateEmail');
  constructor( @Inject(DOCUMENT) private document: Document, private HwaServicesforApplicant: ApplicantService,
               private formbuilder: FormBuilder, private router: Router,
               private userService: UserService, ) { }

  ngOnInit() {

    // Unique Classname for body to call a different background images
    this.document.body.classList.add('applicantBody');
    this.document.body.classList.add('buildresume1');

    // Checking if user is alredy Logged In using SOCIAL/GENRAL

    // PRELOADING
    this.pageLoading = true;
    this.contentLoad = true;
    // Forbuilder For Upload resume FormBuilder

    // Load Form
    this.initMoreInfo(this.localCandidateName, this.localCandidateLName , this.localCandidateEmail, '', '', '', '');
    this.getProfileData();
  }


  // Init Form
  initMoreInfo(fname, lname, email, phone, zip, city, state) {
    this.moreInfoForm = this.formbuilder.group({
      'firstName': [fname, Validators.required],
      'lastName': [lname, Validators.required],
      'Email': [email, Validators.compose([Validators.required,
        Validators.pattern(this.emailMask)])],
      'cellPhone': [phone, [Validators.required, Validators.pattern('^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$')]],
      'zipCode': [zip, Validators.required],
      'city': [city, Validators.required],
      'state': [state, Validators.required],
    });
  }

  // Get Details
    getProfileData() {
    let uid = {
      'uid': localStorage.getItem('loginCandidateUid')
    };
      this.HwaServicesforApplicant.applicantProfileGet(uid).subscribe(
        res => {
          if (res) {
            console.log(res);
            let trimStr = res['applicant_detail'][0];
            this.initMoreInfo(
              trimStr.field_first_name,
              trimStr.field_last_name,
              trimStr.name,
              trimStr.field_phone_number,
              trimStr.field_zip,
              trimStr.field_city,
              trimStr.field_state
            );
          }
        });
    }
  // Update Details
  continueTo() {
    console.log(this.moreInfoForm.controls)
    let parm = {
      'uid': localStorage.getItem('loginCandidateUid'),
      'field_first_name' : this.moreInfoForm.controls.firstName.value,
      'field_last_name' : this.moreInfoForm.controls.lastName.value,
      'name': this.moreInfoForm.controls.Email.value,
      'field_phone_number' : this.moreInfoForm.controls.cellPhone.value,
      'field_zip': this.moreInfoForm.controls.zipCode.value,
      'field_city': this.moreInfoForm.controls.city.value,
      'field_state': this.moreInfoForm.controls.state.value
    };
    this.HwaServicesforApplicant.socialmoreInfo(parm).subscribe(
      res => {
        if (res) {
          this.router.navigate(['/useexisting']);
        }
      });
  }

  // Valid Zipcode, checking if this is only for USA
  validUsCountry(res): boolean {
    let isUs = false;
    // console.log(res)
    if (res['status'] === 'OK') {
      for (let i = 0; i <= res['results'][0].address_components.length - 1; i++) {
        if (res['results'][0].address_components[i].short_name === 'US') {
          isUs = true;
          // console.log('isUs', isUs)
          break;
        }
      }
    }
    return isUs;
  }



  // Auto Filled Zip code city and state function
  zipCodeSearch(count) {
    let zipc = this.moreInfoForm.value.zipCode;
    if (!isNaN(zipc)) {

      this.userService.searchZip(zipc).subscribe(
        res => {
          if (this.validUsCountry(res)) {
            if (res['results'][0].postcode_localities) {
              this.showSelect = true;
              this.getCityinfoList = [];
              for (let j = 0; j <= res['results'][0].postcode_localities.length; j++) {

                this.getCityinfoList.push(res['results'][0].postcode_localities[j]);
              }

            } else {
              this.showSelect = false;
            }

            for (let i = 0; i <= res['results'][0].address_components.length - 1; i++) {

              if (res['results'][0].address_components[i].types[0] === 'administrative_area_level_1') {
                let getstate = res['results'][0].address_components[i].short_name;
                let getCity = res['results'][0].address_components[1].long_name;
                let arryobj = [];
                arryobj[count] = { state: getstate, city: getCity };
                this.moreInfoForm.controls['state'].patchValue(getstate);
                this.moreInfoForm.controls['city'].patchValue(getCity);
                this.validZipStatus = '';
              }

            }
          } else {
            this.validZipStatus = 'Please enter valid US zip code';
            this.moreInfoForm.controls['city'].patchValue('');
            this.moreInfoForm.controls['state'].patchValue('');
            this.showSelect[count] = false;
            // this.hideInput=true;
          }
        });
    } else {
      this.validZipStatus = 'Please enter valid US zip code';
      this.moreInfoForm.controls['city'].patchValue('');
      this.moreInfoForm.controls['state'].patchValue('');
      this.showSelect[count] = false;
    }
  }

}
