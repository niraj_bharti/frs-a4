import { Component, OnInit, Inject} from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApplicantService } from '../../services/applicants.service';

@Component({
  selector: 'app-objective',
  templateUrl: './objective.component.html',
  styleUrls: ['./objective.component.css', '../buildresume.css']
})
export class ObjectiveComponent implements OnInit {
  public objectiveForm: FormGroup;
  private pageLoded: boolean;
  constructor( @Inject(DOCUMENT) private document: Document, private formbuilder: FormBuilder, private router: Router,
              private applicantService: ApplicantService) { }

  ngOnInit() {

    // Unique Classname for body to call a different background images
    this.document.body.classList.add('applicantBody');
    this.document.body.classList.add('buildresume1');

    // Feathing and initializing
    this.initFormControl('');
    this.fetchFieldData();
  }
  // Initializing form
  initFormControl(obj) {
    this.objectiveForm = this.formbuilder.group({
      'object': [obj, Validators.required]
    });

  }
  // Feaching submitted data
  fetchFieldData() {
    let checkStartus;
    if (localStorage.getItem('loginCandidateUid')) {
      checkStartus = localStorage.getItem('loginCandidateUid');
    } else {
      checkStartus = localStorage.getItem('currentApplicantID');
    }
    let Uid = {
      'uid': checkStartus,
      'hwa_nid': localStorage.getItem('applicantHWAToken')
    };
    this.applicantService.getResumeData(Uid).subscribe(
      res => {
        console.log(res['applicant_resume_detail']);
        if (res['applicant_resume_detail']['field_career_objective']) {
          this.initFormControl(res['applicant_resume_detail']['field_career_objective'][0].value);
        }
        this.pageLoded = true;
      });
  }
  // Submitting Form Data to backend
  submitContinue() {
    // Check USER TYPE(Build Resume/Login)
    let userType;
    if (localStorage.getItem('loginCandidateUid')) {
      userType = localStorage.getItem('loginCandidateUid');
    } else {
      userType = localStorage.getItem('currentApplicantID');
    }

    // Post key Data API FORMAT
    let objectiveObj = {
      'uid': userType,
      'resume_type': 'ResumeBuilder',
      'resume_fid': '',
      'resume_template': '',
      'field_career_objective': this.objectiveForm.controls['object'].value
    };
    // POSTING THE DATA TO SERVER(EMPLOYEMENT HISTORY)
    this.applicantService.updateCustomResume(objectiveObj).subscribe(
      res => {
        this.router.navigate(['/accomplishment']);
      });
  }
}
