import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ApplicantService } from '../../services/applicants.service';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-resume-preview',
  templateUrl: './resume-preview.component.html',
  styleUrls: ['./resume-preview.component.css', '../buildresume.css']
})
export class ResumePreviewComponent implements OnInit {
  private workExp = [];
  private eduDeteail = [];
  private licence = [];
  public cName;
  private address;
  private phone;
  private email;
  private objective;
  private skill: any;
  private pageLoded: boolean;
  private militryI;
  private refrence;
  private refrenceList = [];
  private totalExp;
  private totalExp1 = [];
  private sum = 0;
  private disable: boolean;
  private staticURL;
  private relDate: boolean;

  constructor( @Inject(DOCUMENT) private document: Document, private formbuilder: FormBuilder, private router: Router,
               private applicantService: ApplicantService, private userService: UserService) { }

  ngOnInit() {
    // Unique Classname for body to call a different background images
    this.document.body.classList.add('applicantBody');
    this.document.body.classList.add('buildresume1');

    // call
    this.resumeData();


    // check if user has already applied for this position
    let obj = {
      'uid' : localStorage.getItem('loginCandidateUid'),
      'hwa_id': localStorage.getItem('applicantHWAToken')
    };
    this.applicantService.ifApplied(obj).subscribe(
      response => {
        if (response.applied.status === 'Yes') {
          this.disable = false;
        } else {
          this.disable = true;
        }
      }
    );


  }
  resumeData() {
    let checkStartus;
    if (localStorage.getItem('loginCandidateUid')) {
      checkStartus = localStorage.getItem('loginCandidateUid');
    } else {
      checkStartus = localStorage.getItem('currentApplicantID');
    }
    let Uid = {
      'uid': checkStartus,
      'hwa_nid': localStorage.getItem('applicantHWAToken')
    };
    this.applicantService.getResumeData(Uid).subscribe(

      res => {
        console.log('AllData', res);
        localStorage.setItem('pdfid', res.applicant_resume_detail.nid[0].value);
        this.cName = res['user_detail'].field_first_name[0].value + ' ' + res['user_detail'].field_last_name[0].value;
        this.address = res['user_detail'].field_city[0].value;
        this.phone = res['user_detail'].field_phone_number[0].value;
        this.email = res['user_detail'].mail[0].value;
        this.objective = res['applicant_resume_detail'].field_career_objective;
        this.skill = res['applicant_resume_detail']['field_accomplishment_skill'];


        this.eduDeteail = res['applicant_resume_detail']['field_education_details'];
        this.workExp = res['applicant_resume_detail'].field_employment_history;

        this.licence = res['applicant_resume_detail'].field_licenses_certification;
        this.militryI = res['applicant_resume_detail'].field_military_history;
        this.refrenceList = res['applicant_resume_detail'].field_references;
        console.log(this.refrenceList)
        if (res['applicant_resume_detail'].field_experience_year !== undefined) {
          this.totalExp = res['applicant_resume_detail'].field_experience_year[0].value + '.' + res['applicant_resume_detail'].field_experience_month[0].value;
        }
        if (this.workExp) {

          for (let i = 0; i < this.workExp.length; i++) {

             // let timeVal1 = res['applicant_resume_detail'].field_employment_history[i].field_joining_date[0].value;
            // let timeVal2 = res['applicant_resume_detail'].field_employment_history[i].field_re_leaving_date[0].value;


           // let dt1 = new Date(timeVal1 * 1000);
            // let dt2 = new Date(timeVal2 * 1000);
           // this.totalExp = this.totalYear(dt2, dt1);
           // this.totalExp1.push(this.totalExp)
           // this.sum += this.totalExp1[i];
          }
        }
        this.pageLoded = true;

        this.staticURL = this.userService.switchUrl + 'print/pdf/node/' + localStorage.getItem('pdfid');
      });
  }

  submitContinue() {
    this.router.navigate(['/knockoutquestion']);
  }

  convertTimestapm(timestamp) {
    return new Date(timestamp * 1000);
  }

  totalYear(dt2, dt1) {
  let diff = (dt2.getTime() - dt1.getTime()) / 1000;
  diff /= (60 * 60 * 24);
  return Math.abs(Math.round(diff / 365.25));
 }
 // get sum of array
  getSum(total, num) {
    return total + num;
  }

  // Print Resume
  printThis() {
    window.print();
  }

}
