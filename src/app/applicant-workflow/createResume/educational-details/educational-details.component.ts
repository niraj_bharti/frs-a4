import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { ApplicantService } from '../../services/applicants.service';

@Component({
  selector: 'app-educational-details',
  templateUrl: './educational-details.component.html',
  styleUrls: ['./educational-details.component.css', '../buildresume.css']
})

export class EducationalDetailsComponent implements OnInit {

  public EducationalDetailForm: FormGroup;
  private getDegreeName = [];
  private postDataEmpHistory = [];
  private openThisFormArray = [];
  private pageLoded: boolean;
  private futureDate: boolean;
  private futureDateIndex = [];

  constructor( @Inject(DOCUMENT) private document: Document, private formbuilder: FormBuilder, private router: Router,
               private applicantService: ApplicantService) { }

  ngOnInit() {

    // Unique Classname for body to call a different background images
    this.document.body.classList.add('applicantBody');
    this.document.body.classList.add('buildresume1');

    // Add Employement History Dynamiccaly
    this.EducationalDetailForm = this.formbuilder.group({
      degreeGroup: this.formbuilder.array([])
    });

    // calling initEmployeeGroup
    this.featchData();

  }

  // Initialize employerGroup
  initEducationDetails(deg, gm, gy, sn, city, state) {
    return this.formbuilder.group({
      'degree_type': [deg, Validators.required],
      'graduation_month': [gm, Validators.required],
      'graduation_year': [gy, Validators.required],
      'school_name': [sn, Validators.required],
      'city': [city, Validators.required],
      'state': [state, Validators.required],
    });
  }

  // Add more List of form ARRAY
  addEducation() {
    // this.openThisFormArray[0] = true;
    const control = <FormArray>this.EducationalDetailForm.controls['degreeGroup'];
    const addrCtrl = this.initEducationDetails('', '', '', '', '', '');
    control.push(addrCtrl);
    this.openThisFormArray.push(true);
    this.accordianList(this.openThisFormArray.length -1 );
    console.log('xxxxxxx', this.openThisFormArray);
  }

  // FEATCH SAVED DATA
  featchData() {
    let checkStartus;
    if (localStorage.getItem('loginCandidateUid')) {
      checkStartus = localStorage.getItem('loginCandidateUid');
    }  else {
      checkStartus = localStorage.getItem('currentApplicantID');
    }
    let Uid = {
      'uid': checkStartus,
      'hwa_nid': localStorage.getItem('applicantHWAToken')
    };
    this.applicantService.getResumeData(Uid).subscribe(
      res => {
        const control = <FormArray>this.EducationalDetailForm.controls['degreeGroup'];
        let trimVal = res['applicant_resume_detail'].field_education_details;
        // alert(trimVal)
        if (trimVal) {
          // joiningTimestamp
          let yrObtainTimestamp = trimVal[0].field_year_graduated[0].value;
          let obtainDate = new Date(yrObtainTimestamp * 1000);

          for (let i = 0; i < trimVal.length; i++) {
            control.push(this.initEducationDetails(
              this.getDegreeName[i] = trimVal[i].field_type_of_degree_earned[0].value,
              obtainDate.getMonth(),
              obtainDate.getFullYear(),
              trimVal[i].field_name_of_school[0].value,
              trimVal[i].field_city[0].value,
              trimVal[i].field_state[0].value
            ));
            if ( i === 0 ) {
              this.openThisFormArray[i] = true;

            } else {
              this.openThisFormArray[i] = false;

            }
          }
        }else {
          this.addEducation();

        }
        this.pageLoded = true;
      });
  }
  // REMOVE FORM ARRAY
  removeEducation(i: number) {
    const control = <FormArray>this.EducationalDetailForm.controls['degreeGroup'];
    control.removeAt(i);
    this.getDegreeName.splice(i, 1);
  }

  // SUBMIT DATA
  submitContinue() {
    // Holding the form control in var
    let emplHisTrim = this.EducationalDetailForm.controls['degreeGroup']['controls'];

    // loop to itrate through form control
    for (let i = 0; i < emplHisTrim.length; i++) {

      // TIMESTAMP METHOD FOR THE JOINING DATA AND RELEAVING DATE USING toTimestamp() FUNCTION
      let graduationYear = this.toTimestamp(emplHisTrim[i].controls.graduation_year.value, emplHisTrim[i].controls.graduation_month.value);

      // Holding the Object for the Post API
      let changeValue = {
        'field_city' : emplHisTrim[i].controls.city.value,
        'field_name_of_school' : emplHisTrim[i].controls.school_name.value,
        'field_state' : emplHisTrim[i].controls.state.value,
        'field_type_of_degree_earned' : emplHisTrim[i].controls.degree_type.value,
        'field_year_graduated' : graduationYear
      };

      // Push the array value
      this.postDataEmpHistory.push(changeValue);
    }

    // Check USER TYPE(Build Resume/Login)
    let userType;
    if (localStorage.getItem('loginCandidateUid')) {
      userType = localStorage.getItem('loginCandidateUid');
    } else {
      userType = localStorage.getItem('currentApplicantID');
    }

    // Post key Data API FORMAT
    let employementObj = {
      'uid' : userType,
      'resume_type' : 'ResumeBuilder',
      'resume_fid' : '',
      'resume_template' : '',
      'education_detail' : this.postDataEmpHistory,
    };
    // POSTING THE DATA TO SERVER(EMPLOYEMENT HISTORY)
    this.applicantService.updateCustomResume(employementObj).subscribe(
      res => {
        this.router.navigate(['/licence']);
      });
  }

  // TIMESTAMP CONVERSION FUNCTION
  toTimestamp(year, month) {
    let datum = new Date(Date.UTC(year, month));
    return datum.getTime() / 1000.0;
  }

  // Accordian LIST FUNCTION
  accoradianIteam(i) {
    this.accordianList(i);
    this.openThisFormArray[i] = !this.openThisFormArray[i];
  }
  accordianList(j) {
    for (let i = 0; i < this.openThisFormArray.length; i++) {
      if (j !== i) {
        this.openThisFormArray[i] = false;
      }
    }
  }

  // SKIP THIS STEPS
  skipThisSteps() {
    this.router.navigate(['/licence']);
  }

  // Getting Month Val
  getMonthValue(i) {
    const control = <FormArray>this.EducationalDetailForm.controls['degreeGroup'];
    let month = control['controls'][i]['controls'].graduation_month.value;
    let year = control['controls'][i]['controls'].graduation_year.value;
    let yearObtain = this.toTimestamp(year, month);
    let returnOrignalValue = new Date(yearObtain * 1000);
    if ( this.checkFutureDate(returnOrignalValue) === false ) {
      this.futureDate = true;
      this.futureDateIndex[i] = true;
    } else {
      this.futureDate = false;
      this.futureDateIndex[i] = false;
    }
  }

  // Future Date
  checkFutureDate(selectedDate) {
    let today = new Date();
    return selectedDate < today;
  }
}
