import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApplicantService } from '../../services/applicants.service';

@Component({
  selector: 'app-military-history',
  templateUrl: './military-history.component.html',
  styleUrls: ['./military-history.component.css', '../buildresume.css']
})

export class MilitaryHistoryComponent implements OnInit {

  public militryForm: FormGroup;
  private pageLoded: boolean;
  private hideSkip: boolean;
  private futureDate: boolean;
  private futureDateIndex: boolean;
  constructor( @Inject(DOCUMENT) private document: Document, private formbuilder: FormBuilder, private router: Router,
              private applicantService: ApplicantService) { }

  ngOnInit() {

    // Unique Classname for body to call a different background images
    this.document.body.classList.add('applicantBody');
    this.document.body.classList.add('buildresume1');

    // Militry Form FormBuilder
    this.initMilitryForm('', '', '', '', '');
    this.getMilitryData();
  }

  // Initialize Militry Group
  initMilitryForm(b, d, ys, dm, dy) {
    this.militryForm = this.formbuilder.group({
      'branch': [b, Validators.required],
      'descharge': [d, Validators.required],
      'year_served': [ys, [Validators.required, Validators.pattern('[0-9]+')]],
      'descharge_month': [dm, Validators.required],
      'descharge_year': [dy, Validators.required],
    });
  }
  getMilitryData() {
    let checkStartus;
    if (localStorage.getItem('loginCandidateUid')) {
      checkStartus = localStorage.getItem('loginCandidateUid');
    } else {
      checkStartus = localStorage.getItem('currentApplicantID');
    }
    let Uid = {
      'uid': checkStartus,
      'hwa_nid': localStorage.getItem('applicantHWAToken')
    };
    this.applicantService.getResumeData(Uid).subscribe(
      res => {
        let trimVal = res['applicant_resume_detail'].field_military_history;
        if (trimVal) {
          if (trimVal.length !== 0) {
              this.hideSkip = true;
          }
          // joiningTimestamp
          let dischargeTimestamp = trimVal[0].field_date_of_your_discharge[0].value;
          let dischargeDate = new Date(dischargeTimestamp * 1000);

          console.log(res['applicant_resume_detail'].field_military_history);
          this.initMilitryForm(
            trimVal[0].field_branch_of_military_served_[0].value,
            trimVal[0].field_rank_at_the_time_of_discha[0].value,
            trimVal[0].field_number_of_years_served[0].value,
            dischargeDate.getMonth(),
            dischargeDate.getFullYear(),
          );

        } else {
          this.initMilitryForm('', '', '', '', '');
        }

        this.pageLoded = true;
      });

  }
  // Submit Mility DataService
  submitContinue() {

    let deschargeTime = this.toTimestamp(this.militryForm.controls['descharge_year'].value,
      this.militryForm.controls['descharge_month'].value);

    // Check USER TYPE(Build Resume/Login)
    let userType;
    if (localStorage.getItem('loginCandidateUid')) {
      userType = localStorage.getItem('loginCandidateUid');
    } else {
      userType = localStorage.getItem('currentApplicantID');
    }

    // Post key Data API FORMAT
    let employementObj = {
      'uid': userType,
      'resume_type': 'ResumeBuilder',
      'resume_fid': '',
      'resume_template': '',
      'military_history': [
        {
          'field_branch_of_military_served_': this.militryForm.controls['branch'].value,
          'field_date_of_your_discharge': deschargeTime,
          'field_number_of_years_served': this.militryForm.controls['year_served'].value,
          'field_rank_at_the_time_of_discha': this.militryForm.controls['descharge'].value
        }
      ],
    };

    // POSTING THE DATA TO SERVER(EMPLOYEMENT HISTORY)
    this.applicantService.updateCustomResume(employementObj).subscribe(
      res => {
        this.router.navigate(['/refrence']);
      });
  }

  // TIMESTAMP CONVERSION FUNCTION
  toTimestamp(year, month) {
    let datum = new Date(Date.UTC(year, month));
    return datum.getTime() / 1000.0;
  }
  // SKIP THIS STEPS
  skipThisSteps() {
    this.router.navigate(['/refrence']);
  }

  // Getting Month Val
  getMonthValue(i) {
    const control = this.militryForm.controls;
    console.log(this.militryForm.controls)
    let month = control.descharge_month.value;
    let year = control.descharge_year.value;
    let yearObtain = this.toTimestamp(year, month);
    let returnOrignalValue = new Date(yearObtain * 1000);
    if ( this.checkFutureDate(returnOrignalValue) === false ) {
      this.futureDate = true;
      this.futureDateIndex = true;
    } else {
      this.futureDate = false;
      this.futureDateIndex = false;
    }
  }

  // Future Date
  checkFutureDate(selectedDate) {
    let today = new Date();
    return selectedDate < today;
  }
}
