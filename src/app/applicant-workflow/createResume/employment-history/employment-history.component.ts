import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { ApplicantService } from '../../services/applicants.service';

@Component({
  selector: 'app-employment-history',
  templateUrl: './employment-history.component.html',
  styleUrls: ['./employment-history.component.css', '../buildresume.css'],

})

export class EmploymentHistoryComponent implements OnInit {
  public EmploymentHistoryForm: FormGroup;
  private employerName;
  private checkBoxValue = [];
  private getEmpName = [];
  private postDataEmpHistory = [];
  private openThisFormArray = [];
  private isDisable: boolean;
  private pageLoded: boolean;
  private futureDate: boolean;
  private futureDateIndex = [];

  private futureDate2: boolean;
  private futureDateIndex2 = [];
  private compareTime;
  private compareTimeFail: boolean;
  private checkboxStatus: boolean;


  constructor( @Inject(DOCUMENT) private document: Document, private formbuilder: FormBuilder, private router: Router,
               private applicantService: ApplicantService) { }

  ngOnInit() {

    // Default heading name for formArray heading
    this.employerName = 'Current Employer';

    // Unique Classname for body to call a different background images
    this.document.body.classList.add('applicantBody');
    this.document.body.classList.add('buildresume1');

    // Add Employement History Dynamiccaly
    this.EmploymentHistoryForm = this.formbuilder.group({
      exp_year: ['', Validators.required],
      exp_month: ['',  Validators.required],
      employerGroup: this.formbuilder.array([])
    });
    // calling initEmployemetGroup for loading first formARRAY
    this.featchData();
    console.log(this.EmploymentHistoryForm.value['employerGroup'].joinning_month)

  }
  // Initialize employerGroup
  initEmployemetGroup(en, ph, city, state, joingM, joingY, relM, relY, ce, jd) {
    return this.formbuilder.group({
      'employer_name': [en, Validators.required],
      'position_held': [ph, Validators.required],
      'city': [city, Validators.required],
      'state': [state, Validators.required],
      'joinning_month': [joingM, Validators.required],
      'joinning_year': [joingY, Validators.required],
      'releaving_month': [relM],
      'releaving_year': [relY],
      'current_employer': [ce],
      'job_description': [jd, Validators.required]
    });
  }

  // Add more List of form ARRAY
  addEmployer() {
    // CHECKING THE SAVED DATA STORE IN COMMON SERVICES AND CALLED ON SUBMIT FUNCTION
    const control = <FormArray>this.EmploymentHistoryForm.controls['employerGroup'];

    //  CHECKING THE FORM DATA WHETHERE IT IS FILLIED OR BLANK
    const addrCtrl = this.initEmployemetGroup('', '', '', '', '', '', '', '', true, '');
   // this.checkBoxValue.push(1);
    control.push(addrCtrl);

    this.openThisFormArray.push(true);
    this.accordianList(this.openThisFormArray.length - 1);
    this.pageLoded = true;


    let trimDate = this.EmploymentHistoryForm.controls['employerGroup']['controls'];
    for (let k = 0; k < this.EmploymentHistoryForm.controls['employerGroup']['controls'].length; k++) {
      if (trimDate[k]['controls'].releaving_month.value !== '' || trimDate[k]['controls'].releaving_year.value !== '') {
        this.isDisable = false;
      } else {
        this.isDisable = true;
      }
    }

  }

  // FEATCH SAVED DATA
  featchData() {

    let checkStartus;
    if (localStorage.getItem('loginCandidateUid')) {
      checkStartus = localStorage.getItem('loginCandidateUid');
    }  else {
      checkStartus = localStorage.getItem('currentApplicantID');
    }
    let Uid = {
      'uid': checkStartus,
      'hwa_nid': localStorage.getItem('applicantHWAToken')
    };
    this.applicantService.getResumeData(Uid).subscribe(
      res => {


        const control = <FormArray>this.EmploymentHistoryForm.controls['employerGroup'];
        let trimVal = res['applicant_resume_detail'].field_employment_history;
       // Experience Year and Month data fetch
        if (res['applicant_resume_detail'].field_experience_month !== undefined) {
          this.EmploymentHistoryForm.controls['exp_month'].setValue(res['applicant_resume_detail'].field_experience_month[0].value);
          this.EmploymentHistoryForm.controls['exp_year'].setValue(res['applicant_resume_detail'].field_experience_year[0].value);
        }
        // End
        if (trimVal) {
          let check_value;
          for (let i = 0; i < trimVal.length; i++) {
           // console.log(trimVal[i].field_i_work_here[0].value);

            // joiningTimestamp
            let joiningTimestamp = trimVal[i].field_joining_date[0].value;
            let joiningDate = new Date(joiningTimestamp * 1000);

            // realeving date
            let releavingTimestamp;
            let releavingDate;

            // I work here item
            if (trimVal[i].field_i_work_here[0].value === '' || trimVal[i].field_i_work_here[0].value === 'false') {
              check_value = false;
              releavingTimestamp =  trimVal[i].field_re_leaving_date[0].value;
              releavingDate = new Date(releavingTimestamp * 1000);
              control.push(this.initEmployemetGroup(
                this.getEmpName[i] = trimVal[i].field_employer_s_name[0].value,
                trimVal[i].field_position_held[0].value,
                trimVal[i].field_city[0].value,
                trimVal[i].field_state[0].value,
                joiningDate.getMonth(),
                joiningDate.getFullYear(),
                releavingDate.getMonth(),
                releavingDate.getFullYear(),
                this.checkBoxValue.push(check_value),
                trimVal[i].field_job_responsibilities[0].value
              ));
            } else {
              check_value = true;
              control.push(this.initEmployemetGroup(
                this.getEmpName[i] = trimVal[i].field_employer_s_name[0].value,
                trimVal[i].field_position_held[0].value,
                trimVal[i].field_city[0].value,
                trimVal[i].field_state[0].value,
                joiningDate.getMonth(),
                joiningDate.getFullYear(),
                '',
                '',
                this.checkBoxValue.push(check_value),
                trimVal[i].field_job_responsibilities[0].value
              ));
            }

            if ( i === 0 ) {
              this.openThisFormArray[i] = true;
            } else {
              this.openThisFormArray[i] = false;
            }
          }
        }else {
          this.addEmployer();
        }
        this.pageLoded = true;
      });
  }
  // REMOVE FORM ARRAY
  removeEmployer(i: number) {
    const control = <FormArray>this.EmploymentHistoryForm.controls['employerGroup'];
    let index = this.checkBoxValue.indexOf(i);
    this.checkBoxValue.splice(index, i);
    control.removeAt(i);
    this.getEmpName.splice(i, 1);
  }

  // SUBMIT DATA
  submitContinue() {
    // Holding the form control in let
    let emplHisTrim = this.EmploymentHistoryForm.controls['employerGroup']['controls'];

    // loop to itrate through form control
    for (let i = 0; i < emplHisTrim.length; i++) {
      console.log(emplHisTrim[i].controls.current_employer.value);
      // TIMESTAMP METHOD FOR THE JOINING DATA AND RELEAVING DATE USING toTimestamp() FUNCTION
      let joiningDate;
      let releaveDate;
      if (emplHisTrim[i].controls.current_employer.value === true) {
        joiningDate = this.toTimestamp(emplHisTrim[i].controls.joinning_year.value, emplHisTrim[i].controls.joinning_month.value);
      } else {
        joiningDate = this.toTimestamp(emplHisTrim[i].controls.joinning_year.value, emplHisTrim[i].controls.joinning_month.value);
        releaveDate = this.toTimestamp(emplHisTrim[i].controls.releaving_year.value, emplHisTrim[i].controls.releaving_month.value);
      }

      // let returnOrignalValue = new Date(convertToTimestamp * 1000);

      // Holding the Object for the Post API
      let changeValue = {
        'field_job_responsibilities': emplHisTrim[i].controls.job_description.value,
        'field_employer_s_name': emplHisTrim[i].controls.employer_name.value,
        'field_joining_date': joiningDate,
        'field_position_held': emplHisTrim[i].controls.position_held.value,
        'field_re_leaving_date': releaveDate,
        'field_i_work_here': emplHisTrim[i].controls.current_employer.value,
        'field_city': emplHisTrim[i].controls.city.value,
        'field_state': emplHisTrim[i].controls.state.value
      };

      // Push the array value
      this.postDataEmpHistory.push(changeValue);

    }
    // Check USER TYPE(Build Resume/Login)
    let checkLocalData;
    if (localStorage.getItem('loginCandidateUid')) {
      checkLocalData = localStorage.getItem('loginCandidateUid');
    } else {
      checkLocalData = localStorage.getItem('currentApplicantID');
    }
    // this.postDataEmpHistory.push(staticVale);
    // Post key Data API FORMAT
    let employementObj = {
      'uid': checkLocalData,
      'resume_type': 'ResumeBuilder',
      'resume_fid': '',
      'resume_template': '',
      'employment_history': this.postDataEmpHistory,
      'field_experience_month': this.EmploymentHistoryForm.controls['exp_month'].value,
      'field_experience_year': this.EmploymentHistoryForm.controls['exp_year'].value

    };
    console.log(employementObj)
    // POSTING THE DATA TO SERVER(EMPLOYEMENT HISTORY) conditionally (check if comming through Login or Build Resume)
    if (localStorage.getItem('loginCandidateUid')) {
      this.applicantService.updateCustomResume(employementObj).subscribe(
        res => {
          this.router.navigate(['/education']);
        }
      );
    } else {
      this.applicantService.updateCustomResume(employementObj).subscribe(
        res => {
          this.router.navigate(['/education']);
        }
      );
    }

  }

  // TIMESTAMP CONVERSION FUNCTION
  toTimestamp(year, month) {
    let datum = new Date(Date.UTC(year, month));
    return datum.getTime() / 1000.0;
  }

  // Accordian LIST FUNCTION
  accoradianIteam(i) {
    this.accordianList (i);
    this.openThisFormArray[i] = !this.openThisFormArray[i];
  }
  accordianList(j) {
    for (let i = 0; i < this.openThisFormArray.length; i++) {
      if (j !== i) {
        this.openThisFormArray[i] = false;
      }
    }
  }
  // SKIP THIS STEPS
  skipThisSteps() {
    this.router.navigate(['/education']);
  }

  // Check Status of
  checkStatusOf(i) {
    let getRelevingMonth = this.EmploymentHistoryForm.controls['employerGroup']['controls'][i]['controls'].releaving_month.value;
    let getRelevingYear = this.EmploymentHistoryForm.controls['employerGroup']['controls'][i]['controls'].releaving_year.value;

    if (this.checkBoxValue[i] === false) {
      this.isDisable = true;
    } else {
      if (getRelevingMonth === '' || getRelevingYear === '') {
        this.isDisable = true;
      } else {
        this.isDisable = false;
      }
      this.isDisable = false;
    }
  }
// Getting Month Val

  getMonthValue(i) {
    const control = <FormArray>this.EmploymentHistoryForm.controls['employerGroup'];
    let month = control['controls'][i]['controls'].joinning_month.value;
    let year = control['controls'][i]['controls'].joinning_year.value;
    let yearObtain = this.toTimestamp(year, month);
    let returnOrignalValue = new Date(yearObtain * 1000);

    //
    const controlA = <FormArray>this.EmploymentHistoryForm.controls['employerGroup'];
    let monthA = controlA['controls'][i]['controls'].releaving_month.value;
    let yearA = controlA['controls'][i]['controls'].releaving_year.value;
    let yearObtainA = this.toTimestamp(yearA, monthA);
    let returnOrignalValueA = new Date(yearObtainA * 1000);
    if (yearA !== '' && monthA !== '') {
      this.compareDate(returnOrignalValue, returnOrignalValueA);
    }
    //
    if ( this.checkFutureDate(returnOrignalValue) === false ) {
      this.futureDate = true;
      this.futureDateIndex[i] = true;
    } else {
      this.futureDate = false;
      this.futureDateIndex[i] = false;
    }
  }

  //
  getMonthValue2(i) {
    const control = <FormArray>this.EmploymentHistoryForm.controls['employerGroup'];
    let month = control['controls'][i]['controls'].releaving_month.value;
    let year = control['controls'][i]['controls'].releaving_year.value;
    let yearObtain = this.toTimestamp(year, month);
    let returnOrignalValue = new Date(yearObtain * 1000);

    //
    const controlA = <FormArray>this.EmploymentHistoryForm.controls['employerGroup'];
    let monthA = controlA['controls'][i]['controls'].joinning_month.value;
    let yearA = control['controls'][i]['controls'].joinning_year.value;
    let yearObtainA = this.toTimestamp(yearA, monthA);
    let returnOrignalValueA = new Date(yearObtainA * 1000);
    if (year !== '' && month !== '') {
      this.compareDate(returnOrignalValueA, returnOrignalValue);
    }
    //
    if ( this.checkFutureDate(returnOrignalValue) === false ) {
      this.futureDate2 = true;
      this.futureDateIndex2[i] = true;
    } else {
      this.futureDate2 = false;
      this.futureDateIndex2[i] = false;
    }
  }

  // Future Date
  checkFutureDate(selectedDate) {
    let today = new Date();
    return selectedDate < today;
  }

  compareDate(toDate, fromDate) {
    if (toDate >= fromDate ) {
      this.compareTime = true;
      this.compareTimeFail = true;
    } else {
      this.compareTime = false;
      this.compareTimeFail = false;
    }
  }

  isChecked(i) {
    const control = <FormArray>this.EmploymentHistoryForm.controls['employerGroup'];
    let checkbox = control['controls'][i]['controls'].current_employer.value;
    let month = control['controls'][i]['controls'].releaving_month.value;
    let year = control['controls'][i]['controls'].releaving_year.value;
    if (checkbox === false) {
      if (month === '' && year === '') {
        this.checkboxStatus = true;
      } else {
        this.checkboxStatus = false;
      }
    } else {
      this.checkboxStatus = false;
    }

  }

}
