import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { ApplicantDataService } from '../../services/applicant-data.service';
import { ApplicantService } from '../../services/applicants.service';

@Component({
  selector: 'app-candidate-refrence',
  templateUrl: './candidate-refrence.component.html',
  styleUrls: ['./candidate-refrence.component.css', '../buildresume.css']
})
export class CandidateRefrenceComponent implements OnInit {


  public refrenceDetailForm: FormGroup;
  private getLicenceName = [];
  private postDataLicence = [];
  public myModel = '';
  public mask = [/[1-9]/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public zipmask = [/[0-9]/, /\d/, /\d/, /\d/, /\d/];
  public emailMask= /^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  private openThisFormArray= [];
  private pageLoded: boolean;
  private hideSkip: boolean;

  constructor( @Inject(DOCUMENT) private document: Document, private formbuilder: FormBuilder, private router: Router,
               private getReserveData: ApplicantDataService, private applicantService: ApplicantService) { }

  ngOnInit() {

    // Unique Classname for body to call a different background images
    this.document.body.classList.add('applicantBody');
    this.document.body.classList.add('buildresume1');

    // Add Employement History Dynamiccaly
    this.refrenceDetailForm = this.formbuilder.group({
      refrenceGroup: this.formbuilder.array([])
    });

    // calling initEmployemetGroup
    this.featchData();
  }
  // Initialize employerGroup
  initEducationDetails(ref, ph, on, em) {
    return this.formbuilder.group({
      'ref_name': [ref, Validators.required],
      'phone': [ph, [Validators.required, Validators.pattern('^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$')]],
      'org_name': [on, Validators.required],
      'email': [em, Validators.compose([Validators.required, Validators.pattern(this.emailMask)])]
    });
  }

  // Add more List of form ARRAY
  addRefrence() {
    const control = <FormArray>this.refrenceDetailForm.controls['refrenceGroup'];
    const addrCtrl = this.initEducationDetails('', '', '', '');
    control.push(addrCtrl);
    this.openThisFormArray.push(true);
    this.accordianList(this.openThisFormArray.length -1 );
  }

  // FEATCH SAVED DATA
  featchData() {
    let checkStartus;
    if (localStorage.getItem('loginCandidateUid')) {
      checkStartus = localStorage.getItem('loginCandidateUid');
    }  else {
      checkStartus = localStorage.getItem('currentApplicantID');
    }
    let Uid = {
      'uid': checkStartus,
      'hwa_nid': localStorage.getItem('applicantHWAToken')
    };
    this.applicantService.getResumeData(Uid).subscribe(
      res => {
        console.log(res['applicant_resume_detail'])
        const control = <FormArray>this.refrenceDetailForm.controls['refrenceGroup'];
        let trimVal = res['applicant_resume_detail'].field_references;
        // alert(trimVal)
        if (trimVal) {
          if (trimVal.length !== 0) {
            this.hideSkip = true;
          }
          for (let i = 0; i < trimVal.length; i++) {
            control.push(this.initEducationDetails(
              this.getLicenceName[i] = trimVal[i].field_name_of_reference[0].value,
              trimVal[i].field_phone_number[0].value,
              trimVal[i].field_organization_name[0].value,
              trimVal[i].field_email[0].value
            ));
            if ( i === 0 ) {
              this.openThisFormArray[i] = true;

            } else {
              this.openThisFormArray[i] = false;

            }
          }
        }else {
          this.addRefrence();

        }
        this.pageLoded = true;
      });
  }

  // REMOVE FORM ARRAY
  removeLicence(i: number) {
    const control = <FormArray>this.refrenceDetailForm.controls['refrenceGroup'];
    control.removeAt(i);
    this.getLicenceName.splice(i, 1);
  }

  // SUBMIT DATA
  submitContinue() {
    // Holding the form control in let
    let refTrim = this.refrenceDetailForm.controls['refrenceGroup']['controls'];

    // loop to itrate through form control
    for (let i = 0; i < refTrim.length; i++) {
      // Holding the Object for the Post API
      let changeValue = {
        'field_email': refTrim[i].controls.email.value,
        'field_name_of_reference': refTrim[i].controls.ref_name.value,
        'field_organization_name': refTrim[i].controls.org_name.value,
        'field_phone_number': refTrim[i].controls.phone.value
      };

      // Push the array value
      this.postDataLicence.push(changeValue);

    }
    // Check USER TYPE(Build Resume/Login)
    let userType;
    if (localStorage.getItem('loginCandidateUid')) {
      userType = localStorage.getItem('loginCandidateUid');
    } else {
      userType = localStorage.getItem('currentApplicantID');
    }
    // Post key Data API FORMAT
    let employementObj = {
      'uid': userType,
      'resume_type': 'ResumeBuilder',
      'resume_fid': '',
      'resume_template': '',
      'references': this.postDataLicence,
    };
    // POSTING THE DATA TO SERVER(EMPLOYEMENT HISTORY)
    this.applicantService.updateCustomResume(employementObj).subscribe(
      res => {
        console.log('Applicant Refrence', res);
        this.router.navigate(['/preview']);
      }
    );
  }
  // Accordian LIST FUNCTION
  accoradianIteam(i) {
    this.accordianList(i);
    this.openThisFormArray[i] = !this.openThisFormArray[i];
  }
  accordianList(j) {
    for (let i = 0; i < this.openThisFormArray.length; i++) {
      if (j !== i) {
        this.openThisFormArray[i] = false;
      }
    }
  }

  // SKIP THIS STEPS
  skipThisSteps() {
    this.router.navigate(['/preview']);
  }

}
