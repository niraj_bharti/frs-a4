import { Component, OnInit, Inject, } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators,  } from '@angular/forms';
import { Router } from '@angular/router';
import { ApplicantService } from '../../services/applicants.service';

@Component({
  selector: 'app-accomplishment',
  templateUrl: './accomplishment.component.html',
  styleUrls: ['./accomplishment.component.css', '../buildresume.css']
})

export class AccomplishmentComponent implements OnInit {
  private toolbarOptions = [
    'bold',
    'italic',
    'underline',
    { 'align': '' },
    { 'align': 'center' },
    { 'align': 'right' },
    { 'list': 'ordered' },
    { 'list': 'bullet' },
    { 'indent': '-1' },
    { 'indent': '+1' }
  ];
  public accomplishmentForm: FormGroup;
  public describe = {
    modules: {
      toolbar: this.toolbarOptions
    },
    placeholder: `•   Explain Job Responsibilities.
•   Add your work culture.
•   Add specific requirements like shifts, days etc.

`
  };
  private editorData;
  private pageLoded: boolean;
  constructor( @Inject(DOCUMENT) private document: Document, private formbuilder: FormBuilder, private router: Router,
                private applicantService: ApplicantService) { }

  ngOnInit() {

    // Unique Classname for body to call a different background images
    this.document.body.classList.add('applicantBody');
    this.document.body.classList.add('buildresume1');

    this.initFormControl('');
    this.fetchFieldData();

  }



  // Initializing form
  initFormControl(obj) {
    this.accomplishmentForm = this.formbuilder.group({
      'askill': [obj, Validators.required]
    });

  }

  // Feaching submitted data
  fetchFieldData() {
    let checkStartus;
    if (localStorage.getItem('loginCandidateUid')) {
      checkStartus = localStorage.getItem('loginCandidateUid');
    } else {
      checkStartus = localStorage.getItem('currentApplicantID');
    }
    let Uid = {
      'uid': checkStartus,
      'hwa_nid': localStorage.getItem('applicantHWAToken')
    };
    this.applicantService.getResumeData(Uid).subscribe(
      res => {

        if (res['applicant_resume_detail']['field_accomplishment_skill']) {
          this.editorData = res['applicant_resume_detail']['field_accomplishment_skill'][0].value;
        }
        this.pageLoded = true;
      });
  }
  // Submitting Form Data to backend
  submitContinue() {

    // Check USER TYPE(Build Resume/Login)
    let userType;
    if (localStorage.getItem('loginCandidateUid')) {
      userType = localStorage.getItem('loginCandidateUid');
    } else {
      userType = localStorage.getItem('currentApplicantID');
    }

    // Post key Data API FORMAT
    let objectiveObj = {
      'uid': userType,
      'resume_type': 'ResumeBuilder',
      'resume_fid': '',
      'resume_template': '',
      'field_accomplishment_skill': this.accomplishmentForm.controls['askill'].value
    };
    // POSTING THE DATA TO SERVER(EMPLOYEMENT HISTORY)
    this.applicantService.updateCustomResume(objectiveObj).subscribe(
      res => {
        this.router.navigate(['/employement']);
      });
  }

}

