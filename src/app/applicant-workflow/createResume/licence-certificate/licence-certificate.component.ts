import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { ApplicantService } from '../../services/applicants.service';

@Component({
  selector: 'app-licence-certificate',
  templateUrl: './licence-certificate.component.html',
  styleUrls: ['./licence-certificate.component.css', '../buildresume.css']
})
export class LicenceCertificateComponent implements OnInit {

  public licenceDetailForm: FormGroup;
  private getLicenceName = [];
  private postDataLicence = [];
  private openThisFormArray = [];
  private pageLoded: boolean;
  private futureDate: boolean;
  private futureDateIndex = [];
  private hideSkip: boolean;

  constructor( @Inject(DOCUMENT) private document: Document, private formbuilder: FormBuilder, private router: Router,
               private applicantService: ApplicantService) { }

  ngOnInit() {
    // Unique Classname for body to call a different background images
    this.document.body.classList.add('applicantBody');
    this.document.body.classList.add('buildresume1');

    // Add Employement History Dynamiccaly
    this.licenceDetailForm = this.formbuilder.group({
      licenceGroup: this.formbuilder.array([])
    });

    // calling initEmployemetGroup
    this.fetchCertificateData();

  }
  // Initialize employerGroup
  initEducationDetails(lic, om, oy, is, city, state) {
    return this.formbuilder.group({
      'licence_name': [lic, Validators.required],
      'obtained_month': [om, Validators.required],
      'obtained_year': [oy, Validators.required],
      'issue_auth': [is, Validators.required],
      'city': [city, Validators.required],
      'state': [state, Validators.required],
    });
  }

  // Add more List of form ARRAY
  addLicence() {
    const control = <FormArray>this.licenceDetailForm.controls['licenceGroup'];
    const addrCtrl = this.initEducationDetails('', '', '', '', '', '');
    control.push(addrCtrl);

    this.openThisFormArray.push(true);
    this.accordianList(this.openThisFormArray.length -1 );
  }

  fetchCertificateData() {
    let checkStartus;
    if (localStorage.getItem('loginCandidateUid')) {
      checkStartus = localStorage.getItem('loginCandidateUid');
    } else {
      checkStartus = localStorage.getItem('currentApplicantID');
    }
    let Uid = {
      'uid': checkStartus,
      'hwa_nid': localStorage.getItem('applicantHWAToken')
    };
    this.applicantService.getResumeData(Uid).subscribe(
      res => {
        let trimVal = res['applicant_resume_detail'].field_licenses_certification;
        if (trimVal) {
          if (trimVal.length !== 0) {
            this.hideSkip = true;
          }
          const control = <FormArray>this.licenceDetailForm.controls['licenceGroup'];

          // joiningTimestamp
          let yrObtainTimestamp = trimVal[0].field_year_obtained[0].value;
          let obtainDate = new Date(yrObtainTimestamp * 1000);
          for (let i = 0; i < trimVal.length; i++) {
            control.push(this.initEducationDetails(
              this.getLicenceName[i] = trimVal[i].field_name_of_licenses_or_certif[0].value,
              obtainDate.getMonth(),
              obtainDate.getFullYear(),
              trimVal[i].field_name_of_issuing_authority[0].value,
              trimVal[i].field_licenses_city[0].value,
              trimVal[i].field_licenses_state[0].value
            ));

            if ( i === 0 ) {
              this.openThisFormArray[i] = true;

            } else {
              this.openThisFormArray[i] = false;

            }
          }

        } else {
          this.addLicence();
        }

        this.pageLoded = true;
      });
  }

  // REMOVE FORM ARRAY
  removeLicence(i: number) {
    const control = <FormArray>this.licenceDetailForm.controls['licenceGroup'];
    control.removeAt(i);
    this.getLicenceName.splice(i, 1);
  }

  // SUBMIT DATA
  submitContinue() {

    // Holding the form control in let
    let LicenceDataTrim = this.licenceDetailForm.controls['licenceGroup']['controls'];

    // loop to itrate through form control
    for (let i = 0; i < LicenceDataTrim.length; i++) {

      // TIMESTAMP METHOD FOR THE JOINING DATA AND RELEAVING DATE USING toTimestamp() FUNCTION
      let yearObtain = this.toTimestamp(LicenceDataTrim[i].controls.obtained_year.value, LicenceDataTrim[i].controls.obtained_month.value);
      // let returnOrignalValue = new Date(yearObtain * 1000)
      // Holding the Object for the Post API
      let changeValue = {
        'field_licenses_city': LicenceDataTrim[i].controls.city.value,
        'field_licenses_state': LicenceDataTrim[i].controls.state.value,
        'field_name_of_issuing_authority': LicenceDataTrim[i].controls.issue_auth.value,
        'field_name_of_licenses_or_certif': LicenceDataTrim[i].controls.licence_name.value,
        'field_year_obtained': yearObtain
      };

      // Push the array value
      this.postDataLicence.push(changeValue);

    }

    // Check USER TYPE(Build Resume/Login)
    let userType;
    if (localStorage.getItem('loginCandidateUid')) {
      userType = localStorage.getItem('loginCandidateUid');
    } else {
      userType = localStorage.getItem('currentApplicantID');
    }

    // Post key Data API FORMAT
    let employementObj = {
      'uid': userType,
      'resume_type': 'ResumeBuilder',
      'resume_fid': '',
      'resume_template': '',
      'licenses_certification': this.postDataLicence,
    };
    // POSTING THE DATA TO SERVER(EMPLOYEMENT HISTORY)
    this.applicantService.updateCustomResume(employementObj).subscribe(
      res => {
        this.router.navigate(['/militry']);
      });
  }
  // TIMESTAMP CONVERSION FUNCTION
  toTimestamp(year, month) {
    let datum = new Date(Date.UTC(year, month));
    return datum.getTime() / 1000.0;
  }
  // Accordion LIST FUNCTION
  accoradianIteam(i) {
    this.accordianList(i);
    this.openThisFormArray[i] = !this.openThisFormArray[i];
  }
  accordianList(j) {
    for (let i = 0; i < this.openThisFormArray.length; i++) {
      if (j !== i) {
        this.openThisFormArray[i] = false;
      }
    }
  }
  // SKIP THIS STEPS
  skipThisSteps() {
    this.router.navigate(['/militry']);
  }

  // Getting Month Val
  getMonthValue(i) {
  const control = <FormArray>this.licenceDetailForm.controls['licenceGroup'];
  let month = control['controls'][i]['controls'].obtained_month.value;
  let year = control['controls'][i]['controls'].obtained_year.value;
  let yearObtain = this.toTimestamp(year, month);
  let returnOrignalValue = new Date(yearObtain * 1000);
  if ( this.checkFutureDate(returnOrignalValue) === false ) {
    this.futureDate = true;
    this.futureDateIndex[i] = true;
  } else {
    this.futureDate = false;
    this.futureDateIndex[i] = false;
  }
  }

  // Future Date
  checkFutureDate(selectedDate) {
   let today = new Date();
    return selectedDate < today;
  }

}
