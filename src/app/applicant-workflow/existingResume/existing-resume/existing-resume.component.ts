import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { FormBuilder, } from '@angular/forms';
import { Router } from '@angular/router';
import { ApplicantService } from '../../services/applicants.service';

@Component({
  selector: 'app-existing-resume',
  templateUrl: './existing-resume.component.html',
  styleUrls: ['./existing-resume.component.css', '../../applicant.css']
})
export class ExistingResumeComponent implements OnInit {
  private candidateName;
  private nameOfresume;
  private enableFile: boolean;
  private enableResume: boolean;
  private fileName;
  private fileUrl;
  private pageLoded: boolean;
  private showModel: boolean;
  private dynamicText = 'Use Existing Resume';
  private abs: boolean;
  constructor( @Inject(DOCUMENT) private document: Document, private formbuilder: FormBuilder, private router: Router,
               private applicantService: ApplicantService) { }

  ngOnInit() {
    localStorage.removeItem('resumeId');

    // Unique Classname for body to call a different background images
    this.document.body.classList.add('applicantBody');
    this.document.body.classList.add('buildresume1');

    // Fetching resume data
    this.resumeData();

  }

  // Get Previous Resume data
  resumeData() {
    let Uid = {
      'uid': localStorage.getItem('loginCandidateUid'),
      'hwa_nid': localStorage.getItem('applicantHWAToken')
    };
    this.applicantService.getResumeData(Uid).subscribe(
      res => {
        this.candidateName = res.user_detail.field_first_name[0].value;
       this.nameOfresume = res.user_detail.field_first_name[0].value + '_' + res.user_detail.field_last_name[0].value;
        if (res.applicant_resume_detail.length !== 0) {
          if (res.applicant_resume_detail.field_resume_type[0].value === 'File') {
            this.enableFile = true;
            this.fileName = res.applicant_resume_detail.field_resume_upload[0].file_name;
            this.fileUrl = res.applicant_resume_detail.field_resume_upload[0].url;
            localStorage.setItem('resumeURL', res.applicant_resume_detail.field_resume_upload[0].url);
          } else {
            this.enableFile = false;
            this.enableResume = true;
          }
        }
        this.pageLoded = true;
      });
  }
  //
  deleteResume() {
  }

  // applyThroughFile
  applyThroughFile() {
    this.abs = true;
    this.dynamicText = 'Processing';
    let obj = {
      'uid' : localStorage.getItem('loginCandidateUid'),
      'hwa_id': localStorage.getItem('applicantHWAToken')
    };
    this.applicantService.ifApplied(obj).subscribe(
      response => {
        console.log(response)
        if (response.applied.status === 'Yes') {
          this.showModel = true;
          this.dynamicText = 'Applied';
          this.abs = false;
          // this.router.navigate(['/useexisting']);
        } else {
          this.router.navigate(['/knockoutquestion']);
        }
      }
    );
  }

  viewResume() {
    this.router.navigate(['/viewresume']);
  }
  sendResume() {
    this.abs = true;
    this.dynamicText = 'Processing';
    let obj = {
      'uid' : localStorage.getItem('loginCandidateUid'),
      'hwa_id': localStorage.getItem('applicantHWAToken')
    };
    this.applicantService.ifApplied(obj).subscribe(
      response => {
        console.log(response.applied.status)
        if (response.applied.status === 'Yes') {

          this.showModel = true;
          this.dynamicText = 'Applied';
          this.abs = false;
        } else {
          this.router.navigate(['/objective']);
        }
      }
    );
  }

  closeModel() {
    this.showModel = false;
  }
  // Upload Resume


  // Build Resume
  buildResume() {
    this.router.navigate(['/objective']);
  }
  PreResume() {
    this.router.navigate(['/preview']);
  }

}
