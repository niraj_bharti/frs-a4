import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApplicantService } from '../../services/applicants.service';

@Component({
  selector: 'app-applicant-login',
  templateUrl: './applicant-login.component.html',
  styleUrls: ['./applicant-login.component.css', '../../applicant.css']
})
export class ApplicantLoginComponent implements OnInit {
  private applicantLogin: FormGroup;
  public emailMask = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  private errorLogin;
  constructor( @Inject(DOCUMENT) private document: Document, private formbuilder: FormBuilder, private router: Router,
     private applicantService: ApplicantService) { }

  ngOnInit() {

    // remove localstorage
    localStorage.removeItem('applicantForgotPassword');

    if (localStorage.getItem('applicantLogins')) {
      this.router.navigate(['/useexisting']);
    }

    // Unique Classname for body to call a different background images
    this.document.body.classList.add('applicantBody');
    this.document.body.classList.add('buildresume1');

    // Login Form
    this.applicantLogin = this.formbuilder.group({
      'email': ['', Validators.compose([Validators.required,
      Validators.pattern(this.emailMask)])],
      'password': ['', Validators.required]
    });
  }

  //
  loginCandidate(loginFields) {
    let data = JSON.stringify({ 'name': loginFields.email, 'pass': loginFields.password });
    this.applicantService.login(data).subscribe(
      res => {
        console.log(res['full_name']);
        let user = res['current_user'];
        if (user) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('candidate_userToken', JSON.stringify(res['jwt_token']));
          localStorage.setItem('loginCandidateUid', res['current_user'].uid);
          localStorage.setItem('candidate_currentUser', JSON.stringify(user));
          localStorage.setItem('candididateNameByemailPassword', res['full_name']);
          if (res['jwt_token']) {
            this.applicantService.setCandidateLogedIn(true);
            this.router.navigate(['/useexisting']);
          } else {
            this.applicantService.setCandidateLogedIn(false);
          }


        } else {
          this.errorLogin = 'Email Id or Password is Incorrect!';
        }
      },
      error => {
          this.errorLogin = 'Email Id or Password is Incorrect!';
      });
  }
  //
  SignUpPage() {
    // this.router.navigate[('/useexisting')];
    this.router.navigate(['/applicantsignup']);
  }
  //
  forgotPass() {
    localStorage.setItem('applicantForgotPassword', 'redirectToApplicantLogin');
    this.router.navigate(['/forgot-password']);
  }
}
