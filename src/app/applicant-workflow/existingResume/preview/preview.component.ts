import { Component, OnInit } from '@angular/core';
import {SafeUrl} from '@angular/platform-browser';
import { DomSanitizer} from '@angular/platform-browser';
import { Router } from '@angular/router';
@Component({
  selector: 'preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.css']
})
export class PreviewComponent implements OnInit {
  private resumeUrl: SafeUrl;
  private getValue;
  private getFull;
  constructor(private sanitizer: DomSanitizer, private router: Router) { }

  ngOnInit() {
    this.getValue = localStorage.getItem('resumeURL');
    this.getFull = 'https://docs.google.com/viewerng/viewer?url=' + this.getValue +'&embedded=true';
    this.resumeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.getFull);
  }

  goBack() {
    this.router.navigate(['/useexisting']);
  }

}
