import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/Rx';
import { Observable } from 'rxjs';
import {Subject} from 'rxjs/Subject';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {UserService} from '../../services/user.service';


@Injectable()

export class ApplicantService {
  public switchUrl = this.userService.switchUrl;
  private fetachAlltheData = this.switchUrl + 'applicant_ko_skill.json';
  // private uploadResumeApi = this.switchUrl + 'frs_file_upload.json';
  private fileUploadApi = this.switchUrl + 'fileupload.json';
  private registerApplicantApi = this.switchUrl + 'applicant_registration.json';
  private saveKnockoutQuestionApi = this.switchUrl + 'hwa_ko_skill_result_save.json';
  private createResumeApi = this.switchUrl + 'applicant_reusme_workflow.json';
  private getResumeDataApi = this.switchUrl + 'load_resume.json';
  private socialLoginApi = this.switchUrl + 'social_login.json';
  private socialmoreInfoApi = this.switchUrl + 'applicant_user_profile_update.json';
  private applicantProfileGetApi = this.switchUrl + 'applicant_profile_details.json';
  private ifAppliedApi = this.switchUrl + 'applicant_applied_check.json';
  private loginUrl = this.switchUrl + 'applicant_login.json';
  private _candidateisLogedIn: boolean;
  public subjectCandidateIsLogin: Subject<Boolean> = new BehaviorSubject<Boolean>(false);
  constructor(private http: Http, private router: Router, private userService: UserService) {

  }

  //
  setCandidateLogedIn(iscanLogedIn: boolean): void {
    this._candidateisLogedIn = iscanLogedIn;
    this.subjectCandidateIsLogin.next(iscanLogedIn);
  }

  getCandidateLogedIn(): Observable<Boolean> {
    return this.subjectCandidateIsLogin.asObservable();
  }
  //
  isCandidateLogedIn() {
    let token = JSON.parse(localStorage.getItem('candidate_userToken'));
    let user = JSON.parse(localStorage.getItem('candidate_currentUser'));
    if (token) {
      this.setCandidateLogedIn(true);
      return user;
    } else {
      this.setCandidateLogedIn(false);
      this.router.navigate(['/applicantlogin']);
      return null;
    }
  }
  // JWT
  private applicantJWT() {
    // create authorization header with jwt token
    let userToken = JSON.parse(localStorage.getItem('applicantUserToken'));
    if (userToken) {
      let headers = new Headers({ 'Authorization': 'Bearer ' + userToken });
      return new RequestOptions({ headers: headers });
    }
  }



  // CORS SEtting
  authHeaders() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    return headers;
  }
  // Function for featching all the information related to particular HWA
  getHWAdataForApplicant(hwa_nid): any {
    return this.http.post(this.fetachAlltheData, hwa_nid, { headers: this.authHeaders() })
      .map((res: Response) => res.json())
      .catch((error: any) => error.json());
  }

  // Upload file Function
  fileUpload(Obj) {
    return this.http.post(this.fileUploadApi, Obj)
      .map((res: Response) => res.json())
      .catch((error: any) => error.json());
  }

  // Register Applicant Function
  registerApplicant(candidate) {
    return this.http.post(this.registerApplicantApi, candidate, { headers: this.authHeaders() })
      .map((res: Response) => res.json())
      .catch((error: any) => error.json());
  }

  // Saving Knockout question answerd by candindate
  saveKnockoutQuestion(candidateData) {
    return this.http.post(this.saveKnockoutQuestionApi, candidateData, { headers: this.authHeaders() })
      .map((res: Response) => res.json())
      .catch((error: any) => error.json());
  }
  getUpdatedKnockoutQuestion(nodeId) {
    return this.http.get(this.saveKnockoutQuestionApi, { headers: this.authHeaders() })
      .map((res: Response) => res.json())
      .catch((error: any) => error.json());
  }

  // Create resume using TTV
  updateCustomResume(dataObj) {
    return this.http.post(this.createResumeApi, dataObj, { headers: this.authHeaders() })
      .map((res: Response) => res.json())
      .catch((error: any) => error.json());

  }

  // Create resume using TTV
  updateCustomResumeJWT(dataObj) {
    return this.http.post(this.createResumeApi, dataObj, this.applicantJWT())
      .map((res: Response) => res.json())
      .catch((error: any) => error.json());
  }


  // display resume dataObj
  getResumeData(applicantUid) {
    return this.http.post(this.getResumeDataApi, applicantUid, { headers: this.authHeaders() })
      .map((res: Response) => res.json())
      .catch((error: any) => error.json());
  }

  // socialLoginApi
  socialLogin(clientData) {
    return this.http.post(this.socialLoginApi, clientData, { headers: this.authHeaders() })
      .map((res: Response) => res.json())
      .catch((error: any) => error.json());
  }
  // get Profile data
  applicantProfileGet(uid) {
    return this.http.post(this.applicantProfileGetApi, uid, { headers: this.authHeaders() })
      .map((res: Response) => res.json())
      .catch((error: any) => error.json());
  }
  // Get moreinfo from social login
  socialmoreInfo(parm) {
    return this.http.post(this.socialmoreInfoApi, parm, { headers: this.authHeaders() })
      .map((res: Response) => res.json())
      .catch((error: any) => error.json());
  }
  ifApplied(parm) {
    return this.http.post(this.ifAppliedApi, parm, { headers: this.authHeaders() })
      .map((res: Response) => res.json())
      .catch((error: any) => error.json());
  }

  login(user) {
    return this.http.post(this.loginUrl, user, { headers: this.authHeaders() })
      .map((response: Response) => response.json())
      .catch((error: any) => error.json());
  }
}
