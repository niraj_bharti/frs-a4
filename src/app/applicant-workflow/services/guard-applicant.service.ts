import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { ApplicantService } from './applicants.service';

@Injectable()
export class GuardApplicantService  implements CanActivate {

  constructor(private router: Router, private candidateService: ApplicantService) { }
  canActivate() {
    if (localStorage.getItem('candidate_userToken')) {
      // logged in so return true
      this.candidateService.setCandidateLogedIn(true);
      return true;
    }
    // not logged in so redirect to login page
    this.candidateService.setCandidateLogedIn(false);
    this.router.navigate(['/applicantsignup']);
    return false;
  }
}
