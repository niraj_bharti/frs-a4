import { Injectable } from '@angular/core';

@Injectable()
export class ApplicantDataService {
  private _getApplicantData: any;
  private _getEducationData: any;
  private _getLicenceData: any;
  private _getMilitryData: any;
  private _getRefrenceData: any;

  constructor() { }

  get saveStepData(): any {
    return this._getApplicantData;
  }
  set saveStepData(value: any) {
    this._getApplicantData = value;
  }

  get saveEducation(): any {
    return this._getEducationData;
  }
  set saveEducation(value: any) {
    this._getEducationData = value;
  }

  get saveLicence(): any {
    return this._getLicenceData;
  }
  set saveLicence(value: any) {
    this._getLicenceData = value;
  }

  get saveMilitry(): any {
    return this._getMilitryData;
  }
  set saveMilitry(value: any) {
    this._getMilitryData = value;
  }

  get saveRef(): any {
    return this._getRefrenceData;
  }
  set saveRef(value: any) {
    this._getRefrenceData = value;
  }

}
