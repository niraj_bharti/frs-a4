import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { AuthService } from 'angular2-social-login';
import { Router } from '@angular/router';
import {ApplicantService} from '../services/applicants.service';

@Component({
  selector: 'app-applicant-header',
  templateUrl: './applicant-header.component.html',
  styleUrls: ['./applicant-header.component.css']
})
export class ApplicantHeaderComponent implements OnInit, OnDestroy {
  public user;
  private isLogin;
  private candidateName;
  sub: any;
  private hideLogout: boolean;
  public isCandidateLoginusingEmail: boolean;
  constructor(private  applicant: ApplicantService, public _auth: AuthService, private router: Router, private zone: NgZone) { }

  ngOnInit() {
    if (localStorage.getItem('candidate_userToken')) {
      this.applicant.setCandidateLogedIn(true);
    } else {
      this.applicant.setCandidateLogedIn(false);
    }
    this.applicant.getCandidateLogedIn().subscribe((isCandidateLogedIn: any) => {
      if (isCandidateLogedIn) {
        this.isCandidateLoginusingEmail = true;
        this.candidateName = localStorage.getItem('candididateNameByemailPassword');
      } else {
        this.isCandidateLoginusingEmail = false;
        this.candidateName = '';
      }
    });

    if (localStorage.getItem('loginCandidateName')) {
      this.isLogin = localStorage.getItem('loginCandidateName');
      this.hideLogout = true;
    } else {
      this.isLogin = '';
      this.hideLogout = false;
    }

  }
  ngOnDestroy() {
    // this.sub.unsubscribe();

  }

  logout() {
    this._auth.logout().subscribe(
      (data) => {
        localStorage.removeItem('applicantLogins');
        localStorage.removeItem('applicantUserToken');
        localStorage.removeItem('applicantUser');
        localStorage.removeItem('loginCandidateName');
        localStorage.removeItem('loginCandidateUid'); // Removing applicantID that was coming through LOGIN
        this.user = null;
        localStorage.removeItem('koNodeID');
        localStorage.removeItem('currentApplicantID');
        localStorage.removeItem('neeUserId'); // Remove last saved user
        localStorage.removeItem('resumeId');
        localStorage.removeItem('commingThroughBuildResume');
        localStorage.removeItem('applicantLogins');
        localStorage.removeItem('commingThroughLogin');
        this.router.navigate(['/applicantlogin']);
        this.sub.unsubscribe();
      });
  }

  logoutApplicant() {
    localStorage.removeItem('candidate_userToken');
    localStorage.removeItem('loginCandidateUid');
    localStorage.removeItem('candidate_currentUser');
    localStorage.removeItem('candididateNameByemailPassword');
    this.applicant.setCandidateLogedIn(false);
    this.router.navigate(['/applicantlogin']);
  }
  goToLandingPage() {
    if (localStorage.getItem('candidate_userToken')) {
      this.router.navigate(['/useexisting']);
    } else {
      this.router.navigate(['/landing']);
    }
  }
}
