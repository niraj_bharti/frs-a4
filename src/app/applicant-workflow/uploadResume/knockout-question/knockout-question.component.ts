import {Component, Inject, OnInit} from '@angular/core';
import { ApplicantService } from '../../services/applicants.service';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import {DOCUMENT} from "@angular/platform-browser";

@Component({
  selector: 'app-knockout-question',
  templateUrl: './knockout-question.component.html',
  styleUrls: ['../../applicant.css']
})
export class KoQuestionComponent implements OnInit {
  public KoFormApplicant: FormGroup;
  private employerSkillQuestion = [];
  private randomHWAId;
  private HWAcompany;
  private koQuestion;
  private koNid;
  private currentCandidateName;
  private koQuestionPostArr = [];
  private pageLoading: boolean;
  private contentLoad: boolean;
  private formSubmit: boolean;
  constructor(
    private HwaServicesforApplicant: ApplicantService,
    private formbuilder: FormBuilder,
    private router: Router, @Inject(DOCUMENT) private document: Document) { }

  ngOnInit() {
    // Unique Classname for body to call a different background images
    this.document.body.classList.add('applicantBody');
    this.document.body.classList.add('buildresume1');
    // Storing the Candidate name
    if (localStorage.getItem('currentCandidateName')) {
      this.currentCandidateName = localStorage.getItem('currentCandidateName');
    } else if (localStorage.getItem('loginCandidateName')) {
      this.currentCandidateName = localStorage.getItem('loginCandidateName');
    } else if (localStorage.getItem('candididateNameByemailPassword')) {
      this.currentCandidateName = localStorage.getItem('candididateNameByemailPassword');
    }
    // Formbuilder group for knockout question list provided by employer
    this.KoFormApplicant = this.formbuilder.group({
      'listOfSkillQuestions': this.formbuilder.array([
      ])
    });

    // Get question List
    let userId = '';
    if (localStorage.getItem('neeUserId')) {
      userId = localStorage.getItem('neeUserId');
    } else {
      userId = '';
    }
    // Random HWA ID after user click on apply Now CTA
    this.randomHWAId = {
      'hwa_nid': localStorage.getItem('applicantHWAToken'),
      'uid': userId
    };
    this.getQuestionList(); // Calling the get question function
  }

  // Initilizing the question
  initSkillQuestionList(val, qnid) {
    return this.formbuilder.group({
      'checkValue': [val, Validators.required], //  , Validators.required,
      'nid': [qnid, Validators.required]
    });
  }


  // Getting all KO question set by employer
  getQuestionList() {
    this.HwaServicesforApplicant.getHWAdataForApplicant(this.randomHWAId).subscribe(
      res => {
        if (res.applicant_ko !== null) {
          // Get and Assign the value for Postion, Companey and Locations
          this.employerSkillQuestion = res.applicant_ko;
          this.HWAcompany = res.HWA_information[0].field_name_of_your_business;
          this.koQuestion = res.applicant_ko; // Storing Only KO Data in array
          this.koNid = res.applicant_ko; // NID

          // Loop for KO List
          const control = <FormArray>this.KoFormApplicant.controls['listOfSkillQuestions'];
          for (let j = 0; j < this.employerSkillQuestion.length; j++) {
            control.push(this.initSkillQuestionList(this.employerSkillQuestion[j].default, this.employerSkillQuestion[j].nid));
          }
          this.pageLoading = true;
          this.contentLoad = true;
        } else {
          this.router.navigate(['/skillquestion']);
        }
      });
  }

  // Submitting the Ko data
  submitKnockout() {
    this.formSubmit = true;
    let checkNodeId;
    if (localStorage.getItem('koNodeID')) {
      checkNodeId = localStorage.getItem('koNodeID');
    } else {
      checkNodeId = '';
    }
    // koQuestionPostArr
    for (let i = 0; i < this.KoFormApplicant.controls['listOfSkillQuestions']['controls'].length; i++) {
      let changeValue = {
        'nid': this.KoFormApplicant.controls['listOfSkillQuestions'].value[i]['nid'],
        'Value': this.KoFormApplicant.controls['listOfSkillQuestions'].value[i]['checkValue'],
        'title': this.employerSkillQuestion[i].title
      };
      this.koQuestionPostArr.push(changeValue);
    }
    // Check USER TYPE(Build Resume/Login)
    let userType;
    if (localStorage.getItem('loginCandidateUid')) {
      userType = localStorage.getItem('loginCandidateUid');
    } else {
      userType = localStorage.getItem('currentApplicantID');
    }
    // API Keys provided by Backend for KO Questions
    let sendQuestion = {
      'hwa_id': this.randomHWAId.hwa_nid,
      'ko_result': JSON.stringify(this.koQuestionPostArr),
      'skill_result': '',
      'uid': userType
    };

    // Posting data
    this.HwaServicesforApplicant.saveKnockoutQuestion(sendQuestion).subscribe(
      res => {
        localStorage.setItem('koNodeID', res['hwa_result_save']['nid'][0].value);
        localStorage.setItem('neeUserId', res['hwa_result_save']['uid'][0].target_id);
        this.router.navigate(['/skillquestion']);
      });
  }
}
