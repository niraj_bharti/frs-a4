import {Component, Inject, OnInit} from '@angular/core';
import { ApplicantService } from '../../services/applicants.service';
import {DOCUMENT} from "@angular/platform-browser";
@Component({
  selector: 'app-sucess-screen',
  templateUrl: './sucess-screen.component.html',
  styleUrls: ['./sucess-screen.component.css']
})
export class SucessScreenComponent implements OnInit {
  private pageLoading: boolean;
  private contentLoad: boolean;
  private uSerName;
  private randomHWAId;
  constructor(private HwaServicesforApplicant: ApplicantService, @Inject(DOCUMENT) private document: Document) { }

  ngOnInit() {
    this.document.body.classList.add('applicantBody');
    this.document.body.classList.add('buildresume1');

    let userId = '';
    if (localStorage.getItem('neeUserId')) {
      userId = localStorage.getItem('neeUserId');
    } else {
      userId = '';
    }
    this.randomHWAId = {
      'hwa_nid': localStorage.getItem('applicantHWAToken'),
      'uid': userId
    };


    let userType;
    if (localStorage.getItem('loginCandidateUid')) {
      userType = localStorage.getItem('loginCandidateUid');
    } else {
      userType = localStorage.getItem('currentApplicantID');
    }
    // API Keys provided by Backend for Skill Questions
    let sendQuestion = {
      'hwa_id': this.randomHWAId.hwa_nid,
      'ko_result': '',
      'uid': userType,
      'field_applied': 'Yes'
    };
    this.HwaServicesforApplicant.saveKnockoutQuestion(sendQuestion).subscribe(
      res => {
        console.log(res);
      });

    // Storing the Candidate name
    if (localStorage.getItem('currentCandidateName')) {
      this.uSerName = localStorage.getItem('currentCandidateName');
    } else if (localStorage.getItem('loginCandidateName')) {
      this.uSerName = localStorage.getItem('loginCandidateName');
    } else if (localStorage.getItem('candididateNameByemailPassword')) {
      this.uSerName = localStorage.getItem('candididateNameByemailPassword');
    }

    this.pageLoading = true;
    this.contentLoad = true;
    localStorage.removeItem('koNodeID');
    localStorage.removeItem('currentApplicantID');
    localStorage.removeItem('neeUserId')
    localStorage.removeItem('resumeId');
    localStorage.removeItem('commingThroughBuildResume');
  }

}
