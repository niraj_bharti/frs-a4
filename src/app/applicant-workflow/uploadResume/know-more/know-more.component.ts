import {Component, Inject, OnInit} from '@angular/core';
import { ApplicantService } from '../../services/applicants.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { Router } from '@angular/router';
import {DOCUMENT} from '@angular/platform-browser';
import {NotificationsService} from 'angular2-notifications/dist';


@Component({
  selector: 'app-know-more',
  templateUrl: './know-more.component.html',
  styleUrls: ['../../applicant.css']
})
export class KnowMoreComponent implements OnInit {
  private knowMoreInfoForm: FormGroup;
  public myModel = '';
  public mask = [/[1-9]/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public zipmask = [/[0-9]/, /\d/, /\d/, /\d/, /\d/];
  public emailMask = /^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  private getCityinfoList = [];
  private validZipStatus: string;
  private showSelect: boolean;
  private userAlready: string;
  private checkStatus: boolean;
  private pageLoading: boolean;
  private contentLoad: boolean;
  private formSubmit: boolean;
  public options = {
    position: ['top', 'center'],
    timeOut: 5000,
    lastOnBottom: true
  };
  constructor(private HwaServicesforApplicant: ApplicantService,
              private formbuilder: FormBuilder,
              private router: Router,
              private userService: UserService, @Inject(DOCUMENT) private document: Document, private _notificationsService: NotificationsService ) { }

  ngOnInit() {
    this.document.body.classList.add('applicantBody');
    this.document.body.classList.add('buildresume1');
    // Checking if user is already Logged In using SOCIAL/GENERAL
    if (localStorage.getItem('applicantLogins')) {
      this.router.navigate(['/useexisting']);
    }


    // PRELOADING
    this.pageLoading = true;
    this.contentLoad = true;
    //  Forbuilder For Upload resume FormBuilder

    this.knowMoreInfoForm = this.formbuilder.group({
      'firstName': ['', Validators.required],
      'lastName': ['', Validators.required],
      'Email': ['', Validators.compose([Validators.required,
        Validators.pattern(this.emailMask)])],
      'cellPhone': ['', [Validators.required, Validators.pattern('^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$')]],
      'zipCode': ['', Validators.required],
      'city': ['', Validators.required],
      'state': ['', Validators.required],
    });

  }

  //  Register Candidate Action

  registerCandidate(fvalue: any) {

  // removing UID if stored in local storage if already stored
    localStorage.removeItem('neeUserId');
    this.formSubmit = true;
    if (!this.knowMoreInfoForm.controls['cellPhone'].valid) {
      this.checkStatus = true;
    } else {
      this.checkStatus = false;
    }
    let userObj = {
      'mail': fvalue.Email,
      'field_first_name': fvalue.firstName,
      'field_last_name': fvalue.lastName,
      'field_phone_number': fvalue.cellPhone,
      'field_city': fvalue.city,
      'field_state': fvalue.state,
      'field_zip': fvalue.zipCode,
      'resume_id': localStorage.getItem('resumeId')
    };
    this.HwaServicesforApplicant.registerApplicant(userObj).subscribe(
      res => {

        if (res) {

          localStorage.setItem('currentApplicantID', res['data']['uid'][0].value);
          localStorage.setItem('currentCandidateName', res['data']['field_first_name'][0].value);
          if (res['user_status'] === 'existing') {
            this._notificationsService.error(
              'Already Registered!',
              'An account with this email already exists.',
              {
                timeOut: 3500,
                showProgressBar: true,
                pauseOnHover: false,
                clickToClose: false
              }
            );
            this.formSubmit = false;
          } else {
            if (this.knowMoreInfoForm.valid !== true) {
              this.userAlready = `Please fill all the required details`;
            } else {
              if (localStorage.getItem('commingThroughBuildResume')) {
                this.router.navigate(['/objective']);
              } else {
                let fileObj = {
                  'uid': res['data']['uid'][0].value,
                  'resume_type': 'File',
                  'field_resume_upload': localStorage.getItem('resumeId')
                };
                this.HwaServicesforApplicant.updateCustomResume(fileObj).subscribe(
                  response => {
                    console.log(fileObj)
                    this.router.navigate(['/knockoutquestion']);
                   }
                );
              }

            }
          }
        }


      },
      error => {

      });

  }
  //  Valid Zipcode, checking if this is only for USA
  validUsCountry(res): boolean {
    let isUs = false;
    //  console.log(res)
    if (res['status'] === 'OK') {
      for (let i = 0; i <= res['results'][0].address_components.length - 1; i++) {
        if (res['results'][0].address_components[i].short_name === 'US') {
          isUs = true;
          //  console.log('isUs', isUs)
          break;
        }
      }
    }
    return isUs;
  }
  //  Auto Filled Zip code city and state function
  zipCodeSearch(count) {
    let zipc = this.knowMoreInfoForm.value.zipCode;


    if (!isNaN(zipc)) {

      this.userService.searchZip(zipc).subscribe(
        res => {
          console.log('GOOGLE1', res);
          console.log('GOOGLE2', res['results']);
          console.log('GOOGLE3', res['results'][0]);
          console.log('GOOGLE2', res['results'][0].address_components);
          if (this.validUsCountry(res)) {
            if (res['results'][0].postcode_localities) {
              this.showSelect = true;
              this.getCityinfoList = [];
              for (let j = 0; j < res['results'][0].postcode_localities.length; j++) {
                this.getCityinfoList.push(res['results'][0].postcode_localities[j]);
              }

            } else {
              this.showSelect = false;
            }

            for (let i = 0; i <= res['results'][0].address_components.length - 1; i++) {

              if (res['results'][0].address_components[i].types[0] === 'administrative_area_level_1') {
                console.log('STATUS', this.knowMoreInfoForm.controls['city'].status);
                console.log(this.knowMoreInfoForm.controls['state'].status);

                let getstate = res['results'][0].address_components[i].short_name;
                // console.log(this.getCityinfo=res['results'][0].address_components[i].length)
                let getCity = res['results'][0].address_components[1].long_name;
                //   const control = <FormArray>this.profileForm.controls['address'];
                let arryobj = [];
                arryobj[count] = { state: getstate, city: getCity };
                this.knowMoreInfoForm.controls['state'].patchValue(getstate);
                this.knowMoreInfoForm.controls['city'].patchValue(getCity);
                this.validZipStatus = '';
              }

            }
          } else {


            this.validZipStatus = 'Please enter valid US zip code';
            this.knowMoreInfoForm.controls['city'].patchValue('');
            this.knowMoreInfoForm.controls['state'].patchValue('');
           // this.showSelect[count] = false;



            // this.hideInput=true;
          }
        });
    } else {
      this.validZipStatus = 'Please enter valid US zip code';
      this.knowMoreInfoForm.controls['city'].patchValue('');
      this.knowMoreInfoForm.controls['state'].patchValue('');
      this.showSelect[count] = false;
    }
  }

}
