import {Component, Inject, OnInit} from '@angular/core';
import {DOCUMENT, Title} from '@angular/platform-browser';
import { ApplicantService } from '../../services/applicants.service';
import {  FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['../../applicant.css']
})
export class LandingComponent implements OnInit {
  private HWAposition;
  private HWAlocation;
  private HWAcompany;
  private pageLoading: boolean;

  constructor(private HwaServicesforApplicant: ApplicantService,
              private formbuilder: FormBuilder,
              private router: Router,
              private titleService: Title,
              @Inject(DOCUMENT) private document: Document
  ) {

  }

  ngOnInit() {
    this.document.body.classList.add('applicantBody');
    this.document.body.classList.add('buildresume1');
    if (localStorage.getItem('candidate_userToken')) {
      this.router.navigate(['/useexisting']);
    } else {
      this.router.navigate(['/landing']);
    }
    this.titleService.setTitle('Applicant Landing');


    // Checking if user is alredy Logged In using SOCIAL/GENRAL
    if (localStorage.getItem('applicantLogins')) {
      this.router.navigate(['/useexisting']);
    }




    // Setting HWA iD and USEr ID
    let randomHWAId = {
      'hwa_nid': localStorage.getItem('applicantHWAToken'),
      'uid': '',
    };


    // Calling Services getHWAdataForApplicant() for featching basic data posted by an Employer
    this.HwaServicesforApplicant.getHWAdataForApplicant(randomHWAId).subscribe(
      res => {
        console.log(res)
        // Get and Assign the value for Postion, Companey and Locations
        this.HWAposition = res.HWA_information[0].title;
        this.HWAcompany = res.HWA_information[0].field_name_of_your_business;
        this.HWAlocation = res.HWA_information[0].title_1 + ',  ' + res.HWA_information[0].field_city + ',   ' + res.HWA_information[0].field_state;
        this.pageLoading = true;
      }
    );
  }

  // Upload Resume Action
  uploadResume() {
    this.router.navigate(['/basicinformation']);
    return false;
  }


  // Build Resume function
  buildResume() {
    this.router.navigate(['/basicinformation']);
    localStorage.setItem('commingThroughBuildResume', 'creteCandidateProfile');
  }

  // applicantlogin
  loginPage() {
    this.router.navigate(['/applicantlogin']);
    localStorage.setItem('commingThroughLogin', 'useExisting');
  }


}
