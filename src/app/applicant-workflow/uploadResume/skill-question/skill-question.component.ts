import {Component, Inject, OnInit} from '@angular/core';
import { ApplicantService } from '../../services/applicants.service';
import { FormGroup, FormBuilder, Validators,  FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import {DOCUMENT} from "@angular/platform-browser";


@Component({
  selector: 'app-skill-question',
  templateUrl: './skill-question.component.html',
  styleUrls: ['../../applicant.css']
})
export class SkillQuestionComponent implements OnInit {
  public skillExpForm: FormGroup;
  private employerSkillQuestion = [];
  private randomHWAId;
  private HWAcompany;
  private koQuestion;
  private koNid;
  private currentCandidateName;
  private skillQuestionPostArr = [];
  private pageLoading: boolean;
  private contentLoad: boolean;
  private formSubmit: boolean;
  private changeButton_value;
  constructor(
    private HwaServicesforApplicant: ApplicantService,
    private formbuilder: FormBuilder,
    private router: Router, @Inject(DOCUMENT) private document: Document) { }

  ngOnInit() {
    this.document.body.classList.add('applicantBody');
    this.document.body.classList.add('buildresume1');
    if (localStorage.getItem('commingThroughBuildResume')) {
      this.changeButton_value = 'Continue';
    } else {
      this.changeButton_value = 'Submit';
    }
    // Page Redirection if HWA is Null{}
    if (localStorage.getItem('applicantHWAToken')) {

    } else {
      this.router.navigate(['/sucessmessage']);
    }

    // Storing the Candidate name
    if (localStorage.getItem('currentCandidateName')) {
      this.currentCandidateName = localStorage.getItem('currentCandidateName');
    } else if (localStorage.getItem('loginCandidateName')) {
      this.currentCandidateName = localStorage.getItem('loginCandidateName');
    } else if (localStorage.getItem('candididateNameByemailPassword')) {
      this.currentCandidateName = localStorage.getItem('candididateNameByemailPassword');
    }

    // Formbuilder group for knockout question list provided by employer
    this.skillExpForm = this.formbuilder.group({
      'listOfSkillQuestions': this.formbuilder.array([
      ])
    });
    console.log('STATUS gg', localStorage.getItem('neeUserId'));
    // Get question List
    let userId = '';
    if (localStorage.getItem('neeUserId')) {
      userId = localStorage.getItem('neeUserId');
    } else {
      userId = '';
    }

    // Random HWA ID after apply Now
    this.randomHWAId = {
      'hwa_nid': localStorage.getItem('applicantHWAToken'),
      'uid': userId
    };

    this.getQuestionList();
  }

  initSkillQuestionList(val, qnid) {
    return this.formbuilder.group({
      'checkValue': [val, Validators.required], // , Validators.required,
      'nid': [qnid, Validators.required]
    });
  }
  //
  getQuestionList() {

    this.HwaServicesforApplicant.getHWAdataForApplicant(this.randomHWAId).subscribe(
      res => {

        if (res.applicant_skill !== null) {
          // Get and Assign the value for Postion, Companey and Locations
          this.employerSkillQuestion = res.applicant_skill;
          this.HWAcompany = res.HWA_information[0].field_name_of_your_business;
          this.koQuestion = res.applicant_skill; // Storing Only KO Data in array
          this.koNid = res.applicant_skill; // NID

          // Loop for KO List
          const control = <FormArray>this.skillExpForm.controls['listOfSkillQuestions'];
          for (let j = 0; j < this.employerSkillQuestion.length; j++) {
            control.push(this.initSkillQuestionList(this.employerSkillQuestion[j].default, this.employerSkillQuestion[j].nid));

          }
          this.pageLoading = true;
          this.contentLoad = true;
        } else {
          this.router.navigate(['/sucessmessage']);
        }});

  }

  applyNow() {
    this.formSubmit = true;
    let checkNodeId;
    if (localStorage.getItem('koNodeID')) {
      checkNodeId = localStorage.getItem('koNodeID');
    } else {
      checkNodeId = '';
    }
    // koQuestionPostArr
    for (let i = 0; i < this.skillExpForm.controls['listOfSkillQuestions']['controls'].length; i++) {

      let changeValue = {
        'nid': this.skillExpForm.controls['listOfSkillQuestions'].value[i]['nid'],
        'Value': this.skillExpForm.controls['listOfSkillQuestions'].value[i]['checkValue'],
        'title': this.employerSkillQuestion[i].title,
        'field_applied': 'Yes'
      };
      this.skillQuestionPostArr.push(changeValue);
    }
    // Check USER TYPE(Build Resume/Login)
    let userType;
    if (localStorage.getItem('loginCandidateUid')) {
      userType = localStorage.getItem('loginCandidateUid');
    } else {
      userType = localStorage.getItem('currentApplicantID');
    }

    // API Keys provided by Backend for Skill Questions
    let sendQuestion = {
      'hwa_id': this.randomHWAId.hwa_nid,
      'ko_result': '',
      'skill_result': JSON.stringify(this.skillQuestionPostArr),
      'uid': userType
    };
    this.HwaServicesforApplicant.saveKnockoutQuestion(sendQuestion).subscribe(
      res => {
        localStorage.setItem('koNodeID', res['hwa_result_save']['nid'][0].value);
        localStorage.setItem('neeUserId', res['hwa_result_save']['uid'][0].target_id);
        this.router.navigate(['/sucessmessage']);
        console.log(res);
      });
  }
  // Remove data
  removeLocalstorage4knockout() {
    localStorage.removeItem('koNodeID');
  }
}
